<?php

namespace App\Http\Controllers\backoffice;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Spatie\Analytics\Period;
use App\Language;
use App\TicketsCategories;
use App\TicketsCategoriesTranslations;
use Auth;
use Analytics;
use Lava;
use Toastr;

class TicketsCategoriesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $categories = DB::table('tickets_categories')->get();
        $categoriesTranslations = DB::table('tickets_categories_translations')->get();

        return view('backoffice.pages.ticketsCategories.ticketsCategories', compact('categories', 'categoriesTranslations'));
    }

    public function create()
    {

        return view('backoffice.pages.ticketsCategories.add-ticketsCategories');
    }

    public function store(Request $request)
    {        
        // create artist
        $category = new TicketsCategories();
        $category->save();

        $categoryId = TicketsCategories::all()->last()->id;

   

        //PT Translation
        $categoryTranslation = new TicketsCategoriesTranslations();
        $categoryTranslation->name = $request->request->get('name-pt'); 
        $categoryTranslation->ticketscategories_id = $categoryId;
        $categoryTranslation->language_id = Language::all()->first()->id;
        $categoryTranslation->save(); 

        //EN Translation
        $categoryTranslation = new TicketsCategoriesTranslations();
        $categoryTranslation->name = $request->request->get('name-en'); 
        $categoryTranslation->ticketscategories_id = $categoryId;
        $categoryTranslation->language_id = Language::all()->last()->id;
        $categoryTranslation->save(); 


        Toastr::success('Ticket Category created with success.', 'Ticket Category', ["positionClass" => "toast-top-center"]);

        return Redirect::to('admin/ticketscategories');
    }

    public function edit($id)
    {
        $category = TicketsCategories::where('id', $id)->get()[0];
        $categoryTranslationPt = TicketsCategoriesTranslations::where([['ticketscategories_id', '=', $id], ['language_id', '=', '1']])->get()[0];
        $categoryTranslationEn = TicketsCategoriesTranslations::where([['ticketscategories_id', '=', $id], ['language_id', '=', '2']])->get()[0];

        return view('backoffice.pages.ticketsCategories.edit-ticketsCategories', compact('category', 'categoryTranslationPt', 'categoryTranslationEn'));
    }

    public function update(Request $request) 
    {
        $regulationEn = array (
            'title' => $request->request->get('name-en'),
            'description' => $request->request->get('description-en')
        );

        //PT
        $updatePT = array (
            'name' => $request->request->get('name-pt')
        );
        $PTupdate = DB::table('tickets_categories_translations')->where([ ['ticketscategories_id', $request->request->get('id')],['language_id', '=', '1'] ])->update($updatePT);
        
        //EN
        $updateEN = array (
            'name' => $request->request->get('name-en')
        );
        $PTupdate = DB::table('tickets_categories_translations')->where([ ['ticketscategories_id', $request->request->get('id')],['language_id', '=', '2'] ])->update($updateEN);

        Toastr::success('Ticket Category updated with success.', 'Ticket Category', ["positionClass" => "toast-top-center"]);

        return Redirect::to('admin/ticketscategories');
    }


    public function destroy($id)
    {
        TicketsCategoriesTranslations::where('ticketscategories_id', $id)->delete();
        TicketsCategories::destroy($id);

        Toastr::success('Ticket Category deleted with success.', 'Ticket Category', ["positionClass" => "toast-top-center"]);


        return Redirect::to('admin/ticketscategories');

    }
}


