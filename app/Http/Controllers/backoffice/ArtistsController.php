<?php

namespace App\Http\Controllers\backoffice;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;
use Spatie\Analytics\Period;
use App\Language;
use App\Artists;
use App\ArtistsTranslations;
use Auth;
use Analytics;
use Lava;
use Toastr;

class ArtistsController extends Controller
{
    private $ArtistImage; 

    public function __construct()
    {
        $this->middleware('auth:admin');
        $ArtistImage = ''; 
    }
    public function getAll()
    {
        $artists = DB::table('artists')->where('id', '=', 1)->get();

        

        return json_encode($artists); 
    }

    public function index()
    {

        $source = public_path() . '/images/artists/tmp';
        File::deleteDirectory($source);

        $artists = DB::table('artists')->get();
        $artistsTranslations = DB::table('artists_translations')->get(); 

        return view('backoffice.pages.artists.artists', compact('artists', 'artistsTranslations'));
    }

    public function create()
    {
        $artists = DB::table('artists')->get();

        return view('backoffice.pages.artists.add-artists', compact('artists'));
    }

    public function store(Request $request)
    {
        // create artist
        $artist = new Artists();
        $artist->name = $request->request->get('name');
        $artist->day = $request->request->get('day');
        $artist->hours = $request->request->get('hour');
        $artist->facebook = $request->request->get('facebook');
        $artist->youtube = $request->request->get('youtube');
        $artist->main_image = ''; 
        $artist->save();

        //get artist ID
        $artistId = Artists::all()->last()->id;

        //create PT translation
        $artistPt = new ArtistsTranslations();        
        $artistPt->description = $request->request->get('description-pt');
        $artistPt->artists_id = $artistId; 
        $artistPt->language_id = '1';  //lang
        $artistPt->save();

        //create EN translation
        $artistEn = new ArtistsTranslations();
        $artistEn->description = $request->request->get('description-en');
        $artistEn->artists_id = $artistId; 
        $artistEn->language_id = '2'; //lang
        $artistEn->save(); 

       //create ES translation
       $artistEs = new ArtistsTranslations();
       $artistEs->description = $request->request->get('description-es');
       $artistEs->artists_id = $artistId; 
       $artistEs->language_id = '3'; //lang
       $artistEs->save(); 




        //Create artist folder if doesn't  exists
        $source = public_path() . '/artists';
        $destination = public_path() . '/images/artists' . '/' . $artistId;

        if (!File::exists($destination)) {
            File::makeDirectory($destination, 0777, true);
        }

        //save img uploaded
        $this->storeImage('main-image', $artistId);

        //Delete tmp directory
        $source = public_path() . '/images/artists/tmp';

        File::deleteDirectory($source);

        //get filename 
        $currentImg = File::allFiles(public_path() . $this->ArtistImage)[0]->getRelativePathname();

        //update artist with img path
        DB::table('artists')->where('id', $artistId)->update(['main_image' => $this->ArtistImage . '/'.  $currentImg]);

        
        Toastr::success('Artist created with success.', 'Artist', ["positionClass" => "toast-top-center"]);


        return Redirect::to('admin/artists');
    }

    public function edit($id)
    {
        $artist = Artists::where('id', $id)->get()[0];
        $artistPt = ArtistsTranslations::where([['artists_id', '=', $id], ['language_id', '=', '1']])->get()[0];
        $artistEn = ArtistsTranslations::where([['artists_id', '=', $id], ['language_id', '=', '2']])->get()[0];
        $artistEs = ArtistsTranslations::where([['artists_id', '=', $id], ['language_id', '=', '3']])->get()[0];

        $source = public_path() . '/images/artists/tmp';

        File::deleteDirectory($source);

        //Create folder artist/id if not exists
        $destination = public_path() . '/images/artists/tmp';

        if (!File::exists($destination)) {
            File::makeDirectory($destination, 0777, true);
        }

        $this->copyFolderToTmp('main-image', $id);

        return view('backoffice.pages.artists.edit-artists', compact('artist', 'artistPt', 'artistEn', 'artistEs'));
    }

    public function update(Request $request)
    {
        $source = public_path() . '/images/artists/' . $request->request->get('id');
        File::deleteDirectory($source);

        // artist main image
        $this->storeImage('main-image', $request->request->get('id'));

        $source = public_path() . '/images/services/tmp';
        File::deleteDirectory($source);

        // update artist entry
        $updatedArtist = array(
            'name' => $request->request->get('name'),
            'day' => $request->request->get('day'),
            'hours' => $request->request->get('hour'),
            'facebook' => $request->request->get('facebook'),
            'youtube' => $request->request->get('youtube'),
        );

        DB::table('artists')->where('id', $request->request->get('id'))->update($updatedArtist);

        // update artist PT
        $updatedArtistPt = array(
            'description' => $request->request->get('description-pt'),
        );

        DB::table('artists_translations')->where('artists_id', $request->request->get('id'))->update($updatedArtistPt);
        
        // update artist EN
        $updatedArtistEn = array(
            'description' => $request->request->get('description-en'),
        );

        DB::table('artists_translations')->where('artists_id', $request->request->get('id'))->update($updatedArtistEn);

        // update artist ES
        $updatedArtistEs = array(
            'description' => $request->request->get('description-es'),
        );

        DB::table('artists_translations')->where('artists_id', $request->request->get('id'))->update($updatedArtistEs);


        
        //get filename 
        $currentImg = File::allFiles(public_path() . $this->ArtistImage)[0]->getRelativePathname();
        //update sponsor with img path
        DB::table('artists')->where('id', $request->request->get('id'))->update(['main_image' => $this->ArtistImage . '/'.  $currentImg]);


        Toastr::success('Artist edited with success.', 'Artists', ["positionClass" => "toast-top-center"]);

        return Redirect::to('admin/artists');
    }

    public function destroy($id)
    {
        ArtistsTranslations::where('artists_id', $id)->delete();
        Artists::destroy($id);

        Toastr::success('Artist deleted with success.', 'Artist', ["positionClass" => "toast-top-center"]);


        return Redirect::to('admin/artists');

    }

    public function imageUpload(Request $request)
    {
        $folder = $request->request->get('folder');
        $name = $request->request->get('name');

        $path = public_path() . '/images/artists/' . $folder . '/' . $name . '/';

        $tmpImage = $_FILES[$name]["tmp_name"];

        $image = $_FILES[$name]['name'];

        if (!File::exists($path)) {
            File::makeDirectory($path, 0777, true);
        }

        $image = str_replace(' ', '', $image);
        File::copy($tmpImage, $path . '/' . $image);
        //echo json_encode($this->ArtistImage);
        echo json_encode($image);
    }

    public function imageDelete(Request $request)
    {
        $folder = $request->request->get('folder');
        $name = $request->request->get('name');
        $image = $request->request->get('imageName');

        if (is_array($image)) {
            $image = $image[0];
        } else {
            $image = $image;
        }

        $path = public_path() . '/images/artists/' . $folder . '/' . $name . '/';

        $image = str_replace(' ', '', $image);

        File::delete($path . str_replace('"', "", $image));
    }


    public function storeImage($folder, $serviceId, $tmp = "")
    {
        if ($tmp == "") {
            $source = public_path() . '/images/artists/tmp/' . $folder;
        } else {
            $source = public_path() . '/images/artists/tmp/' . $tmp;
        }
        if (File::files($source)) {
            $destination = public_path() . '/images/artists' . '/' . $serviceId . '/' . $folder;
            File::copyDirectory($source, $destination);
        }

        $this->ArtistImage = strstr($destination, '/images'); 
    }


    public function imageLoad(Request $request)
    {
        $id = $_GET['id'];
        $name = $_GET['name'];

        $path = public_path() . '/images/artists/' . $id . '/' . $name;

        if (File::exists($path)) {
            $files = File::allFiles($path);
            $data = array();

            foreach ($files as $file) {
                $details = array();
                $details['name'] = $file->getFileName();
                $details['path'] = '/images/artists/' . $id . '/' . $name . '/' . $file->getFileName();
                $details['size'] = $file->getSize();
                $data[] = $details;
            }

            echo json_encode($data);
        }
    }


    public function copyFolderToTmp($folder, $id, $tmp = "")
    {
        $source = public_path() . '/images/artists/' . $id . '/' . $folder;

        if (File::files($source)) {
            $destination = public_path() . '/images/artists/tmp/' . $folder;
            File::copyDirectory($source, $destination);
        }
    }
}


