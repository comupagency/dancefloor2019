<?php

namespace App\Http\Controllers\backoffice;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;
use Spatie\Analytics\Period;
use App\Language;
use App\Sponsors;
use App\SponsorsTranslations;
use Auth;
use Toastr;

class SponsorsController extends Controller
{

    private $sponsorLogo; 

    public function __construct()
    {
        $this->middleware('auth:admin');
        $sponsorLogo = ''; 
    }

    public function index()
    {

        $source = public_path() . '/images/sponsors/tmp';
        File::deleteDirectory($source);

        $sponsors = DB::table('sponsors')->get();
        $sponsorsTranslations = DB::table('sponsors_translations')->get();
        $sponsorsCategoriesTranslations = DB::table('sponsors_categories_translations')->get();

        return view('backoffice.pages.sponsors.sponsors', compact('sponsors','sponsorsTranslations','sponsorsCategoriesTranslations'));
    }

    public function create()
    {
        $sponsorsCategories = DB::table('sponsors_categories')->get();
        $sponsorsCategoriesTranslations = DB::table('sponsors_categories_translations')->get();

        return view('backoffice.pages.sponsors.add-sponsors', compact('sponsorsCategories', 'sponsorsCategoriesTranslations'));
    }

    public function store(Request $request)
    {
        // create sponsor
        $sponsor = new Sponsors();
        $sponsor->name = $request->request->get('name');
        $sponsor->logo = '';
        $sponsor->category_id = $request->request->get('category');
        $sponsor->save();

        //get sponsor ID
        $sponsorId = Sponsors::all()->last()->id;

        //create PT translation
        $sponsorPt = new SponsorsTranslations();        
        $sponsorPt->description = $request->request->get('description-pt');
        $sponsorPt->sponsors_id = $sponsorId; 
        $sponsorPt->language_id = Language::all()->first()->id;  //lang
        $sponsorPt->save();

        //create EN translation
        $sponsorEn = new SponsorsTranslations();
        $sponsorEn->description = $request->request->get('description-en');
        $sponsorEn->sponsors_id = $sponsorId; 
        $sponsorEn->language_id = Language::all()->last()->id; //lang
        $sponsorEn->save(); 


        //Create frolder cars/id if not exists
        $source = public_path() . '/sponsors';
        $destination = public_path() . '/images/sponsors' . '/' . $sponsorId;

        if (!File::exists($destination)) {
            File::makeDirectory($destination, 0777, true);
        }

        // car main features
        $this->storeImage('main-image', $sponsorId);

        //Delete tmp directory
        $source = public_path() . '/images/sponsors/tmp';

        File::deleteDirectory($source);

        //get filename 
        $currentImg = File::allFiles(public_path() . $this->sponsorLogo)[0]->getRelativePathname();

        //update sponsor with img path
        DB::table('sponsors')->where('id', $sponsorId)->update(['logo' => $this->sponsorLogo . '/'.  $currentImg]);


        Toastr::success('Sponsor created with success.', 'Sponsors', ["positionClass" => "toast-top-center"]);


        return Redirect::to('admin/sponsors');
    }

    public function edit($id)
    {
        $sponsor = DB::table('sponsors')->where('id', $id)->get()[0];
        $sponsorPt = DB::table('sponsors_translations')->where([['sponsors_id', '=', $id], ['language_id', '=', '1']])->get()[0];
        $sponsorEn = DB::table('sponsors_translations')->where([['sponsors_id', '=', $id], ['language_id', '=', '2']])->get()[0];

        $sponsorsCategories = DB::table('sponsors_categories')->get();
        $sponsorsCategoriesTranslations = DB::table('sponsors_categories_translations')->get();

    
        $source = public_path() . '/images/sponsors/tmp';

        File::deleteDirectory($source);

        //Create folder artist/id if not exists
        $destination = public_path() . '/images/sponsors/tmp';

        if (!File::exists($destination)) {
            File::makeDirectory($destination, 0777, true);
        }

        $this->copyFolderToTmp('main-image', $id);

        
        return view('backoffice.pages.sponsors.edit-sponsors', compact('sponsor', 'sponsorPt', 'sponsorEn', 'sponsorsCategories','sponsorsCategoriesTranslations'));
    }

    public function update(Request $request)
    {
        $source = public_path() . '/images/sponsors/' . $request->request->get('id');
        File::deleteDirectory($source);

        // sponsor main image
        $this->storeImage('main-image', $request->request->get('id'));

        $source = public_path() . '/images/sponsors/tmp';
        File::deleteDirectory($source);

        // update sponsor entry
        $updatedSponsor = array(
            "name" => $request->request->get('name'),
            "logo" => '',
            "category_id" => $request->request->get('category'),
        );

        //update sponsor  translations
        DB::table('sponsors')->where('id', $request->request->get('id'))->update($updatedSponsor);

        // update sponsor PT
        $updatedSponsorPt = array(
            'description' => $request->request->get('description-pt'),
        );

        DB::table('sponsors_translations')->where([ ['sponsors_id', $request->request->get('id')] , ['language_id', '=', '1'] ])->update($updatedSponsorPt);

        // update sponsor EM
        $updatedSponsorEn = array(
            'description' => $request->request->get('description-en'),
        );

        DB::table('sponsors_translations')->where([['sponsors_id', $request->request->get('id')],['language_id', '=', '2'] ])->update($updatedSponsorEn);
        

        //get filename 
        $currentImg = File::allFiles(public_path() . $this->sponsorLogo)[0]->getRelativePathname();
        //update sponsor with img path
        DB::table('sponsors')->where('id', $request->request->get('id'))->update(['logo' => $this->sponsorLogo . '/'.  $currentImg]);


        Toastr::success('Sponsor edited with success.', 'Sponsors', ["positionClass" => "toast-top-center"]);

        return Redirect::to('admin/sponsors');
    }

    public function destroy($id)
    {
        
        SponsorsTranslations::where('sponsors_id', $id)->delete();
        Sponsors::destroy($id);

        Toastr::success('Sponsor deleted with success.', 'Sponsors', ["positionClass" => "toast-top-center"]);


        return Redirect::to('admin/sponsors');

    }

    public function imageUpload(Request $request)
    {
        $folder = $request->request->get('folder');
        $name = $request->request->get('name');

        $path = public_path() . '/images/sponsors/' . $folder . '/' . $name . '/';

        $tmpImage = $_FILES[$name]["tmp_name"];

        $image = $_FILES[$name]['name'];

        if (!File::exists($path)) {
            File::makeDirectory($path, 0777, true);
        }

        $image = str_replace(' ', '', $image);
        File::copy($tmpImage, $path . '/' . $image);
        echo json_encode($image);
    }
    

    public function imageDelete(Request $request)
    {
        $folder = $request->request->get('folder');
        $name = $request->request->get('name');
        $image = $request->request->get('imageName');

        if (is_array($image)) {
            $image = $image[0];
        } else {
            $image = $image;
        }

        $path = public_path() . '/images/sponsors/' . $folder . '/' . $name . '/';

        $image = str_replace(' ', '', $image);

        File::delete($path . str_replace('"', "", $image));
    }


    public function storeImage($folder, $serviceId, $tmp = "")
    {
        if ($tmp == "") {
            $source = public_path() . '/images/sponsors/tmp/' . $folder;
        } else {
            $source = public_path() . '/images/sponsors/tmp/' . $tmp;
        }
        if (File::files($source)) {
            $destination = public_path() . '/images/sponsors' . '/' . $serviceId . '/' . $folder;
            File::copyDirectory($source, $destination);
        }

        $this->sponsorLogo = strstr($destination, '/images'); 
    }


    public function imageLoad(Request $request)
    {
        $id = $_GET['id'];
        $name = $_GET['name'];

        $path = public_path() . '/images/sponsors/' . $id . '/' . $name;

        if (File::exists($path)) {
            $files = File::allFiles($path);
            $data = array();

            foreach ($files as $file) {
                $details = array();
                $details['name'] = $file->getFileName();
                $details['path'] = '/images/sponsors/' . $id . '/' . $name . '/' . $file->getFileName();
                $details['size'] = $file->getSize();
                $data[] = $details;
            }

            echo json_encode($data);
        }
    }


    public function copyFolderToTmp($folder, $id, $tmp = "")
    {
        $source = public_path() . '/images/sponsors/' . $id . '/' . $folder;

        if (File::files($source)) {
            $destination = public_path() . '/images/sponsors/tmp/' . $folder;
            File::copyDirectory($source, $destination);
        }
    }
}


