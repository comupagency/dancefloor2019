<?php

namespace App\Http\Controllers\backoffice;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;
use Spatie\Analytics\Period;
use App\Language;
use App\Regulations;
use App\RegulationsTranslations;
use Auth;
use Toastr;

class RegulationsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $regulations = DB::table('regulations')->get();
        $regulationsTranslations = DB::table('regulations_translations')->get();

        return view('backoffice.pages.regulations.regulations', compact('regulations','regulationsTranslations'));
    }

    public function create()
    {
        $regulations = DB::table('regulations')->get();
        $regulationsTranslations = DB::table('regulations_translations')->get();

        return view('backoffice.pages.regulations.add-regulations', compact('regulations', 'regulationsTranslations'));
    }

    public function store(Request $request)
    {   
        // create regulation
        $regulation = new Regulations();
        $regulation->save();

        //get regulation ID
        $regulationId = Regulations::all()->last()->id;

        //create PT translation
        $regulationPt = new RegulationsTranslations();     
        $regulationPt->title = $request->request->get('name-pt');
        $regulationPt->description = $request->request->get('description-pt');
        $regulationPt->regulations_id = $regulationId; 
        $regulationPt->language_id = Language::all()->first()->id;  //lang
        $regulationPt->save();

        //create EN translation
        $regulationEn = new RegulationsTranslations();
        $regulationEn->title = $request->request->get('name-en');
        $regulationEn->description = $request->request->get('description-en');
        $regulationEn->regulations_id = $regulationId; 
        $regulationEn->language_id = Language::all()->last()->id; //lang
        $regulationEn->save(); 

        Toastr::success('Regulation created with success.', 'Regulations', ["positionClass" => "toast-top-center"]);

        return Redirect::to('admin/regulations');
    }

    public function edit($id)
    {
        
        $regulation = DB::table('regulations')->where('id', $id)->get()[0];
        $regulationPt = DB::table('regulations_translations')->where([['regulations_id', '=', $id], ['language_id', '=', '1']])->get()[0];
        $regulationEn = DB::table('regulations_translations')->where([['regulations_id', '=', $id], ['language_id', '=', '2']])->get()[0];
        
        return view('backoffice.pages.regulations.edit-regulations', compact('regulation', 'regulationPt', 'regulationEn'));
    }

    public function update(Request $request)
    {
        //update translations
        $regulationPt = array (
            'title' => $request->request->get('name-pt'),
            'description' => $request->request->get('description-pt')
        );

        $regulationEn = array (
            'title' => $request->request->get('name-en'),
            'description' => $request->request->get('description-en')
        );

        DB::table('regulations_translations')->where([ ['regulations_id', $request->request->get('id')],['language_id', '=', '1'] ])->update($regulationPt);
        DB::table('regulations_translations')->where([ ['regulations_id', $request->request->get('id')], ['language_id', '=', '2'] ])->update($regulationEn);
        
        Toastr::success('Regulation edited with success.', 'Regulations', ["positionClass" => "toast-top-center"]);

        return Redirect::to('admin/regulations');
    }

    public function destroy($id)
    {
        RegulationsTranslations::where('regulations_id', $id)->delete();
        Regulations::destroy($id);

        Toastr::success('Regulation deleted with success.', 'Regulations', ["positionClass" => "toast-top-center"]);

        return Redirect::to('admin/regulations');
    }
}


