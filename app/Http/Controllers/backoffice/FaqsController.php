<?php

namespace App\Http\Controllers\Backoffice;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Redirect;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Faqs;
use App\FaqsTranslations;
use Auth;
use Toastr; 


class FaqsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    
    public function index()
    {
        $faqs = DB::table('faqs')->get();
        $faqsTranslations = DB::table('faqs_translations')->get();

        return view('backoffice.pages.faqs.faqs', compact('faqs','faqsTranslations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $languages = LaravelLocalization::getSupportedLocales();
        return view('backoffice.pages.faqs.add-faqs', ['langs' => $languages]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

         // CREATE FAQS
         $faqs = Faqs::create(); 
         //RUN ALL LANGUAGES AND SAVE TO DATABASE
         $languages = LaravelLocalization::getSupportedLocales();
         $i = 1;
         foreach ($languages as $key => $lang){
             $faqsTranslations = new faqsTranslations;
             $faqsTranslations->question =  $request->request->get('textTitle-'.$key);
             $faqsTranslations->answer =  $request->request->get('textDescription-'.$key);
             $faqsTranslations->localeId =  $i;
             $faqsTranslations->faqsId =  $faqs->id;
             $faqsTranslations->save();
             $i++;
        }

        Toastr::success('FAQ was created with success', 'FAQS', [   "positionClass" => "toast-top-center"]);
        return Redirect::to('admin/faqs');
    }

    public function edit($id)
    {
        $faqTranslations = DB::table('faqs_translations')->where('faqs_id', '=', $id)->get();        
        $faq = DB::table('faqs')->where('id', '=', $id)->get();

        return view('backoffice.pages.faqs.edit-faqs', compact('faq','faqTranslations'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
        $langs = LaravelLocalization::getSupportedLocales();
        foreach($langs as $key => $lang){
            $lang = DB::table('languages')->where('name', $key)->get();
            $updatedFaqs = array('question' => $_POST['textTitle-'.$key], 'answer' => $_POST['textDescription-'.$key]);

            DB::table('faqstranslations')->where('faqsId', $_POST['id'])
            ->where('localeId',$lang[0]->id)
            ->update($updatedFaqs);
        }

        Toastr::success('FAQ was edited with success', 'FAQS', [   "positionClass" => "toast-top-center"]);

        return Redirect::to('admin/faqs');   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Remove from faqs translations 
        $languages = LaravelLocalization::getSupportedLocales();
        $faqsTranslations = new faqsTranslations;
        $faqsTranslations->where('faqsId', $id)->delete();
            
        //remove from faqs
        $faqs = Faqs::destroy($id); 

        Toastr::success('FAQ was deleted with success', 'FAQS', [   "positionClass" => "toast-top-center"]);
        
        return Redirect::to('admin/faqs');
    }
        /** 
     * Get current language
     */
    public function getCurrentLanguage(){
        $currentLang = App::getLocale();
        $lang = DB::table('languages')->where('name', $currentLang)->first();
        return $lang->id;
    }
}
