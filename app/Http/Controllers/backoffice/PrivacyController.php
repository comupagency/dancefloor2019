<?php

namespace App\Http\Controllers\backoffice;


use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Privacy;
use App\PrivacyTranslations;
use App\Language; 
use Auth;
use Analytics;
use Spatie\Analytics\Period;
use Lava;
use Toastr;

class PrivacyController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $privacy = DB::table('privacy')->get()->first();
        $privacyPT = DB::table('privacy_translations')->where('privacy_id','=', $privacy->id)->where('language_id', '=', '1')->get();
        $privacyEN = DB::table('privacy_translations')->where('privacy_id','=', $privacy->id)->where('language_id', '=', '2')->get();
        $privacyES = DB::table('privacy_translations')->where('privacy_id','=', $privacy->id)->where('language_id', '=', '3')->get();


        if(isset($privacy)){
            return view('backoffice.pages.privacy.privacy', compact('privacyPT', 'privacyEN','privacyES', 'privacy'));
        }

        return view('backoffice.pages.privacy.privacy');
    }

    public function update(Request $request)
    {

        //dd($request->request);
        //if no privacy
        $check = DB::table('privacy')->get()->first();

        if(empty($check)){
            //create privacy
            $priv = Privacy::create();

            //create PT
            $privPT = new PrivacyTranslations; 
            $privPT->description = $request->request->get('description-pt');
            $privPT->privacy_id = DB::table('privacy')->first()->id;
            $privPT->language_id = '1';
            $privPT->save();

            //create EN
            $privEN = new PrivacyTranslations; 
            $privEN->description = $request->request->get('description-en');
            $privEN->privacy_id = DB::table('privacy')->first()->id;
            $privEN->language_id = '2';
            $privEN->save();

            //create ES
            $privES = new PrivacyTranslations; 
            $privES->description = $request->request->get('description-es');
            $privES->privacy_id = DB::table('privacy')->first()->id;
            $privES->language_id = '3';
            $privES->save();

            //return route
            Toastr::success('Privacy Policy created with success.', 'Privacy Policy', ["positionClass" => "toast-top-center"]);

            return Redirect::to('admin/privacy');
        }

        //edit PT
        $privUpdate = array(
            'description' => $request->request->get('description-pt'),
        );
        DB::table('privacy_translations')->where('privacy_id', '=', $request->request->get('id'))->update($privUpdate);

        //edit EN
        $privUpdate = array(
            'description' => $request->request->get('description-en'),
        );
        DB::table('privacy_translations')->where('privacy_id', '=', $request->request->get('id'))->update($privUpdate);
        
        //edit ES
        $privUpdate = array(
            'description' => $request->request->get('description-es'),
        );
        DB::table('privacy_translations')->where('privacy_id', '=', $request->request->get('id'))->update($privUpdate);

        Toastr::success('Privacy Policy edited with success.', 'Privacy Policy', ["positionClass" => "toast-top-center"]);

        return Redirect::to('admin/privacy');

        
    }
}
