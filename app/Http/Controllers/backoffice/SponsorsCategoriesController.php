<?php

namespace App\Http\Controllers\backoffice;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;
use Spatie\Analytics\Period;
use App\Language;
use App\SponsorsCategories;
use App\SponsorsCategoriesTranslations;
use Auth;
use Analytics;
use Lava;
use Toastr;

class SponsorsCategoriesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {

        $source = public_path() . '/images/sponsorscategories/tmp';
        File::deleteDirectory($source);
        
        $sponsorsCategories = DB::table('sponsors_categories')->get(); 
        $sponsorsCategoriesTranslations = DB::table('sponsors_categories_translations')->get();

        return view('backoffice.pages.sponsorCategories.sponsorCategories', compact('sponsorsCategories', 'sponsorsCategoriesTranslations'));
    }

    public function create()
    {

        return view('backoffice.pages.sponsorCategories.add-sponsorCategories');
    }

    public function store(Request $request)
    {
        // create artist
        $category = new SponsorsCategories();
        $category->order = $request->request->get('order');
        $category->save();

        $categoryId = SponsorsCategories::all()->last()->id;

        //PT Translation
        $categoryTranslation = new SponsorsCategoriesTranslations();
        $categoryTranslation->name = $request->request->get('name-pt'); 
        $categoryTranslation->sponsorscategories_id = $categoryId;
        $categoryTranslation->language_id = Language::all()->first()->id;
        $categoryTranslation->save(); 

        //EN Translation
        $categoryTranslation = new SponsorsCategoriesTranslations();
        $categoryTranslation->name = $request->request->get('name-en'); 
        $categoryTranslation->sponsorscategories_id = $categoryId;
        $categoryTranslation->language_id = Language::all()->last()->id;
        $categoryTranslation->save(); 


        Toastr::success('Category created with success.', 'Category', ["positionClass" => "toast-top-center"]);

        return Redirect::to('admin/sponsorcategories');
    }

    public function edit($id)
    {
        $category = SponsorsCategories::where('id', $id)->get()[0];
        $categoryTranslationPt = SponsorsCategoriesTranslations::where([['sponsorscategories_id', '=', $id], ['language_id', '=', '1']])->get()[0];
        $categoryTranslationEn = SponsorsCategoriesTranslations::where([['sponsorscategories_id', '=', $id], ['language_id', '=', '2']])->get()[0];

        return view('backoffice.pages.sponsorcategories.edit-sponsorCategories', compact('category', 'categoryTranslationPt', 'categoryTranslationEn'));
    }

    public function destroy($id)
    {
        SponsorsCategoriesTranslations::where('sponsorscategories_id', $id)->delete();
        SponsorsCategories::destroy($id);

        Toastr::success('Sponsor Category deleted with success.', 'Category', ["positionClass" => "toast-top-center"]);


        return Redirect::to('admin/sponsorcategories');

    }
}


