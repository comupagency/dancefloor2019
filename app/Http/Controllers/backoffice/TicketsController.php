<?php

namespace App\Http\Controllers\backoffice;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;
use Spatie\Analytics\Period;
use App\Language;
use App\Tickets;
use App\TicketsTranslations;
use App\TicketsCategories;
use App\TicketsCategoriesTranslations;
use Auth;
use Analytics;
use Lava;
use Toastr;

class TicketsController extends Controller
{

    private $TicketImage; 

    public function __construct()
    {
        $this->middleware('auth:admin');
        $TicketImage = ''; 

    }

    public function index()
    {   
        $source = public_path() . '/images/tickets/tmp';
        File::deleteDirectory($source);

        $tickets = DB::table('tickets')->get();
        $ticketsTranslations = DB::table('tickets_translations')->get(); 
        $categories = DB::table('tickets_categories');
        $categoriesTranslations = DB::table('tickets_categories_translations')->get(); 
        

        return view('backoffice.pages.tickets.tickets', compact('tickets', 'ticketsTranslations', 'categories', 'categoriesTranslations'));
    }

    public function create()
    {
        $categories = DB::table('tickets_categories')->get();
        $categoriesTranslations = DB::table('tickets_categories_translations')->get(); 


        return view('backoffice.pages.tickets.add-tickets', compact('categories', 'categoriesTranslations'));
    }

    public function store(Request $request)
    {
        //create ticket
        $ticket = new Tickets();
        $ticket->price = $request->request->get('price'); 
        $ticket->category_id = $request->request->get('category');
        $ticket->link = $request->request->get('link');
        $ticket->save(); 

        $ticketId = Tickets::all()->last()->id; 

        //create PT
        $ticketTranslation = new TicketsTranslations();
        $ticketTranslation->name = $request->request->get('name-pt');
        $ticketTranslation->tickets_id = $ticketId; 
        $ticketTranslation->language_id = Language::all()->first()->id;
        $ticketTranslation->save();

        //create EN
        $ticketTranslation = new TicketsTranslations();
        $ticketTranslation->name = $request->request->get('name-en');
        $ticketTranslation->tickets_id = $ticketId; 
        $ticketTranslation->language_id = Language::all()->last()->id;
        $ticketTranslation->save();

        //save img uploaded
        $this->storeImage('main-image', $ticketId);

        //Delete tmp directory
        $source = public_path() . '/images/tickets/tmp';

        File::deleteDirectory($source);

        //get filename 
        $currentImg = File::allFiles(public_path() . $this->TicketImage)[0]->getRelativePathname();
        DB::table('tickets')->where('id', $ticketId)->update(['main_image' => $this->TicketImage . '/'.  $currentImg]);


        Toastr::success('Ticket created with success.', 'Ticket', ["positionClass" => "toast-top-center"]);

        return Redirect::to('admin/tickets');
    }

    public function edit($id)
    {        
        $ticket = Tickets::where('id', $id)->get()[0];

        $ticketTranslationPt = TicketsTranslations::where([['tickets_id', '=', $id], ['language_id', '=', '1']])->get()[0];
        $ticketTranslationEn = TicketsTranslations::where([['tickets_id', '=', $id], ['language_id', '=', '2']])->get()[0];

        $ticketCategory = TicketsCategoriesTranslations::where([['ticketscategories_id', '=', $ticket->category_id], ['language_id', '=', '1']])->get()[0];

        $categories = DB::table('tickets_categories')->get();
        $categoriesTranslations = DB::table('tickets_categories_translations')->get(); 

        $source = public_path() . '/images/tickets/tmp';

        File::deleteDirectory($source);

        //Create folder artist/id if not exists
        $destination = public_path() . '/images/tickets/tmp';

        if (!File::exists($destination)) {
            File::makeDirectory($destination, 0777, true);
        }

        $this->copyFolderToTmp('main-image', $id);

        
        return view('backoffice.pages.tickets.edit-tickets', compact('ticket', 'ticketTranslationPt','ticketTranslationEn', 'ticketCategory','categories', 'categoriesTranslations'));
    }

    public function update(Request $request)
    {
        $source = public_path() . '/images/tickets/' . $request->request->get('id');
        File::deleteDirectory($source);

        // artist main image
        $this->storeImage('main-image', $request->request->get('id'));

        $source = public_path() . '/images/tickets/tmp';
        File::deleteDirectory($source);


        $ticketUpdate = array(
            'price' => $request->request->get('price'),
            'category_id' => $request->request->get('category'),
            'link' => $request->request->get('link')
        );
        
        DB::table('tickets')->where('id', '=', $request->request->get('id'))->update($ticketUpdate);

        $ticketPT = array(
            'name' =>$request->request->get('name-pt')
        );
        DB::table('tickets_translations')->where([ ['tickets_id', $request->request->get('id')],['language_id', '=', '1'] ])->update($ticketPT);

        $ticketEN = array(
            'name' =>$request->request->get('name-en')
        );
        DB::table('tickets_translations')->where([ ['tickets_id', $request->request->get('id')],['language_id', '=', '2'] ])->update($ticketEN);

        
        //get filename 
        $currentImg = File::allFiles(public_path() . $this->TicketImage)[0]->getRelativePathname();
        //update sponsor with img path
        DB::table('tickets')->where('id', $request->request->get('id'))->update(['main_image' => $this->TicketImage . '/'.  $currentImg]);


        Toastr::success('Ticket edited with success.', 'Ticket', ["positionClass" => "toast-top-center"]);

        return Redirect::to('admin/tickets');
    }

    public function destroy($id)
    {
        TicketsTranslations::where('tickets_id', $id)->delete();
        Tickets::destroy($id);

        Toastr::success('Ticket deleted with success.', 'Ticket', ["positionClass" => "toast-top-center"]);


        return Redirect::to('admin/tickets');

    }

    public function imageUpload(Request $request)
    {
        $folder = $request->request->get('folder');
        $name = $request->request->get('name');

        $path = public_path() . '/images/tickets/' . $folder . '/' . $name . '/';

        $tmpImage = $_FILES[$name]["tmp_name"];

        $image = $_FILES[$name]['name'];

        if (!File::exists($path)) {
            File::makeDirectory($path, 0777, true);
        }

        $image = str_replace(' ', '', $image);
        File::copy($tmpImage, $path . '/' . $image);
        //echo json_encode($this->TicketImage);
        echo json_encode($image);
    }

    public function imageDelete(Request $request)
    {
        $folder = $request->request->get('folder');
        $name = $request->request->get('name');
        $image = $request->request->get('imageName');

        if (is_array($image)) {
            $image = $image[0];
        } else {
            $image = $image;
        }

        $path = public_path() . '/images/tickets/' . $folder . '/' . $name . '/';

        $image = str_replace(' ', '', $image);

        File::delete($path . str_replace('"', "", $image));
    }


    public function storeImage($folder, $serviceId, $tmp = "")
    {
        if ($tmp == "") {
            $source = public_path() . '/images/tickets/tmp/' . $folder;
        } else {
            $source = public_path() . '/images/tickets/tmp/' . $tmp;
        }
        if (File::files($source)) {
            $destination = public_path() . '/images/tickets' . '/' . $serviceId . '/' . $folder;
            File::copyDirectory($source, $destination);
        }

        $this->TicketImage = strstr($destination, '/images'); 
    }


    public function imageLoad(Request $request)
    {
        $id = $_GET['id'];
        $name = $_GET['name'];

        $path = public_path() . '/images/tickets/' . $id . '/' . $name;

        if (File::exists($path)) {
            $files = File::allFiles($path);
            $data = array();

            foreach ($files as $file) {
                $details = array();
                $details['name'] = $file->getFileName();
                $details['path'] = '/images/tickets/' . $id . '/' . $name . '/' . $file->getFileName();
                $details['size'] = $file->getSize();
                $data[] = $details;
            }

            echo json_encode($data);
        }
    }


    public function copyFolderToTmp($folder, $id, $tmp = "")
    {
        $source = public_path() . '/images/tickets/' . $id . '/' . $folder;

        if (File::files($source)) {
            $destination = public_path() . '/images/tickets/tmp/' . $folder;
            File::copyDirectory($source, $destination);
        }
    }
}


