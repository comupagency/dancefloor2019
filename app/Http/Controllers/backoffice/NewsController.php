<?php

namespace App\Http\Controllers\backoffice;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;
use Spatie\Analytics\Period;
use App\Language;
use App\News;
use App\NewsTranslations;
use Auth;
use Analytics;
use Lava;
use Toastr;

class NewsController extends Controller
{
    private $mainImage;
    private $thumbnailImage;

    public function __construct()
    {
        $this->middleware('auth:admin');
        $mainImage = ''; 
        $thumbnailImage = '';
    }

    public function getAll()
    {
        $news = DB::table('news')->where('id', '=', 1)->get();

        

        return json_encode($news); 
    }

    public function index()
    {

        $source = public_path() . '/images/news/tmp';
        File::deleteDirectory($source);


        
        $news = DB::table('news')->get();
        $newsTranslations = DB::table('news_translations')->get();

        return view('backoffice.pages.news.news', compact('news','newsTranslations'));
    }

    public function create()
    {
        $news = DB::table('news')->get();

        return view('backoffice.pages.news.add-news', compact('news'));
    }

    public function store(Request $request)
    {



        // create artist
        $new = new News();
        $new->main_image = ''; 
        $new->thumbnail = '';
        $new->save();

        //get artist ID
        $newId = News::all()->last()->id;

        //create PT translation
        $newPt = new NewsTranslations(); 
        $newPt->title = $request->request->get('title-pt');       
        $newPt->subtitle = $request->request->get('subtitle-pt');
        $newPt->description = $request->request->get('description-pt');
        $newPt->news_id = $newId; 
        $newPt->language_id = '1';  //lang
        $newPt->save();

        //create EN translation
        $newEn = new NewsTranslations();
        $newEn->title = $request->request->get('title-en');
        $newEn->subtitle = $request->request->get('subtitle-en');
        $newEn->description = $request->request->get('description-en');
        $newEn->news_id = $newId; 
        $newEn->language_id = '2'; //lang
        $newEn->save(); 

        //create ES translation
        $newEn = new NewsTranslations();
        $newEn->title = $request->request->get('title-es');
        $newEn->subtitle = $request->request->get('subtitle-es');
        $newEn->description = $request->request->get('description-es');
        $newEn->news_id = $newId; 
        $newEn->language_id = '3'; //lang
        $newEn->save(); 

        //Create artist folder if doesn't  exists
        $source = public_path() . '/news';
        $destination = public_path() . '/images/news' . '/' . $newId;

        if (!File::exists($destination)) {
            File::makeDirectory($destination, 0777, true);
        }

        //save img uploaded
        $this->storeImage('main-image', $newId);
        $this->storeImage('thumbnail', $newId);

        //Delete tmp directory
        $source = public_path() . '/images/news/tmp';

        File::deleteDirectory($source);

        //get filename 
        $currentThumbnail = File::allFiles(public_path() . $this->thumbnailImage)[0]->getRelativePathname();
        $currentMainImg = File::allFiles(public_path() . $this->mainImage)[0]->getRelativePathname();

        //update artist with img path
        DB::table('news')->where('id', $newId)->update(['thumbnail' => $this->thumbnailImage . '/'.  $currentThumbnail]);
        DB::table('news')->where('id', $newId)->update(['main_image' => $this->mainImage . '/'.  $currentMainImg]);

        
        Toastr::success('News created with success.', 'News', ["positionClass" => "toast-top-center"]);


        return Redirect::to('admin/news');
    }

    public function edit($id)
    {
        $news = News::where('id', $id)->get()[0];
        $newsPt = NewsTranslations::where([['news_id', '=', $id], ['language_id', '=', '1']])->get()[0];
        $newsEn = NewsTranslations::where([['news_id', '=', $id], ['language_id', '=', '2']])->get()[0];
        $newsEs = NewsTranslations::where([['news_id', '=', $id], ['language_id', '=', '3']])->get()[0];

        
        $source = public_path() . '/images/news/tmp';

        File::deleteDirectory($source);

        //Create folder artist/id if not exists
        $destination = public_path() . '/images/news/tmp';

        if (!File::exists($destination)) {
            File::makeDirectory($destination, 0777, true);
        }

        $this->copyFolderToTmp('main-image', $id);
        $this->copyFolderToTmp('thumbnail', $id);

        return view('backoffice.pages.news.edit-news', compact('news', 'newsPt', 'newsEn','newsEs'));
    }

    public function update(Request $request)
    {


        $source = public_path() . '/images/news/' . $request->request->get('id');
        File::deleteDirectory($source);

        // artist main image
        $this->storeImage('thumbnail', $request->request->get('id'));
        $this->storeImage('main-image', $request->request->get('id'));

        $source = public_path() . '/images/news/tmp';
        File::deleteDirectory($source);

        // update artist PT
        $updatedNewsPt = array(
            'title' => $request->request->get('title-pt'),
            'subtitle' => $request->request->get('subtitle-pt'),
            'description' => $request->request->get('description-pt'),
        );
        
        DB::table('news_translations')->where([ ['news_id', $request->request->get('id')],['language_id', '=', '1'] ])->update($updatedNewsPt);
        
        // update artist EN
        $updatedNewsEn = array(
            'title' => $request->request->get('title-en'),
            'subtitle' => $request->request->get('subtitle-en'),
            'description' => $request->request->get('description-en'),
        );

        DB::table('news_translations')->where([ ['news_id', $request->request->get('id')],['language_id', '=', '2'] ])->update($updatedNewsEn);


        // update artist ES
        $updatedNewsEs = array(
            'title' => $request->request->get('title-es'),
            'subtitle' => $request->request->get('subtitle-es'),
            'description' => $request->request->get('description-es'),
        );

        DB::table('news_translations')->where([ ['news_id', $request->request->get('id')],['language_id', '=', '3'] ])->update($updatedNewsEs);
        
        //get filename 
        $currentThumbnail = File::allFiles(public_path() . $this->thumbnailImage)[0]->getRelativePathname();
        $currentMainImg = File::allFiles(public_path() . $this->mainImage)[0]->getRelativePathname();

        //update sponsor with img path
        DB::table('news')->where('id', $request->request->get('id'))->update(['thumbnail' => $this->thumbnailImage . '/'.  $currentThumbnail]);
        DB::table('news')->where('id', $request->request->get('id'))->update(['main_image' => $this->mainImage . '/'.  $currentMainImg]);



        Toastr::success('News edited with success.', 'News', ["positionClass" => "toast-top-center"]);

        return Redirect::to('admin/news');
    }

    public function destroy($id)
    {
        NewsTranslations::where('news_id', $id)->delete();
        News::destroy($id);

        Toastr::success('News deleted with success.', 'News', ["positionClass" => "toast-top-center"]);


        return Redirect::to('admin/news');

    }

    public function imageUpload(Request $request)
    {

        
        $folder = $request->request->get('folder');
        $name = $request->request->get('name');

        $path = public_path() . '/images/news/' . $folder . '/' . $name . '/';

        $tmpImage = $_FILES[$name]["tmp_name"];

        $image = $_FILES[$name]['name'];

        if (!File::exists($path)) {
            File::makeDirectory($path, 0777, true);
        }

        $image = str_replace(' ', '', $image);
        File::copy($tmpImage, $path . '/' . $image);
        //echo json_encode($this->ArtistImage);
        echo json_encode($image);
    }

    public function imageDelete(Request $request)
    {
        $folder = $request->request->get('folder');
        $name = $request->request->get('name');
        $image = $request->request->get('imageName');

        if (is_array($image)) {
            $image = $image[0];
        } else {
            $image = $image;
        }

        $path = public_path() . '/images/news/' . $folder . '/' . $name . '/';

        $image = str_replace(' ', '', $image);

        File::delete($path . str_replace('"', "", $image));
    }


    public function storeImage($folder, $newId, $tmp = "")
    {
        if ($tmp == "") {
            $source = public_path() . '/images/news/tmp/' . $folder;
        } else {
            $source = public_path() . '/images/news/tmp/' . $tmp;
        }
        if (File::files($source)) {
            $destination = public_path() . '/images/news' . '/' . $newId . '/' . $folder;
            File::copyDirectory($source, $destination);
        }
        
        if($folder == 'thumbnail'){
            $this->thumbnailImage = strstr($destination, '/images');
        } 
        if($folder == 'main-image'){
            $this->mainImage = strstr($destination, '/images');
        }
    }


    public function imageLoad(Request $request)
    {
        $id = $_GET['id'];
        $name = $_GET['name'];

        $path = public_path() . '/images/news/' . $id . '/' . $name;

        if (File::exists($path)) {
            $files = File::allFiles($path);
            $data = array();

            foreach ($files as $file) {
                $details = array();
                $details['name'] = $file->getFileName();
                $details['path'] = '/images/news/' . $id . '/' . $name . '/' . $file->getFileName();
                $details['size'] = $file->getSize();
                $data[] = $details;
            }

            echo json_encode($data);
        }
    }


    public function copyFolderToTmp($folder, $id, $tmp = "")
    {
        $source = public_path() . '/images/news/' . $id . '/' . $folder;

        if (File::files($source)) {
            $destination = public_path() . '/images/news/tmp/' . $folder;
            File::copyDirectory($source, $destination);
        }
    }
}


