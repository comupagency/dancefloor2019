<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use App;
use DB;

use Illuminate\Http\Request;

class ArtistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $language = DB::table('languages')->where('name', App::getLocale())->get()->first()->id;




        $artist = DB::table('artists')->where('id', $id)->get()->first();



        $artistDescription = DB::table('artists_translations')->where('language_id', $language)
            ->where('artists_id', $artist->id)->get()->first()->description;


        $artistName = $artist->name;
        $artistImage = $artist->main_image;


        $next = DB::table('artists')->where('id', '>', $artist->id)->min('id');


        $nextName = '';

        if ($next) {
            $nextName = DB::table('artists')->where('id', $next)->get()->first()->name;
            $nextName = str_replace(' ', '-', $nextName);
            $nextName = str_replace('/[^A-Za-z0-9\-\']/', '-', $nextName);
        }

        $previous = DB::table('artists')->where('id', '<', $artist->id)->max('id');

        $previousName = '';

        if ($previous) {
            $previousName = DB::table('artists')->where('id', $previous)->get()->first()->name;
            $previousName = str_replace(' ', '-', $previousName);
            $previousName = str_replace('/[^A-Za-z0-9\-\']/', '-', $previousName);
        }

        return view('frontend.artist', compact('artist', 'artistName', 'artistImage', 'artistDescription', 'next', 'previous', 'nextName', 'previousName'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
