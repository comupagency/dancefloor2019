<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use App;
use DB;

use Illuminate\Http\Request;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
        $language = DB::table('languages')->where('name', App::getLocale())->get()->first()->id;

        $news = DB::table('news')->orderBy('id', 'desc')->paginate(5);

        return view('frontend.news', compact('news', 'language'));
    }


    public function singleNew($id){

        $language = DB::table('languages')->where('name', App::getLocale())->get()->first()->id;
        
        $new = DB::table('news')->where('id', $id)->get()->first();

        $newsTtranlastions = DB::table('news_translations')->where('news_id', $new->id)
        ->where('language_id', $language)->get()->first();

        $nextId = DB::table('news')->where('id', '>',  $id)->min('id');
        $previousId = DB::table('news')->where('id', '<',  $id)->max('id');

        return view('frontend.new', compact('new', 'newsTtranlastions', 'language', 'previousId', 'nextId'));

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
