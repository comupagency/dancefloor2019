<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Admin;
use Auth;
use Mail; 

class HourlyUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'hour:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send an hourly email to all the users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Starting command');

        $admin = new Admin();
        $admin->firstName ='name';
        $admin->lastName = 'lastname';
        $admin->email = 'teste@comup.pt';
        $admin->password = bcrypt('teste123');
        $admin->type ='1';

        $admin->save();
        
        $this->info('TESTE');
    
    }
}
