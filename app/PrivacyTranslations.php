<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PrivacyTranslations extends Model
{
    protected $fillable = ['description'];

    protected $table = 'privacy_translations';
}
