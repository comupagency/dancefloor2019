<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TicketsCategoriesTranslations extends Model
{
    protected $table = 'tickets_categories_translations';

}
