<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RegulationsTranslations extends Model
{
    protected $table = 'regulations_translations';

}
