# Box-Moments [![Framework](https://img.shields.io/badge/Laravel-v5.7.20-green.svg)](https://packagist.org/packages/laravel/framework) [![License](https://poser.pugx.org/laravel/framework/license)](https://packagist.org/packages/laravel/framework)

This repo contains the source code of Box-Moments website.

## Example

<img style="max-width:100%; height: auto" src="https://github.com/fabiommfernandes/Dancefloor/blob/master/public/img/logo.png">

Multi page website with the following content:

- Home
- Info
- Press
- Tickets
- Partners
- News
- Store

## About

This project uses our [backend](https://github.com/fabiommfernandes/Laravel-Backend). To run this project it's advised to check the git page of our backend.

## Installation

Install this package by cloning this repository and install like you normally install Laravel.

- Run `composer install` and `npm install`
- Run `npm run dev` to generate assets
- Copy `.env.example` to `.env` and fill your values (`php artisan key:generate`, database, etc)
- Run `php artisan migrate --seed`
- Run `php artisan serve`.
- Open the website in your browser and you're done.

## Support

This website is tailormade to our needs, for our personal use on a daily basis.
We do not follow [semver](http://semver.org) for this project and do not provide support whatsoever. However if you're a bit familiar with Laravel you should easily find your way.

For more details on the project, feel free to contact us.

## Developed by

- [Fábio Fernandes](https://github.com/fabiomiguelmfernandes)
- [Ricardo Guerreiro](https://github.com/rguerreiro7)

## License

This project and the Laravel framework are open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
