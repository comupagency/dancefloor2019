document.addEventListener("fullscreenchange", function() {
    if (!document.fullscreenElement) jQuery('#youtube_iframe').remove();
  }, false);
  
  document.addEventListener("msfullscreenchange", function() {
    if (!document.msFullscreenElement) jQuery('#youtube_iframe').remove();
  }, false);
  
  document.addEventListener("mozfullscreenchange", function() {
    if (!document.mozFullScreen) jQuery('#youtube_iframe').remove();
  }, false);
  
  document.addEventListener("webkitfullscreenchange", function() {
    if (!document.webkitIsFullScreen) jQuery('#youtube_iframe').remove();
  }, false);
  
  if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
      jQuery('.youtube__popup')[0].href = "https://www.youtube.com/watch?v=lEv4PkOcJUA";
  }else{
      jQuery( ".youtube__popup" ).click(function() {
  
          if (!Element.prototype.requestFullscreen) {
              Element.prototype.requestFullscreen = Element.prototype.mozRequestFullscreen || Element.prototype.webkitRequestFullscreen || Element.prototype.msRequestFullscreen;
          }
  
          // Check if clicked element is a video thumbnail
          var videoId = this.getAttribute('data-video');
  
          // Create iframe
          var iframe = document.createElement('div');
          iframe.innerHTML = '<p>x</p><iframe id="youtube_iframe" width="560" height="315" src="https://www.youtube.com/embed/' + videoId + '?rel=0&autoplay=1&modestbranding=1" frameborder="0" allow="autoplay; encrypted-media"></iframe>';
          var video = iframe.childNodes[1];
  
          // Replace the image with the video
          jQuery('body').append(video);
  
          // Enter fullscreen mode
          video.requestFullscreen();
      });
  }