$(function() {
    var header = $(".header");
    $(window).scroll(function() {    
        var scroll = $(window).scrollTop();
    
        if (scroll >= 50) {
            header.addClass("header-scroll");
        } else {
            header.removeClass("header-scroll");
        }
    });
});