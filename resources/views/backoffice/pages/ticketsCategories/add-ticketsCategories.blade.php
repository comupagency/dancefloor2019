@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Tickets Categories</h1>
@stop

@section('content')
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Add new category</h3>
    </div>
    <form enctype='multipart/form-data' role="form" method="post" action="{{ action('backoffice\TicketsCategoriesController@store') }}">
        {{ csrf_field() }}  
        <div class="box-body">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-file-text-o"></i></span>
                <input type="text" class="form-control" name="name-pt" placeholder="Name PT" required="" data-parsley-errors-container="#errorName" data-parsley-error-message="Name is required">
            </div>
            <div id="errorName" name="errordiv1" class="error-span"></div>
        </div>
        <div class="box-body">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-file-text-o"></i></span>
                <input type="text" class="form-control" name="name-en" placeholder="Name EN" required="" data-parsley-errors-container="#errorNameEn" data-parsley-error-message="Name is required">
            </div>
            <div id="errorNameEn" name="errordiv1" class="error-span"></div>
        </div>


        <div class="box-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
</div>



<style>

</style>


<script>
jQuery( document ).ready(function() {
    
    jQuery("form").parsley();

});
</script>
@stop