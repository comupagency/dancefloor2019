@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Ticket Categories</h1>
@stop

@section('content')

<div class="box">
    <div class="box-header">
      <h3 class="box-title">Ticket Categories</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <table id="example1" class="table table-bordered table-striped table-hover">
        <thead>
          <tr>
            <th>#</th>
            <th>Name PT</th>
            <th>Name EN</th>
          </tr>
        </thead>
        <tbody>
            @foreach ($categories as $category)
                <tr data-toggle="collapse" data-target="#{{ $category->id }}" role="button">
                    <td>
                        {{ $category->id }}
                    </td>
                    <td>
                        @php 
                            if($categoriesTranslations->where('ticketscategories_id', $category->id)->where('language_id', '1')->first()){
                                echo $categoriesTranslations->where('ticketscategories_id', $category->id)->where('language_id', '1')->first()->name; 
                            }
                        @endphp
                        <div class="collapse expand" id="{{ $category->id }}">
                            <div class="card card-body">
                                <a href="{{ route('admin.ticketscategories.edit', ['id' => $category->id ])}}"> 
                                    <i class="fa fa-edit blue-square"></i> 
                                </a> 
                                
                                <a href="{{ route('admin.ticketscategories.delete', ['id' => $category->id ])}}" onclick="return confirm('Do you want to delete this artist?')"> 
                                    <i class="fa fa-close red-square"></i> 
                                </a> 
                            </div>
                        </div>
                    </td>
                    <td class="text-center">
                        @php    
                            if($categoriesTranslations->where('ticketscategories_id', $category->id)->where('language_id', '2')->first()){
                                echo $categoriesTranslations->where('ticketscategories_id', $category->id)->where('language_id', '2')->first()->name; 
                            }
                        @endphp
                    </td>
                </tr>
            @endforeach
        </tbody>
        <tfoot>
          <tr>
            <th>#</th>
            <th>Name PT</th>
            <th>Name EN</th>
          </tr>
        </tfoot>
      </table>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->
</div>
<!-- /.col -->
</div>

{!! Toastr::message() !!}

@stop