@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Privacy Policies</h1>
    
@stop

@section('content')
<div class="box-header">
    <a onclick="enableInputs()"> 
        <i class="fa fa-edit blue-square"></i> 
    </a>
</div>


<form id="form" enctype='multipart/form-data' role="form" method="post" action="{{ action('backoffice\PrivacyController@update') }}">
  {{ csrf_field() }}  
  <div class="box scroll">
    <!-- /.box-header -->
    <div class="box-body descriptionFirst">
        <h5>PT: </h5>
        <div class="input-group">
            @if(isset($privacyPT[0]))
                {!! $privacyPT[0]->description !!}
            @endif 
        </div>

        <hr>
        
        <h5>EN: </h5>
        <div class="input-group">
            @if(isset($privacyEN[0]))
                {!! $privacyEN[0]->description !!}
            @endif 
        </div>

        <hr>
        
        <h5>ES: </h5>
        <div class="input-group">
            @if(isset($privacyES[0]))
                {!! $privacyES[0]->description !!}
            @endif 
        </div>
    </div>

    <div class="input-group width100 descriptionShow" style="display: none;">
        <h5>Description - PT</h5>
        <textarea id="elm1" name="description-pt" id="description" class="form-control">
            @if(isset($privacyPT[0]))
                {!! $privacyPT[0]->description !!}
            @endif 
        </textarea>
    </div>


    <div class="input-group width100 descriptionShow mt-5" style="display: none;">
        <h5>Description - EN</h5>
        <textarea id="elm2" name="description-en" id="description" class="form-control">
            @if(isset($privacyEN[0]))
                {!! $privacyEN[0]->description !!}
            @endif 
        </textarea>
    </div>

    <div class="input-group width100 descriptionShow mt-5" style="display: none;">
        <h5>Description - ES</h5>
        <textarea id="elm3" name="description-en" id="description" class="form-control">
            @if(isset($privacyES[0]))
                {!! $privacyES[0]->description !!}
            @endif 
        </textarea>
    </div>


    @if(isset($privacy))
        <input type="hidden" name="id" value="{{ $privacy->id }}">  
    @else
        <input type="hidden" name="id" value="1">
    @endif
    <!-- /.box-body -->
  </div>
  <div class="box-footer submit" style="display: none;">
      <button type="submit" class="btn btn-primary">Submit</button>
  </div>
  <!-- /.box -->
  </div>
  <!-- /.col -->
  </div>
</form>

{!! Toastr::message() !!}

<script>


function enableInputs(){
    if(jQuery('.submit').is(':visible')){

        jQuery('.descriptionFirst').css('display','block');
        jQuery('.scroll').css('overflow', 'scroll');
        jQuery('.scroll').css('height', '500px');
        jQuery('.descriptionShow').css('display','none');
        jQuery('.submit').css('display','none');



    }else{
        jQuery('.descriptionFirst').css('display','none');
        jQuery('.scroll').css('overflow', 'hidden');
        jQuery('.scroll').css('height', 'auto');
        jQuery('.descriptionShow').css('display','block');
        jQuery('.submit').css('display','block');
    }

}

</script>

@stop