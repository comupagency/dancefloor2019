@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Sponsor Categories</h1>
@stop

@section('content')

<div class="box">
    <div class="box-header">
      <h3 class="box-title">Sponsor Categories</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <table id="example1" class="table table-bordered table-striped table-hover">
        <thead>
            <tr>
                <th>#</th>
                <th>name</th>
                <th>description PT</th>
                <th>description EN</th>
                <th>logo</th>
                <th>category</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($sponsors as $sc)
            
              <tr data-toggle="collapse" data-target="#{{ $sc->id }}" role="button">
                  <td>
                    {{$sc->id}}
                  </td>
                  <td>
                      {{ $sc->name }}
                      <div class="collapse expand" id="{{ $sc->id }}">
                            <div class="card card-body">
                                <a href="{{ route('admin.sponsors.edit', ['id' => $sc->id ])}}"> 
                                  <i class="fa fa-edit blue-square"></i> 
                                </a> 
                                <a href="{{ route('admin.sponsors.delete', ['id' => $sc->id ])}}" onclick="return confirm('Do you want to delete this Sponsor?')"> 
                                  <i class="fa fa-close red-square"></i> 
                                </a> 
                            </div>
                        </div>
                  </td>
                  <td>
                      
                      @php 
                        if($sponsorsTranslations->where('sponsors_id', $sc->id)->where('language_id', '1')->first()){
                          echo $sponsorsTranslations->where('sponsors_id', $sc->id)->where('language_id', '1')->first()->description; 
                        }
                      @endphp
                  
                  </td>
                  <td>
                    @php 
                      if($sponsorsTranslations->where('sponsors_id', $sc->id)->where('language_id','2')->first()){
                        echo $sponsorsTranslations->where('sponsors_id', $sc->id)->where('language_id','2')->first()->description;
                      }
                    @endphp                  
                </td>
                <td>
                        @php
                        if( $sc->logo ) {
                            echo '<img src="'. $sc->logo .'" height="100" width="100">';

                        }else{
                            echo '<img src="http://lorempixel.com/100/100/cats" height="100" width="100">';
                        }
                        @endphp
                    <!-- show logo thumbnail -->
                </td>
                <td>
                    @php 
                        if($sponsorsCategoriesTranslations->where('sponsorscategories_id', $sc->category_id)->first()){
                            echo $sponsorsCategoriesTranslations->where('sponsorscategories_id', $sc->category_id)->first()->name;
                        }
                    @endphp 
                </td>
              </tr>
            @endforeach
        </tbody>
        <tfoot>
          <tr>
            <th>#</th>
            <th>name</th>
            <th>description PT</th>  
            <th>description EN</th>
            <th>logo</th>
            <th>category</th>
          </tr>
        </tfoot>
      </table>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->
</div>
<!-- /.col -->
</div>

{!! Toastr::message() !!}

@stop