@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Sponsor Categories</h1>
@stop

@section('content')

<div class="box">
    <div class="box-header">
      <h3 class="box-title">Sponsor Categories</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <table id="example1" class="table table-bordered table-striped table-hover">
        <thead>
          <tr>
            <th>#</th>
            <th>Name PT</th>
            <th>Name EN</th>
            <th>Order</th>
          </tr>
        </thead>
        <tbody>
            @foreach ($sponsorsCategories as $sc)
              <tr data-toggle="collapse" data-target="#{{ $sc->id }}" role="button">
                  <td>
                    {{$sc->id}}
                  </td>
                  <td>                      
                      @php 
                        if($sponsorsCategoriesTranslations->where('sponsorscategories_id', $sc->id)->where('language_id', '1')->first()){
                          echo $sponsorsCategoriesTranslations->where('sponsorscategories_id', $sc->id)->where('language_id', '1')->first()->name; 
                        }
                      @endphp
                      <div class="collapse expand" id="{{ $sc->id }}">
                          <div class="card card-body">
                              <a href="{{ route('admin.sponsorscategories.edit', ['id' => $sc->id ])}}"> 
                                <i class="fa fa-edit blue-square"></i> 
                              </a> 
                              <a href="{{ route('admin.sponsorscategories.delete', ['id' => $sc->id ])}}" onclick="return confirm('Do you want to delete this Category?')"> 
                                <i class="fa fa-close red-square"></i> 
                              </a> 
                          </div>
                      </div>
                  </td>
                  <td>
                    @php 
                      if($sponsorsCategoriesTranslations->where('sponsorscategories_id', $sc->id)->where('language_id','2')->first()){
                        echo $sponsorsCategoriesTranslations->where('sponsorscategories_id', $sc->id)->where('language_id','2')->first()->name;
                      }
                    @endphp                  
                  </td>
                  <td>
                      {{ $sc->order }}
                  </td>
              </tr>
            @endforeach
        </tbody>
        <tfoot>
          <tr>
              <th>#</th>
              <th>Name PT</th>
              <th>Name EN</th>
              <th>Order</th>
          </tr>
        </tfoot>
      </table>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->
</div>
<!-- /.col -->
</div>

{!! Toastr::message() !!}

@stop