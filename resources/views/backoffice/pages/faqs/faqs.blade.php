@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Faqs</h1>
@stop

@section('content')

<div class="box">
    <div class="box-header">
      <h3 class="box-title">Faqs</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <table id="example1" class="table table-bordered table-striped table-hover">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Question PT</th>
                            <th scope="col">Answer PT</th>
                            <th scope="col">Question EN</th>
                            <th scope="col">Answer EN</th>
                        </tr>
                    </thead>
                    @foreach ($faqs as $faq)
                        <tbody>
                            <tr data-toggle="collapse" data-target="#{{ $faq->id }}" role="button">
                                <td>
                                    {{$faq->id}}
                                </td>
                                <td>                      
                                    @php 
                                        if($faqsTranslations->where('faqs_id', $faq->id)->where('language_id', '1')->first()){
                                        echo $faqsTranslations->where('faqs_id', $faq->id)->where('language_id', '1')->first()->question; 
                                        }
                                    @endphp
                                    <div class="collapse expand" id="{{ $faq->id }}">
                                        <div class="card card-body">
                                            <a href="{{ route('admin.faqs.edit', ['id' => $faq->id ])}}"> 
                                                <i class="fa fa-edit blue-square"></i> 
                                            </a> 
                                            <a href="{{ route('admin.faqs.delete', ['id' => $faq->id ])}}" onclick="return confirm('Do you want to delete this Faq?')"> 
                                                <i class="fa fa-close red-square"></i> 
                                            </a> 
                                        </div>
                                    </div>
                                </td>
                                <td>                      
                                    @php 
                                        if($faqsTranslations->where('faqs_id', $faq->id)->where('language_id', '1')->first()){
                                            echo $faqsTranslations->where('faqs_id', $faq->id)->where('language_id', '1')->first()->answer; 
                                        }
                                    @endphp
                                </td>
                                <td>                      
                                    @php 
                                        if($faqsTranslations->where('faqs_id', $faq->id)->where('language_id', '2')->first()){
                                            echo $faqsTranslations->where('faqs_id', $faq->id)->where('language_id', '2')->first()->question; 
                                        }
                                    @endphp
                                </td>
                                <td>                      
                                    @php 
                                        if($faqsTranslations->where('faqs_id', $faq->id)->where('language_id', '2')->first()){
                                            echo $faqsTranslations->where('faqs_id', $faq->id)->where('language_id', '2')->first()->answer; 
                                        }
                                    @endphp
                                </td>
                            </tr>
                        </tbody>
                    @endforeach
                        <tfoot>
                            <tr class="thead-dark">
                                <th scope="col">#</th>
                                <th scope="col">Question PT</th>
                                <th scope="col">Answer PT</th>
                                <th scope="col">Question EN</th>
                                <th scope="col">Answer EN</th>
                            </tr>
                        </tfoot>
                </table>

            </div>
        </section>
        
    </div>
    </div>

    


</div>
{!! Toastr::message() !!}


@endsection