@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Sponsors</h1>
@stop

@section('content')


<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Add new Sponsor</h3>
    </div>
    
    <div class="tab-form-wrap" role="tabpanel">

        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#one" aria-controls="one" role="tab" data-toggle="tab">1</a> Step One | General Information</li>
            <li role="presentation"><a href="#two" aria-controls="two" role="tab" data-toggle="tab">2</a> Step Two | Multi-Language Content</li>
        </ul>


        <form enctype='multipart/form-data' role="form" method="post" action="{{ action('backoffice\RegulationsController@update') }}">
            {{ csrf_field() }}  
    
        <!-- Tab panes -->
        <div class="tab-content">
  
                    <div role="tabpanel" class="tab-pane fade in active" id="one">
                        <h3>Base Info</h3>
                        <fieldset >
                            <span class="input-label">Name PT</span>
                        <input class="input-clear" name="name-pt" role="text-role" type="text" value="{{ $regulationPt->title}}"  placeholder="Name PT" required>
                            <span class="error-span" style="display: none; "> Please insert a name PT</span>
                        </fieldset>

                        <fieldset >
                            <span class="input-label">Name EN</span>
                            <input class="input-clear" name="name-en" role="text-role" type="text"  value="{{ $regulationEn->title}}"  placeholder="Name EN" required>
                            <span class="error-span" style="display: none; "> Please insert a name EN</span>
                        </fieldset>
                    </div>

                    <div role="tabpanel" class="tab-pane fade" id="two">
                        <fieldset >
                            <span class="input-label">Description PT</span>
                            <textarea id="elm1" name="description-pt" class="form-control" required=""> {{ $regulationPt->description}} </textarea>
                            <span class="error-span" style="display: none; ">  Please insert a description in Portuguese</span>
                        </fieldset>

                        <fieldset >
                            <span class="input-label">Description EN</span>
                            <textarea id="elm2" name="description-en" class="form-control" required=""> {{ $regulationEn->description}} </textarea>
                            <span class="error-span" style="display: none; ">Please insert a description in English</span>
                        </fieldset>
                    </div>

                    <input type="hidden" name="id" id="id" value="{{ $regulation->id }}">

                
                    <a id="nextBtn" class="blk-btn btnNext">next step</a>
                    <button type="submit" id="submitBtn" class="blk-btn btnNext hidden">Submit</button>
        </div>
    </form>
    </div>
</div>


<script>

$(function() {
    var $this = $(this);
    let formInputs;
    let formSelects; 
    let textAreas;
    let emptyFields = 0;
    let submitBtn = document.getElementById('submitBtn')
    let nextBtn = document.getElementById('nextBtn')

    // Setup next button
    $('.btnNext').on('click', function() {        

        //get form elements + create error counter
        let form = document.querySelectorAll('div[role="tabpanel"].active');
        formInputs = form[0].querySelectorAll('input');
        formSelects = form[0].querySelectorAll('select');
        textAreas = form[0].querySelectorAll('textarea'); 
        emptyFields = 0; 
        
        validateInputs(emptyFields);
        validateSelects(formSelects, emptyFields);
        validateTextArea(textAreas, emptyFields); 

        if(emptyFields == 0 ){
            nextTab(); 
        }else{
            //do nothing
        }
        
    });
    
    function validateInputs(){
        formInputs.forEach(function (currentValue, currentIndex){
            if(currentValue.hasAttribute('required')) {
                let parent = currentValue.parentElement; 
                
                //check text inputs
                if(currentValue.value === ''){
                    //increment error counter
                    emptyFields++; 
                    //add error css
                    parent.classList.add('validation-error');
                    parent.lastElementChild.style.display='block'
                    
                }else{
                    
                    if( parent.classList.contains('validation-error') ) {
                        parent.classList.remove('validation-error')
                        parent.lastElementChild.style.display='none'
                    }
                }
                //end of check text inputs

            }
        }); 
    }

    function validateSelects(){
        formSelects.forEach(function (currentValue, currentIndex){
            if(currentValue.hasAttribute('required')) {
                let parent = currentValue.parentElement; 
                //check text inputs
                if(currentValue.value === ''){
                    //increment error counter
                    emptyFields++; 
                    //add error css
                    parent.classList.add('validation-error');
                }else{
                    
                    if( parent.classList.contains('validation-error') ) {
                        parent.classList.remove('validation-error')
                    }
                }
                //end of check text inputs
            }
        }); 
    }
    function validateTextArea() {
        textAreas.forEach(function (currentValue, currentIndex){
            if(currentValue.hasAttribute('required')) {
                let parent = currentValue.parentElement; 
                //check text inputs
                tinyMCE.triggerSave();

                if(currentValue.value === ''){
                    //increment error counter
                    emptyFields++; 
                    //add error css
                    parent.classList.add('validation-error');
                    parent.lastElementChild.style.display='block'
                }else{
                    
                    if( parent.classList.contains('validation-error') ) {
                        parent.classList.remove('validation-error')
                        parent.lastElementChild.style.display='none'
                    }
                }
                //end of check text inputs
            }
        }); 
    }



        $('.tab-form-wrap .tab-content fieldset').on('click', function() {
            $this.children('.input-clear').focus();
        });
        
        $('.tab-form-wrap .tab-content fieldset .input-clear')
            .on('focus', function() {
            $this.parent().addClass('focus');
        })
            .on('focusout', function() {
            $this.parent().removeClass('focus');
        });


    });


    function nextTab() {
        var e = $('ul[role="tablist"] li.active').next().find('a[data-toggle="tab"]');  
        if(e.length > 0) e.click();  
        isLastTab();
    }

    function isLastTab() {
        var e = $('ul[role="tablist"] li:last').hasClass('active'); 

 

        if( e ){
            submitBtn.classList.remove('hidden');
            nextBtn.classList.add('hidden');
            
        }else{
            submitBtn.classList.add('hidden');
            nextBtn.classList.remove('hidden');

        } 
        return e;
    }
    jQuery( document ).ready(function() {
        


});



</script>


@stop