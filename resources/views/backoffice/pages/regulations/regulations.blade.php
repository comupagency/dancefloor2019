@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Regulations</h1>
@stop

@section('content')

<div class="box">
    <div class="box-header">
      <h3 class="box-title">Regulations</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <table id="example1" class="table table-bordered table-striped table-hover">
        <thead>
            <tr>
                <th>#</th>
                <th>title PT</th>
                <th>title EN</th>
                <th>description PT</th>  
                <th>description EN</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($regulations as $reg)
            
              <tr data-toggle="collapse" data-target="#{{ $reg->id }}" role="button">
                  <td>
                    {{$reg->id}}
                  </td>
                  <td>
                    @php 
                        if($regulationsTranslations->where('regulations_id', $reg->id)->where('language_id', '1')->first()){
                          echo $regulationsTranslations->where('regulations_id', $reg->id)->where('language_id', '1')->first()->title; 
                        }
                    @endphp
                      <div class="collapse expand" id="{{ $reg->id }}">
                            <div class="card card-body">
                                <a href="{{ route('admin.regulations.edit', ['id' => $reg->id ])}}"> 
                                  <i class="fa fa-edit blue-square"></i> 
                                </a> 
                                <a href="{{ route('admin.regulations.delete', ['id' => $reg->id ])}}" onclick="return confirm('Do you want to delete this Regulation?')"> 
                                  <i class="fa fa-close red-square"></i> 
                                </a> 
                            </div>
                        </div>
                  </td>
                  <td>
                    @php 
                        if($regulationsTranslations->where('regulations_id', $reg->id)->where('language_id', '2')->first()){
                          echo $regulationsTranslations->where('regulations_id', $reg->id)->where('language_id', '2')->first()->title; 
                        }
                    @endphp
                </td>
                  <td>       
                    <button type="button" class="btn btn-link btn-lg" data-toggle="modal" data-target="#{{ $reg->id }}pt">Preview</button>                    
                    <div class="modal fade" id="{{ $reg->id }}pt" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                        @php 
                                        if($regulationsTranslations->where('regulations_id', $reg->id)->where('language_id', '1')->first()){
                                        echo $regulationsTranslations->where('regulations_id', $reg->id)->where('language_id', '1')->first()->description; 
                                        }
                                    @endphp
                                </div>
                            </div>
                        </div>
                    </div>                          
                  </td>
                  <td>
                    <button type="button" class="btn btn-link btn-lg" data-toggle="modal" data-target="#{{ $reg->id }}en">Preview</button>                    
                    <div class="modal fade" id="{{ $reg->id }}en" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                        @php 
                                        if($regulationsTranslations->where('regulations_id', $reg->id)->where('language_id', '2')->first()){
                                        echo $regulationsTranslations->where('regulations_id', $reg->id)->where('language_id', '2')->first()->description; 
                                        }
                                    @endphp
                                </div>
                            </div>
                        </div>
                    </div>                        
                </td>
              </tr>
            @endforeach
        </tbody>
        <tfoot>
          <tr>
            <th>#</th>
            <th>title PT</th>
            <th>title EN</th>
            <th>description PT</th>  
            <th>description EN</th>
          </tr>
        </tfoot>
      </table>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->
</div>
<!-- /.col -->
</div>

{!! Toastr::message() !!}

@stop