@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>News</h1>
@stop

@section('content')

<div class="box">
    <div class="box-header">
      <h3 class="box-title">News</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <table id="example1" class="table table-bordered table-striped table-hover">
        <thead>
            <tr>
                <th>#</th>
                <th>Title PT</th>
                <th>Title EN</th>
                <th>Title ES</th>
                <th>Subtitle PT</th>
                <th>Subtitle EN</th>
                <th>Subtitle ES</th>
                <th>Description PT</th>
                <th>Description EN</th>
                <th>Description ES</th>
                <th>Thumbnail</th>
                <th>Main Image</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($news as $new)
                <tr data-toggle="collapse" data-target="#{{ $new->id }}" role="button">
                    <td>
                        {{ $new->id }}
                    </td>
                    <td>
                        @php 
                            if( $newsTranslations->where('news_id', $new->id)->where('language_id', '1')->first() ){
                                echo $newsTranslations->where('news_id', $new->id)->where('language_id', '1')->first()->title;
                            }else{
    
                            }
                        @endphp
                        <div class="collapse expand" id="{{ $new->id }}">
                            <div class="card card-body">
                                <a href="{{ route('admin.news.edit', ['id' => $new->id ])}}"> 
                                <i class="fa fa-edit blue-square"></i> 
                                </a> 
                                
                                <a href="{{ route('admin.news.delete', ['id' => $new->id ])}}" onclick="return confirm('Do you want to delete this news?')"> 
                                <i class="fa fa-close red-square"></i> 
                                </a> 
                            </div>
                        </div>
                    </td>
                    <td>
                        @php 
                            if( $newsTranslations->where('news_id', $new->id)->where('language_id', '2')->first() ){
                                echo $newsTranslations->where('news_id', $new->id)->where('language_id', '2')->first()->title;
                            }else{

                            }
                        @endphp
                    </td>

                    <td>
                        @php 
                            if( $newsTranslations->where('news_id', $new->id)->where('language_id', '3')->first() ){
                                echo $newsTranslations->where('news_id', $new->id)->where('language_id', '3')->first()->title;
                            }else{
                                echo "tst"; 
                            }
                        @endphp
                    </td>
                    <td>
                        @php 
                            if( $newsTranslations->where('news_id', $new->id)->where('language_id', '1')->first() ){
                                echo $newsTranslations->where('news_id', $new->id)->where('language_id', '1')->first()->subtitle;
                            }else{

                            }
                        @endphp
                    </td>
                    <td>
                        @php 
                            if( $newsTranslations->where('news_id', $new->id)->where('language_id', '2')->first() ){
                                echo $newsTranslations->where('news_id', $new->id)->where('language_id', '2')->first()->subtitle;
                            }else{

                            }
                        @endphp
                    </td>
                    <td>
                        @php 
                            if( $newsTranslations->where('news_id', $new->id)->where('language_id', '3')->first() ){
                                echo $newsTranslations->where('news_id', $new->id)->where('language_id', '3')->first()->subtitle;
                            }else{

                            }
                        @endphp
                    </td>
                    <td>
                        <button type="button" class="btn btn-link btn-lg" data-toggle="modal" data-target="#{{ $new->id }}-pt">Preview</button>                    
                        <div class="modal fade" id="{{ $new->id }}-pt" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        @php 
                                            if($newsTranslations->where('news_id', $new->id)->where('language_id', '1')->first()){
                                                echo $newsTranslations->where('news_id', $new->id)->where('language_id', '1')->first()->description; 
                                            }
                                        @endphp
                                    </div>
                                </div>
                            </div>
                        </div>                        
                    </td>

                    <td>
                        <button type="button" class="btn btn-link btn-lg" data-toggle="modal" data-target="#{{ $new->id }}-en">Preview</button>                    
                        <div class="modal fade" id="{{ $new->id }}-en" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                            @php 
                                            if($newsTranslations->where('news_id', $new->id)->where('language_id', '2')->first()){
                                                echo $newsTranslations->where('news_id', $new->id)->where('language_id', '2')->first()->description; 
                                            }
                                        @endphp
                                    </div>
                                </div>
                            </div>
                        </div>                        
                    </td>
                    <td>
                        <button type="button" class="btn btn-link btn-lg" data-toggle="modal" data-target="#{{ $new->id }}-es">Preview</button>                    
                        <div class="modal fade" id="{{ $new->id }}-es" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                            @php 
                                            if($newsTranslations->where('news_id', $new->id)->where('language_id', '3')->first()){
                                                echo $newsTranslations->where('news_id', $new->id)->where('language_id', '3')->first()->description; 
                                            }
                                        @endphp
                                    </div>
                                </div>
                            </div>
                        </div>                        
                    </td>

                    <td>
                        @php
                        if( $new->thumbnail ) {
                            echo '<img src="'. $new->thumbnail .'" height="100" width="100">';

                        }else{
                            echo '<img src="/images/news/no-image/icon.png" height="100" width="100">';
                        }
                        @endphp
                        <!-- show logo thumbnail -->
                    </td>
                    
                    <td>
                        @php
                        if( $new->main_image ) {
                            echo '<img src="'. $new->main_image .'" height="100" width="100">';

                        }else{
                            echo '<img src="/images/news/no-image/icon.png" height="100" width="100">';
                        }
                        @endphp
                        <!-- show logo thumbnail -->
                    </td>
                </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <th>#</th>
                <th>Title PT</th>
                <th>Title EN</th>
                <th>Title ES</th>
                <th>Subtitle PT</th>
                <th>Subtitle EN</th>
                <th>Subtitle ES</th>
                <th>Description PT</th>
                <th>Description EN</th>
                <th>Description ES</th>
                <th>Thumbnail</th>
                <th>Main Image</th>
            </tr>
        </tfoot>
    </table>
    </div>
    <!-- /.box-body -->
    </div>
<!-- /.box -->
</div>
<!-- /.col -->
</div>

{!! Toastr::message() !!}

@stop