@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Tickets</h1>
@stop

@section('content')

<div class="box">
    <div class="box-header">
      <h3 class="box-title">Tickets</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <table id="example1" class="table table-bordered table-striped table-hover">
        <thead>
          <tr>
            <th>#</th>
            <th>Name PT</th>
            <th>Name EN</th>
            <th>Category PT</th>
            <th>Category EN</th>
            <th>Price</th>
            <th>Link</th>
            <th>Image</th>
          </tr>
        </thead>
        <tbody>
            @foreach ($tickets as $ticket)
                <tr data-toggle="collapse" data-target="#{{ $ticket->id }}" role="button">
                    <td>
                        {{ $ticket->id }}
                    </td>
                    <td>
                        @php 
                            if($ticketsTranslations->where('tickets_id', $ticket->id)->where('language_id', '1')->first()){
                                echo $ticketsTranslations->where('tickets_id', $ticket->id)->where('language_id', '1')->first()->name; 
                            }
                        @endphp
                        <div class="collapse expand" id="{{ $ticket->id }}">
                            <div class="card card-body">
                                <a href="{{ route('admin.tickets.edit', ['id' => $ticket->id ])}}"> 
                                    <i class="fa fa-edit blue-square"></i> 
                                </a> 
                                
                                <a href="{{ route('admin.tickets.delete', ['id' => $ticket->id ])}}" onclick="return confirm('Do you want to delete this artist?')"> 
                                    <i class="fa fa-close red-square"></i> 
                                </a> 
                            </div>
                        </div>
                    </td>
                    <td class="text-center">
                        @php 
                            if($ticketsTranslations->where('tickets_id', $ticket->id)->where('language_id', '2')->first()){
                                echo $ticketsTranslations->where('tickets_id', $ticket->id)->where('language_id', '2')->first()->name; 
                            }
                        @endphp
                    </td>
                    <td class="text-center">
                        @php
                            if($categoriesTranslations->where('ticketscategories_id', $ticket->category_id)->where('language_id', '1')->first()){
                                echo $categoriesTranslations->where('ticketscategories_id', $ticket->category_id)->where('language_id', '1')->first()->name; 
                            }
                        @endphp
                    </td>
                    <td class="text-center">
                        @php
                            if($categoriesTranslations->where('ticketscategories_id', $ticket->category_id)->where('language_id', '1')->first()){
                                echo $categoriesTranslations->where('ticketscategories_id', $ticket->category_id)->where('language_id', '1')->first()->name; 
                            }
                        @endphp
                    </td>

                    <td class="text-center">
                        {{ $ticket->price }}€
                    </td>

                    <td class="text-center">
                        {{ $ticket->link }}€
                    </td>

                    <td>
                            @php
                            if( $ticket->main_image ) {
                                echo '<img src="'. $ticket->main_image .'" height="100" width="100">';
      
                            }else{
                                echo '<img src="http://lorempixel.com/100/100/cats" height="100" width="100">';
                            }
                            @endphp
                            <!-- show logo thumbnail -->
                        </td>
                </tr>
            @endforeach
        </tbody>
        <tfoot>
          <tr>
            <th>#</th>
            <th>Name PT</th>
            <th>Name EN</th>
            <th>Category PT</th>
            <th>Category EN</th>
            <th>Price</th>
            <th>Link</th>
            <th>Image</th>
          </tr>
        </tfoot>
      </table>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->
</div>
<!-- /.col -->
</div>

{!! Toastr::message() !!}

@stop