@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Tickets</h1>
@stop

@section('content')
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Edit ticket</h3>
    </div>
    <form enctype='multipart/form-data' role="form" method="post" action="{{ action('backoffice\TicketsController@update') }}">
        {{ csrf_field() }}  
        <div class="box-body">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-file-text-o"></i></span>
                <input type="text" class="form-control" name="name-pt" value="{{$ticketTranslationPt->name}}" required="" data-parsley-errors-container="#errorNamePt" data-parsley-error-message="Name is required">
            </div>
            <div id="errorNamePt" name="errordiv1" class="error-span"></div>
        </div>

        <div class="box-body">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-file-text-o"></i></span>
                <input type="text" class="form-control" name="name-en" value="{{$ticketTranslationEn->name}}" required="" data-parsley-errors-container="#errorNameEn" data-parsley-error-message="Name is required">
            </div>
            <div id="errorNameEn" name="errordiv1" class="error-span"></div>
        </div>

        <div class="box-body">
            <select class="form-control" name="category" required>
            <option value="{{$ticketCategory->ticketscategory_id}}" disabled selected value> {{$ticketCategory->name}} </option>
                @foreach($categories as $category)
                    <option value="{{ $category->id }}"> 
                        @php 
                            if( $categoriesTranslations->where('ticketscategories_id', $category->id)->where('language_id', '1')->first()){
                                echo $categoriesTranslations->where('ticketscategories_id', $category->id)->where('language_id', '1')->first()->name;
                            }
                        @endphp 
                    </option>
                @endforeach
            </select>
        </div>

        <div class="box-body">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-file-text-o"></i></span>
                <input type="number" class="form-control" name="price" value="{{$ticket->price}}" required="" data-parsley-errors-container="#errorPrice" data-parsley-error-message="Price is required">
            </div>
            <div id="errorPrice" name="errordiv1" class="error-span"></div>
        </div>

        <div class="box-body">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-file-text-o"></i></span>
                <input type="text" class="form-control" name="link" value="{{$ticket->link}}" required="" data-parsley-errors-container="#errorLink" data-parsley-error-message="Link is required">
            </div>
            <div id="errorLink" name="errordiv1" class="error-span"></div>
        </div>


        <div class="box-body">
                <div id="main-image">Main</div>
        </div>

        <input type="hidden" name="id" id="id" value="{{ $ticket->id }}">

        <div class="box-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
</div>



<style>

</style>


<script>
jQuery( document ).ready(function() {
    
    jQuery("form").parsley();

    function fileUploader(name,folder,maxFiles,acceptedTypes,folderName){

        jQuery("#"+name).uploadFile({
        url:'{{ action("backoffice\TicketsController@imageUpload") }}',
        fileName: name,
        acceptFiles: acceptedTypes,
        showPreview: true,
        maxFileCount: maxFiles,
        maxFileSize: '30000000',
        formData: {
            "_token":"{{ csrf_token() }}", 
            "name": name,
            "folder": "tmp"
        },
        previewHeight: "100px",
        previewWidth: "100px",
        showDelete: true,
        deleteCallback: function (imageName, action) {
            jQuery.post("{{ action('backoffice\TicketsController@imageDelete') }}", {
                action: "delete",
                imageName: imageName,
                "folder": "tmp",
                "name": name,
                _token:"{{ csrf_token() }}"},
                function (resp,textStatus, jqXHR) {
            });
        },
        onLoad:function(obj)
            {
                jQuery.ajax({
                    cache: false,
                    url:'{{ action("backoffice\TicketsController@imageLoad") }}',
                    dataType: "json",
                    data: {
                        "id": jQuery('#id').val(), 
                        "name": folderName
                    },
                    success: function(data) 
                    {
                        for(var i=0;i<data.length;i++)
                        { 
                            obj.createProgress(data[i]["name"],data[i]["path"],data[i]["size"]);
                        }
                    }
                });
            }, 
        }); 
    }

    jQuery("#main-image").onload = fileUploader('main-image','main-image',1,'image/*','main-image');

});
</script>
@stop