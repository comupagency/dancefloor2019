@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Tickets</h1>
@stop

@section('content')
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Add new ticket</h3>
    </div>
    <form enctype='multipart/form-data' role="form" method="post" action="{{ action('backoffice\TicketsController@store') }}">
        {{ csrf_field() }}  
        <div class="box-body">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-file-text-o"></i></span>
                <input type="text" class="form-control" name="name-pt" placeholder="Name PT" required="" data-parsley-errors-container="#errorNamePt" data-parsley-error-message="Name is required">
            </div>
            <div id="errorNamePt" name="errordiv1" class="error-span"></div>
        </div>

        <div class="box-body">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-file-text-o"></i></span>
                <input type="text" class="form-control" name="name-en" placeholder="Name EN" required="" data-parsley-errors-container="#errorNameEn" data-parsley-error-message="Name is required">
            </div>
            <div id="errorNameEn" name="errordiv1" class="error-span"></div>
        </div>

        <div class="box-body">
            <select class="form-control" name="category" required>
                <option disabled selected value>-- select an option --</option>
                @foreach($categories as $category)
                    <option value="{{ $category->id }}"> 
                        @php 
                            if( $categoriesTranslations->where('ticketscategories_id', $category->id)->where('language_id', '1')->first()){
                                echo $categoriesTranslations->where('ticketscategories_id', $category->id)->where('language_id', '1')->first()->name;
                            }
                        @endphp 
                    </option>
                @endforeach
            </select>
        </div>

        <div class="box-body">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-file-text-o"></i></span>
                <input type="number" class="form-control" name="price" placeholder="price" required="" data-parsley-errors-container="#errorPrice" data-parsley-error-message="Price is required">
            </div>
            <div id="errorPrice" name="errordiv1" class="error-span"></div>
        </div>

        <div class="box-body">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-file-text-o"></i></span>
                <input type="text" class="form-control" name="link" placeholder="link" required="" data-parsley-errors-container="#errorLink" data-parsley-error-message="link is required">
            </div>
            <div id="errorLink" name="errordiv1" class="error-span"></div>
        </div>

        <div class="box-body">
            <div id="main-image">Main</div>
            <input type="hidden" name="img" id="img" value="tst/img">
        </div>



        <div class="box-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
</div>



<style>

</style>


<script>
jQuery( document ).ready(function() {
    
    jQuery("form").parsley();

    
    function fileUploader(name,folder,maxFiles,acceptedTypes){
        jQuery("#"+name).uploadFile({
        url:'{{ action("backoffice\TicketsController@imageUpload") }}',
        fileName: name,
        acceptFiles: acceptedTypes,
        showPreview: true,
        maxFileCount: maxFiles,
        maxFileSize: '30000000',
        formData: {
            "_token":"{{ csrf_token() }}", 
            "name": name,
            "folder": folder
        },
        previewHeight: "100px",
        previewWidth: "100px",
        showDelete: true,
        deleteCallback: function (imageName, action) {
            jQuery.post("{{ action('backoffice\TicketsController@imageDelete') }}", {
                action: "delete",
                imageName: imageName,
                "folder": folder,
                "name": name,
                _token:"{{ csrf_token() }}"},
                function (resp,textStatus, jqXHR) {
            });
        },
    }); 
    }

    jQuery("#main-image").onload = fileUploader('main-image','tmp',1,'image/*');

});



</script>
@stop