@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Artists</h1>
@stop

@section('content')

<div class="box">
    <div class="box-header">
      <h3 class="box-title">Artists</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <table id="example1" class="table table-bordered table-striped table-hover">
        <thead>
          <tr>
            <th>Name</th>
            <th>Description</th>
            <th>Day</th>
            <th>Hours</th>
            <th>Main Image</th>
          </tr>
        </thead>
        <tbody>
            @foreach ($artists as $artist)
           
              <tr data-toggle="collapse" data-target="#{{ $artist->id }}" role="button">
                  <td>
                      {{ $artist->name }}
                      <div class="collapse expand" id="{{ $artist->id }}">
                          <div class="card card-body">
                              <a href="{{ route('admin.artists.edit', ['id' => $artist->id ])}}"> 
                                <i class="fa fa-edit blue-square"></i> 
                              </a> 
                              
                              <a href="{{ route('admin.artists.delete', ['id' => $artist->id ])}}" onclick="return confirm('Do you want to delete this artist?')"> 
                                <i class="fa fa-close red-square"></i> 
                              </a> 
                          </div>
                      </div>
                  </td>

                  <td>
                    <button type="button" class="btn btn-link btn-lg" data-toggle="modal" data-target="#{{ $artist->id }}-pt">Preview</button>                    
                    <div class="modal fade" id="{{ $artist->id }}-pt" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                  @php 
                                    if($artistsTranslations->where('artists_id', $artist->id)->where('language_id', '1')->first()){
                                        echo $artistsTranslations->where('artists_id', $artist->id)->where('language_id', '1')->first()->description; 
                                    }
                                  @endphp
                                </div>
                            </div>
                        </div>
                    </div>                        
                  </td>

                  <td class="text-center">
                      {{ $artist->day }}
                  </td>
                  <td class="text-center">
                      {{ $artist->hours }}
                  </td>
                  <td>
                      @php
                      if( $artist->main_image ) {
                          echo '<img src="'. $artist->main_image .'" height="100" width="100">';

                      }else{
                          echo '<img src="http://lorempixel.com/100/100/cats" height="100" width="100">';
                      }
                      @endphp
                      <!-- show logo thumbnail -->
                  </td>

              </tr>
            @endforeach
        </tbody>
        <tfoot>
          <tr>
              <th>Name</th>
              <th>Description</th>
              <th>Day</th>
              <th>Hours</th>
              <th>Main Image</th>
          </tr>
        </tfoot>
      </table>
    </div>
    <!-- /.box-body -->
  </div>
  <!-- /.box -->
</div>
<!-- /.col -->
</div>

{!! Toastr::message() !!}

@stop