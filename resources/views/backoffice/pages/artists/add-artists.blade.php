@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Artists</h1>
@stop

@section('content')


<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Add new Artist</h3>
    </div>
    
    <div class="tab-form-wrap" role="tabpanel">

        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#one" aria-controls="one" role="tab" data-toggle="tab">1</a> Step One | General Information</li>
            <li role="presentation"><a href="#two" aria-controls="two" role="tab" data-toggle="tab">2</a> Step Two | Multi-Language Content</li>
            <li role="presentation"><a href="#three" aria-controls="three" role="tab" data-toggle="tab">3</a> Step Three | Images</li>
        </ul>


        <form enctype='multipart/form-data' role="form" method="post" action="{{ action('backoffice\ArtistsController@store') }}">
            {{ csrf_field() }}  
    
        <!-- Tab panes -->
        <div class="tab-content">
  
                    <div role="tabpanel" class="tab-pane fade in active" id="one">
                        <h3>Base Info</h3>
                        <fieldset >
                            <span class="input-label">Name</span>
                            <input class="input-clear" name="name" role="text-role" type="text" placeholder="Placeholder" required>
                            <span class="error-span" style="display: none; "> Please insert a name</span>
                        </fieldset>

                        <fieldset class="custom-dropdown">
                            <span class="input-label">Day</span>
                            <select id="select" name="day" class="input-clear" required>
                                <option value="" disabled="disabled " selected>Select a Day</option>
                                <option value="1">Day 1</option>
                                <option value="2">Day 2</option>
                                <option value="3">Day 3</option>
                            </select>
                            <span class="error-span" style="display: none; "> Please select a day</span>
                        </fieldset>

                        <fieldset >
                            <span class="input-label">Hour</span>
                            <input name="hour" class="input-clear" type="number" placeholder="00:00" title="Time" required>
                            <span class="error-span" style="display: none; ">  Please insert a hour</span>
                        </fieldset>

                        <h3>Social Media </h3>
                        <fieldset >
                            <span class="input-label">Facebook</span>
                            <input name="facebook" class="input-clear" type="text" placeholder="00:00" title="Time" required>
                            <span class="error-span" style="display: none; ">  Please insert a Facebook link</span>
                        </fieldset>

                        <fieldset >
                            <span class="input-label">Youtube</span>
                            <input name="youtube" class="input-clear" type="text" placeholder="00:00" title="Time" required>
                            <span class="error-span" style="display: none; ">  Please insert a Youtube link</span>
                        </fieldset>
                    </div>



                    <div role="tabpanel" class="tab-pane fade" id="two">
                        <fieldset >
                            <span class="input-label">Description PT</span>
                            <textarea id="elm1" name="description-pt" class="form-control" required=""></textarea>
                            <span class="error-span" style="display: none; ">  Please insert a description in Portuguese</span>
                        </fieldset>

                        <fieldset >
                            <span class="input-label">Description EN</span>
                            <textarea id="elm2" name="description-en" class="form-control" required=""></textarea>
                            <span class="error-span" style="display: none; ">Please insert a description in English</span>
                        </fieldset>

                        <fieldset >
                            <span class="input-label">Description ES</span>
                            <textarea id="elm3" name="description-es" class="form-control" required=""></textarea>
                            <span class="error-span" style="display: none; ">Please insert a description in ES</span>
                        </fieldset>
                    </div>


                    <div role="tabpanel" class="tab-pane fade" id="three">
                        <fieldset>
                            <div id="main-image">Main</div>
                            <input type="hidden" name="img" id="img" value="tst/img">
                        </fieldset>
                    </div>
                

                    <a id="nextBtn" class="blk-btn btnNext">next step</a>
                    <button type="submit" id="submitBtn" class="blk-btn btnNext hidden">Submit</button>

         
        </div>
    </form>
    </div>
</div>


<script>

$(function() {
    var $this = $(this);
    let formInputs;
    let formSelects; 
    let textAreas;
    let emptyFields = 0;
    let submitBtn = document.getElementById('submitBtn')
    let nextBtn = document.getElementById('nextBtn')

    // Setup next button
    $('.btnNext').on('click', function() {        

        //get form elements + create error counter
        let form = document.querySelectorAll('div[role="tabpanel"].active');
        formInputs = form[0].querySelectorAll('input');
        formSelects = form[0].querySelectorAll('select');
        textAreas = form[0].querySelectorAll('textarea'); 
        emptyFields = 0; 
        
        validateInputs(emptyFields);
        validateSelects(formSelects, emptyFields);
        validateTextArea(textAreas, emptyFields); 

        console.log("END: ", emptyFields)
        if(emptyFields == 0 ){
            nextTab(); 
        }else{
            //do nothing
        }
        
    });
    
    function validateInputs(){
        formInputs.forEach(function (currentValue, currentIndex){
            if(currentValue.hasAttribute('required')) {
                let parent = currentValue.parentElement; 
                
                //check text inputs
                if(currentValue.value === ''){
                    //increment error counter
                    emptyFields++; 
                    //add error css
                    parent.classList.add('validation-error');
                    parent.lastElementChild.style.display='block'
                    
                }else{
                    
                    if( parent.classList.contains('validation-error') ) {
                        parent.classList.remove('validation-error')
                        parent.lastElementChild.style.display='none'
                    }
                }
                //end of check text inputs

            }
        }); 
    }

    function validateSelects(){
        formSelects.forEach(function (currentValue, currentIndex){
            if(currentValue.hasAttribute('required')) {
                let parent = currentValue.parentElement; 
                //check text inputs
                console.log(currentValue.value); 
                if(currentValue.value === ''){
                    //increment error counter
                    emptyFields++; 
                    //add error css
                    parent.classList.add('validation-error');
                }else{
                    
                    if( parent.classList.contains('validation-error') ) {
                        parent.classList.remove('validation-error')
                    }
                }
                //end of check text inputs
            }
        }); 
    }
    function validateTextArea() {
        textAreas.forEach(function (currentValue, currentIndex){
            if(currentValue.hasAttribute('required')) {
                let parent = currentValue.parentElement; 
                //check text inputs
                tinyMCE.triggerSave();

                console.log(currentValue.value); 
                if(currentValue.value === ''){
                    //increment error counter
                    emptyFields++; 
                    //add error css
                    parent.classList.add('validation-error');
                    parent.lastElementChild.style.display='block'
                }else{
                    
                    if( parent.classList.contains('validation-error') ) {
                        parent.classList.remove('validation-error')
                        parent.lastElementChild.style.display='none'
                    }
                }
                //end of check text inputs
            }
        }); 
    }



        $('.tab-form-wrap .tab-content fieldset').on('click', function() {
            $this.children('.input-clear').focus();
        });
        
        $('.tab-form-wrap .tab-content fieldset .input-clear')
            .on('focus', function() {
            $this.parent().addClass('focus');
        })
            .on('focusout', function() {
            $this.parent().removeClass('focus');
        });


    });


    function nextTab() {
        var e = $('ul[role="tablist"] li.active').next().find('a[data-toggle="tab"]');  
        if(e.length > 0) e.click();  
        isLastTab();
    }

    function isLastTab() {
        var e = $('ul[role="tablist"] li:last').hasClass('active'); 

 

        if( e ){
            submitBtn.classList.remove('hidden');
            nextBtn.classList.add('hidden');
            
        }else{
            submitBtn.classList.add('hidden');
            nextBtn.classList.remove('hidden');

        } 
        return e;
    }

jQuery( document ).ready(function() {

    function fileUploader(name,folder,maxFiles,acceptedTypes){
        jQuery("#"+name).uploadFile({
        url:'{{ action("backoffice\ArtistsController@imageUpload") }}',
        fileName: name,
        acceptFiles: acceptedTypes,
        showPreview: true,
        maxFileCount: maxFiles,
        maxFileSize: '30000000',
        formData: {
            "_token":"{{ csrf_token() }}", 
            "name": name,
            "folder": folder
        },
        previewHeight: "100px",
        previewWidth: "100px",
        showDelete: true,
        deleteCallback: function (imageName, action) {
            jQuery.post("{{ action('backoffice\ArtistsController@imageDelete') }}", {
                action: "delete",
                imageName: imageName,
                "folder": folder,
                "name": name,
                _token:"{{ csrf_token() }}"},
                function (resp,textStatus, jqXHR) {
            });
        },
    }); 
    }

    jQuery("#main-image").onload = fileUploader('main-image','tmp',1,'image/*');


});



</script>


@stop