@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Services</h1>
@stop

@section('content')
<div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Add new service</h3>
        </div>


    <div class="tab-form-wrap" role="tabpanel">

        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#one" aria-controls="one" role="tab" data-toggle="tab">1</a> Step One | General Information</li>
            <li role="presentation"><a href="#two" aria-controls="two" role="tab" data-toggle="tab">2</a> Step Two | Multi-Language Content</li>
            <li role="presentation"><a href="#three" aria-controls="three" role="tab" data-toggle="tab">3</a> Step Three | Images</li>
        </ul>
        <form enctype='multipart/form-data' role="form" method="post" action="{{ action('backoffice\ArtistsController@update') }}">
            {{ csrf_field() }}  
    
        <!-- Tab panes -->
        <div class="tab-content">
  
                    <div role="tabpanel" class="tab-pane fade in active" id="one">
                        <h3>Base Info</h3>
                        <fieldset >
                            <span class="input-label">Name</span>
                            <input class="input-clear" name="name" role="text-role" type="text" placeholder="Name" value="{{ $artist->name }}" required>
                            <span class="error-span" style="display: none; "> Please insert a name</span>
                        </fieldset>

                        <fieldset class="custom-dropdown">
                            <span class="input-label">Day</span>
                            <select id="select" name="day" class="input-clear" required>
                                <option value="" disabled="disabled " selected>Select a Day</option>
                                <option value="1" {{ $artist->day == '1' ? 'selected="selected"' : '' }}>Day 1</option>
                                <option value="2" {{ $artist->day == '2' ? 'selected="selected"' : '' }}>Day 2</option>
                                <option value="3" {{ $artist->day == '3' ? 'selected="selected"' : '' }}>Day 3</option>
                            </select>
                            <span class="error-span" style="display: none; "> Please select a day</span>
                        </fieldset>

                        <fieldset >
                            <span class="input-label">Hour</span>
                            <input name="hour" class="input-clear" type="number" placeholder="00:00" title="Time" value="{{ $artist->hours}}" required>
                            <span class="error-span" style="display: none; ">  Please insert a hour</span>
                        </fieldset>

                        <h3>Social Media </h3>
                        <fieldset >
                            <span class="input-label">Facebook</span>
                            <input name="facebook" class="input-clear" type="text" placeholder="Https://www.facebook.com"  value="{{ $artist->facebook}}" title="facebook" required>
                            <span class="error-span" style="display: none; ">  Please insert a Facebook link</span>
                        </fieldset>

                        <fieldset >
                            <span class="input-label">Youtube</span>
                            <input name="youtube" class="input-clear" type="text" placeholder="Https://www.youtube.com" value="{{ $artist->youtube}}" title="youtube" required>
                            <span class="error-span" style="display: none; ">  Please insert a Youtube link</span>
                        </fieldset>
                    </div>



                    <div role="tabpanel" class="tab-pane fade" id="two">
                        <fieldset >
                            <span class="input-label">Description PT</span>
                            <textarea id="elm1" name="description-pt" class="form-control"  required="">{{ $artistPt->description}}</textarea>
                            <span class="error-span" style="display: none; ">  Please insert a description in Portuguese</span>
                        </fieldset>

                        <fieldset >
                            <span class="input-label">Description EN</span>
                            <textarea id="elm2" name="description-en" class="form-control" required="">{{ $artistEn->description}}</textarea>
                            <span class="error-span" style="display: none; ">Please insert a description in English</span>
                        </fieldset>

                        <fieldset >
                            <span class="input-label">Description ES</span>
                            <textarea id="elm3" name="description-es" class="form-control" required="">{{ $artistEs->description}}</textarea>
                            <span class="error-span" style="display: none; ">Please insert a description in ES</span>
                        </fieldset>
                    </div>

                    <input type="hidden" name="id" id="id" value="{{ $artist->id }}">

                    <div role="tabpanel" class="tab-pane fade" id="three">
                        <fieldset>
                            <div id="main-image">Main</div>
                        </fieldset>
                    </div>
                
                    <a id="nextBtn" class="blk-btn btnNext">next step</a>
                    <button type="submit" id="submitBtn" class="blk-btn btnNext hidden">Submit</button>

            
            </div>
        </form>
    </div>
</div>
    <style>

        .hidden {
            display: none;
        }
    
    
        input:focus,
        select:focus,
        textarea:focus,
        button:focus {
            outline: none;
        }
    
        .welcome-message {
            text-align: center;
        }
    
        .welcome-message h1 {
            font-weight: 800;
        }
    
        .welcome-message p {
        font-weight: 700;
        }
    
        .blk-btn {
            width: 460px;
            margin: 0 auto;
            display: block;
            line-height: 50px;
            border-radius: 5px;
            background: #2196F3;
            text-align: center;
            color: white;
            text-transform: uppercase;
            font-weight: bold;
            -webkit-transition: all .3s;
            -moz-transition: all .3s;
            -o-transition: all .3s;
            transition: all .3s;
        }
    
        .blk-btn:hover, .blk-btn:focus {
        color: white;
        text-decoration: none;
        background: #1976D2;
        }
    
        .tab-form-wrap {
        background: #fff;
        border-radius: 2px;
        box-shadow: 0 1px 2px rgba(0, 0, 0, 0.25);
        }
    
        /* 3a. Nav tabs */
        .tab-form-wrap .nav-tabs {
        border-bottom: 1px solid #e0e0e0;
        background: #2196f3;
        color: white; 
        }
    
        .tab-form-wrap .nav-tabs li {
        width: 10%;
        height: 40px;
        line-height: 40px;
        color: #fafafa;
        overflow: hidden;
        margin: 0;
        border-right: 1px solid #e0e0e0;
        -webkit-transition: width .4s ease;
        -moz-transition: width .4s ease;
        -o-transition: width .4s ease;
        transition: width .4s ease;  
        }
    
        .tab-form-wrap .nav-tabs li:last-child {
        border-right: none;
        }
    
        .tab-form-wrap .nav-tabs li.active {
        width: 70%;
        }
    
        .tab-form-wrap .nav-tabs li a, .tab-form-wrap .nav-tabs li a:hover, .tab-form-wrap .nav-tabs li a:focus {
        border: none;
        background: none;
        }
    
        .tab-form-wrap .nav-tabs li a:hover, .tab-form-wrap .nav-tabs li a:focus {
        color: inherit;  
        }
    
        .tab-form-wrap .nav-tabs li a {
        font-size: 14px;
        line-height: 20px;
        height: 40px;
        overflow: hidden;
        border-radius: 0;
        margin: 0;
        text-align: center;
        color: inherit;
        float: left;
        width: 100%;
        }
        .tab-form-wrap .nav-tabs li.active a {
        text-align: left;
        width: auto; 
        font-weight: bold;
        }
    
        /*.tab-form-wrap .nav-tabs li a span.step-title {
        visibility: hidden;
        }
    
        .tab-form-wrap .nav-tabs li.active a span.step-title {
        visibility: visible;
        }*/
    
        /* Tab panes */
        .tab-form-wrap .tab-content {
        padding: 15px;
        }
    
        .tab-form-wrap .tab-content fieldset {
        border: 1px solid #e0e0e0;
        padding: 8px 8px 0 8px;
        border-radius: 5px;
        margin-bottom: 15px;
        cursor: text;
        }
    
        .tab-form-wrap .tab-content fieldset.focus {
        border-color: #2196F3;
        }
    
        .tab-form-wrap .tab-content fieldset > * {
        cursor: text;
        }
    
        /* Validation Styles */
        .tab-form-wrap .tab-content fieldset.validation-error {
        border-color: #F44336;
        }
    
        .tab-form-wrap .tab-content fieldset.validation-error .input-label {
        color: #F44336;
        }
    
        .tab-form-wrap .tab-content fieldset .input-label {
        display: block;
        font-size: 11px;
        line-height: 12px;
        font-weight: bold;
        text-transform: uppercase;
        }
    
        .tab-form-wrap .tab-content fieldset .input-clear {
        display: inline-block;
        width: 100%;
        font-size: 14px;
        line-height: 28px;
        border: none;
        }
    
        .error-span {
            display: block;
            color: red; 
            font-size: 11px;
            line-height: 12px;
            font-weight: bold;
            text-transform: uppercase;
            font-style: italic;
            width: 100%;
        }
    
        /* Custom dropdown */
        .custom-dropdown {
        position: relative;
        vertical-align: middle;
        cursor: pointer !important;
        }
    
        .custom-dropdown select, .custom-dropdown .input-label {
        cursor: pointer !important;
        }
    
        .custom-dropdown select {	
        border: 0;
        margin: 0;
        border-radius: 3px;
        text-indent: 0.01px;
        text-overflow: '';
        -webkit-appearance: button; /* hide default arrow in chrome OSX */
        }
    
        .custom-dropdown::before,
        .custom-dropdown::after {
        content: "";
        position: absolute;
        pointer-events: none;
        }
    
        .custom-dropdown::after { /*  Custom dropdown arrow */
        content: "\25BC";
        height: 1em;
        font-size: .625em;
        line-height: 1;
        right: 1.2em;
        top: 50%;
        margin-top: -.5em;
        }
    
        .custom-dropdown::before { /*  Custom dropdown arrow cover */
        width: 2em;
        right: 0;
        top: 0;
        bottom: 0;
        border-radius: 0 3px 3px 0;
        }
    
        .custom-dropdown::before {
        background-color: #e0e0e0;
        }
    
        .custom-dropdown::after {
        color: #9e9e9e;
        }
    
    </style>
    
    
<script>



$(function() {
    var $this = $(this);
    let formInputs;
    let formSelects; 
    let textAreas;
    let emptyFields = 0;
    let submitBtn = document.getElementById('submitBtn')
    let nextBtn = document.getElementById('nextBtn')

    // Setup next button
    $('.btnNext').on('click', function() {        

        //get form elements + create error counter
        let form = document.querySelectorAll('div[role="tabpanel"].active');
        formInputs = form[0].querySelectorAll('input');
        formSelects = form[0].querySelectorAll('select');
        textAreas = form[0].querySelectorAll('textarea'); 
        emptyFields = 0; 
        
        validateInputs(emptyFields);
        validateSelects(formSelects, emptyFields);
        validateTextArea(textAreas, emptyFields); 

        console.log("END: ", emptyFields)
        if(emptyFields == 0 ){
            nextTab(); 
        }else{
            //do nothing
        }
        
    });
    
    function validateInputs(){
        formInputs.forEach(function (currentValue, currentIndex){
            if(currentValue.hasAttribute('required')) {
                let parent = currentValue.parentElement; 
                
                //check text inputs
                if(currentValue.value === ''){
                    //increment error counter
                    emptyFields++; 
                    //add error css
                    parent.classList.add('validation-error');
                    parent.lastElementChild.style.display='block'
                    
                }else{
                    
                    if( parent.classList.contains('validation-error') ) {
                        parent.classList.remove('validation-error')
                        parent.lastElementChild.style.display='none'
                    }
                }
                //end of check text inputs

            }
        }); 
    }

    function validateSelects(){
        formSelects.forEach(function (currentValue, currentIndex){
            if(currentValue.hasAttribute('required')) {
                let parent = currentValue.parentElement; 
                //check text inputs
                console.log(currentValue.value); 
                if(currentValue.value === ''){
                    //increment error counter
                    emptyFields++; 
                    //add error css
                    parent.classList.add('validation-error');
                }else{
                    
                    if( parent.classList.contains('validation-error') ) {
                        parent.classList.remove('validation-error')
                    }
                }
                //end of check text inputs
            }
        }); 
    }
    function validateTextArea() {
        textAreas.forEach(function (currentValue, currentIndex){
            if(currentValue.hasAttribute('required')) {
                let parent = currentValue.parentElement; 
                //check text inputs
                tinyMCE.triggerSave();

                console.log(currentValue.value); 
                if(currentValue.value === ''){
                    //increment error counter
                    emptyFields++; 
                    //add error css
                    parent.classList.add('validation-error');
                    parent.lastElementChild.style.display='block'
                }else{
                    
                    if( parent.classList.contains('validation-error') ) {
                        parent.classList.remove('validation-error')
                        parent.lastElementChild.style.display='none'
                    }
                }
                //end of check text inputs
            }
        }); 
    }



        $('.tab-form-wrap .tab-content fieldset').on('click', function() {
            $this.children('.input-clear').focus();
        });
        
        $('.tab-form-wrap .tab-content fieldset .input-clear')
            .on('focus', function() {
            $this.parent().addClass('focus');
        })
            .on('focusout', function() {
            $this.parent().removeClass('focus');
        });


    });


    function nextTab() {
        var e = $('ul[role="tablist"] li.active').next().find('a[data-toggle="tab"]');  
        if(e.length > 0) e.click();  
        isLastTab();
    }

    function isLastTab() {
        var e = $('ul[role="tablist"] li:last').hasClass('active'); 

 

        if( e ){
            submitBtn.classList.remove('hidden');
            nextBtn.classList.add('hidden');
            
        }else{
            submitBtn.classList.add('hidden');
            nextBtn.classList.remove('hidden');

        } 
        return e;
    }

jQuery( document ).ready(function() {
        
   

    function fileUploader(name,folder,maxFiles,acceptedTypes,folderName){

        jQuery("#"+name).uploadFile({
        url:'{{ action("backoffice\ArtistsController@imageUpload") }}',
        fileName: name,
        acceptFiles: acceptedTypes,
        showPreview: true,
        maxFileCount: maxFiles,
        maxFileSize: '30000000',
        formData: {
            "_token":"{{ csrf_token() }}", 
            "name": name,
            "folder": "tmp"
        },
        previewHeight: "100px",
        previewWidth: "100px",
        showDelete: true,
        deleteCallback: function (imageName, action) {
            jQuery.post("{{ action('backoffice\ArtistsController@imageDelete') }}", {
                action: "delete",
                imageName: imageName,
                "folder": "tmp",
                "name": name,
                _token:"{{ csrf_token() }}"},
                function (resp,textStatus, jqXHR) {
            });
        },
        onLoad:function(obj)
            {
                jQuery.ajax({
                    cache: false,
                    url:'{{ action("backoffice\ArtistsController@imageLoad") }}',
                    dataType: "json",
                    data: {
                        "id": jQuery('#id').val(), 
                        "name": folderName
                    },
                    success: function(data) 
                    {
                        for(var i=0;i<data.length;i++)
                        { 
                            obj.createProgress(data[i]["name"],data[i]["path"],data[i]["size"]);
                        }
                    }
                });
            }, 
    }); 
    }

    jQuery("#main-image").onload = fileUploader('main-image','main-image',1,'image/*','main-image');

});
    </script>
    @stop