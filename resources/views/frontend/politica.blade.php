<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,600i,700,800,800i" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('css/frontend/style.css') }}">
        <link rel="shortcut icon" type="image/png" href="{{ asset('img/favicon.png') }}">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.6/css/swiper.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">


        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.6/js/swiper.min.js"></script>
        <script src="{{ asset('js/frontend/style.js') }}"></script>
        <script src='https://www.google.com/recaptcha/api.js'></script>

        
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-113582586-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-113582586-1');
</script>


        <title>Dancefloor | #JumpToTheDrop</title>
    </head>


<body>
    @include('frontend.partials._navigation')


    <main>
        <section class="section-intro u-center-text">
            <div class="u-center-text u-margin-bottom-big">
                <h2 class="heading-secondary heading-big">
                    Política de Privacidade
                </h2>
                <a id="buynow" class="btn btn--blue u-margin-top-medium" href="{{ route('tickets') }}">{{ __('home.buy') }}</a>
				<script>

					//analytics triggers

					jQuery('#buynow').on('click', function(){
							ga.getAll()[0].send('event', 'Botão Comprar Bilhete', 'clique')
					});

				</script>
			</div> 
        </section>


        <section class="section__politica u-center-text">
            <h3 class="heading-tertiary u-margin-bottom-small u-margin-top-small"> 
                Política de Privacidade
            </h3>
            <p class="politica__paragraph">Com a entrada em vigor do Regulamento Geral sobre a Proteção de Dados, desde do dia 25 de maio de 2018, passou a existir um conjunto único de regras de proteção de dados para todas as empresas ativas na UE, independentemente da sua localização. Com o âmbito, para os cidadãos terem um maior controlo sobre os seus dados pessoais e condições mais equitativas para as empresas.</p>
            

            <h3 class="heading-tertiary u-margin-bottom-small u-margin-top-small"> 
                O que são dados pessoais?
            </h3>
            <p class="politica__paragraph">Dados pessoais são informação relativa a uma pessoa viva, identificada ou identificável. Também constituem dados pessoais o conjunto de informações distintas que podem levar à identificação de uma determinada pessoa.</p>
            <p class="politica__paragraph">O RGPD protege os dados pessoais independentemente da tecnologia utilizada para o tratamento desses dados – é neutra em termos tecnológicos e aplica-se tanto ao tratamento automatizado como ao tratamento manual, desde que os dados sejam organizados de acordo com critérios pré-definidos (por exemplo, por ordem alfabética). Também é irrelevante o modo como os dados são armazenados — num sistema informático, através de videovigilância, ou em papel; em todos estes casos, os dados pessoais estão sujeitos aos requisitos de proteção previstos no RGPD.</p>
            
            <h3 class="heading-tertiary u-margin-bottom-small u-margin-top-small"> 
                Que dados pessoais são recolhidos e como são recolhidos?
            </h3>
            <p class="politica__paragraph">Utilizamos várias tecnologias de internet (p. ex. Cookies ou Java-Script) apenas para facilitar a sua utilização das aplicações de internet.
            O nosso site utiliza o Google Analytics que é o serviço de análises Web da Google para avaliar a utilização do nosso site.
            Estas informações são genéricas e, em nenhum caso, a Google associará o seu endereço IP com outros dados Google. Os dados recolhidos são avaliados anonimamente, exclusivamente para fins estatísticos.
            </p>

            <h3 class="heading-tertiary u-margin-bottom-small u-margin-top-small"> 
                Prazo de validade e segurança do tratamento de dados:
            </h3>
            <p class="politica__paragraph">Exceto quando o candidato solicitar a destruição de seu arquivo, a empresa retém os dados pessoais dos candidatos por um período de 2 anos após o último contato, sob condições seguras para proteger, em particular, a confidencialidade e protegê-los contra as perdas, acessos, usos, alterações ou divulgações não autorizadas, de acordo com os meios atuais da arte, em conformidade com as disposições regulamentares e legais em vigor.</p>
            <p class="politica__paragraph">No entanto, não podemos oferecer uma garantia absoluta de segurança, na medida em que a Internet é uma rede aberta, suscetível por natureza a esses riscos, e declina qualquer responsabilidade por quaisquer danos resultantes de uma intrusão fraudulenta de um terceiro tendo causado uma modificação da informação divulgada no site através do Serviço.</p>
            <p class="politica__paragraph">Poderá contatar-nos através de um dos contatos neste website, para mais esclarecimentos ou caso pretenda esclarecer alguma dúvida quanto a esta política de privacidade</p>
            
            <h3 class="heading-tertiary u-margin-bottom-small u-margin-top-small"> 
                Formulários de contactos, newsletter e redes sociais
            </h3>
            <p class="politica__paragraph">Os dados obtidos através dos nossos formulários de contactos e newsletter são usados apenas para responder ás suas questões e apagados imediatamente a seguir. 
Ao clicar nos links das nossas redes sociais, poderá enviar dados estatísticos para estas empresas e para o Google analytics. Mais informações sobre a privacidade de dados podem ser acedidas nos sites das respetivas redes sociais: Facebook, Instagram, Twitter, Linkedin.
</p>
            <h3 class="heading-tertiary u-margin-bottom-small u-margin-top-small"> 
                Ligações a website de terceiros
            </h3>
            <p class="politica__paragraph">Podemos usar ligações para outros websites, que consideremos relevantes para os utilizadores. A nossa política de privacidade não é aplicável a sites de terceiros, pelo que deverá consultar a política de privacidade destes mesmos websites.
</p>
            <h3 class="heading-tertiary u-margin-bottom-small u-margin-top-small"> 
                Conteúdo incorporado de outros sites
            </h3>
            <p class="politica__paragraph">Os artigos neste site podem incluir conteúdo incorporado (por exemplo: vídeos, imagens, artigos, etc.). O conteúdo incorporado de outros websites comporta-se tal como se o utilizador visitasse esses websites.
</p>
            <h3 class="heading-tertiary u-margin-bottom-small u-margin-top-small"> 
                Atualizações desta política de segurança
            </h3>
            <p class="politica__paragraph">Haverá necessidade de alterar essa política periodicamente para garantir que esteja atualizada com os requisitos legais mais recentes e com quaisquer alterações em nossas práticas da gestão de privacidade.
Qualquer alteração a esta política será publicada e uma versão mais recente desta política estará sempre disponível nesta página.
</p>
        </section>
    </main>

    @include('frontend.partials._footer')
    
</body>

</html>