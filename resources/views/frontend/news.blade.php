<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,600i,700,800,800i" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('css/frontend/style.css') }}">
        <link rel="shortcut icon" type="image/png" href="{{ asset('img/favicon.png') }}">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.6/css/swiper.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">


        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.6/js/swiper.min.js"></script>
        <script src="{{ asset('js/frontend/style.js') }}"></script>
        <script src='https://www.google.com/recaptcha/api.js'></script>

        
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-113582586-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-113582586-1');
</script>


        <title>Dancefloor | #JumpToTheDrop</title>
    </head>



<body>
    @include('frontend.partials._navigation')

    <main>
        <section class="section-news u-center-text">
            <div class="u-center-text u-margin-bottom-big">
                <h2 class="heading-secondary heading-big">
                    {{ __('home.news') }}
                </h2>
                <a id="buynow" class="btn btn--blue u-margin-top-medium" href="{{ route('tickets') }}">{{ __('home.buy') }}</a>
				<script>

					//analytics triggers

					jQuery('#buynow').on('click', function(){
							ga.getAll()[0].send('event', 'Botão Comprar Bilhete', 'clique')
					});

				</script>
			</div> 
        </section>

        <section class="news">
            <div class="row">

            @foreach($news as $new)
                @php  
                    $newsTtranlastions = DB::table('news_translations')->where('news_id', $new->id)
                    ->where('language_id', $language)->get();
                @endphp
            <div class="news__content">
                    <div class="news__image__holder">
                        <img class="news__image--all" src="{{ $new->thumbnail }}" alt="News 1">
                    </div>
                    <div class="news__content--holder">
                        <div class="news__date--all">
                            <i class="fas fa-calendar-alt"></i> {{ date("d-m-Y", strtotime($new->created_at ))}}
                        </div>
                        
                        <div class="news__title--all u-margin-top-small">
                            {{ $newsTtranlastions[0]->title }}
                        </div>

                        <div class="news__description--all u-margin-top-small">
                            {{ $newsTtranlastions[0]->subtitle }}
                        </div>

                        <a href="{{ route('new', ['name' => $newsTtranlastions[0]->news_id] )}}" class="btn btn--blue btn--small news__button u-margin-top-small--mobile">{{ __('home.see-more') }}</a>
                    </div>
                </div>
            @endforeach
            {{ $news->links() }}

            </div>
        </section>
    </main>
    @include('frontend.partials._footer')

</body>

</html>