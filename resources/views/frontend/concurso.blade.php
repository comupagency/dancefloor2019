<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,600i,700,800,800i" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('css/frontend/style.css') }}">
        <link rel="shortcut icon" type="image/png" href="{{ asset('img/favicon.png') }}">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.6/css/swiper.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">


        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.6/js/swiper.min.js"></script>
        <script src="{{ asset('js/frontend/style.js') }}"></script>
        <script src='https://www.google.com/recaptcha/api.js'></script>

        
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-113582586-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-113582586-1');
</script>


        <title>Dancefloor | #JumpToTheDrop</title>
    </head>


<body>
    @include('frontend.partials._navigation')


    <main>
        <section class="section-intro u-center-text">
            <div class="u-center-text u-margin-bottom-big">
                <h2 class="heading-secondary heading-big">
                    Concurso
				</h2>
				<div >
				<img src="{{ asset('images/concurso/DF-Logo-ConcursoDj-Sem-Fundo.png') }}" class="concurso__image">
				</div>
                <a onClick="ga('send', { hitType: 'event',eventCategory: 'Botão Comprar Bilhete',eventAction: 'clique',});" class="btn btn--blue u-margin-top-medium" href="{{ route('tickets') }}">{{ __('home.buy') }}</a>
            </div> 
        </section>


        <section class="section__politica u-center-text">
		
            <h3 class="heading-tertiary u-margin-bottom-small u-margin-top-small" style="font-size:2rem;"> 
                Regulamento Passatempo "Salta da pista para o palco"
            </h3> 
			<p class="politica__paragraph u-margin-top-small u-margin-bottom-small"> O Dancefloor irá realizar um passatempo, entre os dias 22 e 26 de abril. O vencedor será anunciado no dia 6 de maio. </p>
			<p class="politica__paragraph u-margin-top-small u-margin-bottom-small"> A participação neste passatempo pressupõe a aceitação dos presentes termos e condições: </p>

			<p class="politica__paragraph u-margin-top-small u-margin-bottom-small"><b>1 – Organização</b><br> O Dancefloor organiza um passatempo online destinado a DJs amadores e/ou profissionais, denominado <b>“Salta da pista para o palco”</b> que será regido pelos termos e condições constantes no presente Regulamento.</p>
			
			<p class="politica__paragraph u-margin-top-small u-margin-bottom-small"><b>2 – Duração do Passatempo</b><br>
				A primeira fase do passatempo <b>“Salta da pista para o palco”</b> tem início às 18h00 horas do dia 22 de abril, e encerra às 23h59 horas do dia 26 de abril. O vencedor será anunciado no dia 6 de maio.  </p>
			<p class="politica__paragraph u-margin-top-small u-margin-bottom-small"><b>3 – Participantes</b><br>
				<b>3.1</b>  O passatempo é destinado a todos os seguidores da <a style="color: white;" href="https://www.facebook.com/dancefloorjump/">página oficial do Dancefloor no Facebook</a>. (Para tornar-se seguidor basta fazer “gosto“ na página do Dancefloor). 
				<br><b>3.2</b>  Os participantes que não cumprirem com o estipulado no presente Regulamento do Passatempo não terão direito à atribuição do prémio previsto. 
				<br><b>3.3</b>   O Dancefloor reserva o direito exclusivo de negar o direito de participação a qualquer indivíduo caso considere que as condições estipuladas tenham sido infringidas.
				<br><b>3.4 </b>  Em caso de erro, mal-entendido ou conflito acerca do funcionamento de quaisquer aspetos do passatempo, bem como em relação à atuação dos participantes, a decisão tomada pelo Dancefloor considerar-se-á concludente e definitiva. 
				<br><b>3.5 </b> O Dancefloor reserva-se o direito de tomar as decisões necessárias para o decurso normal deste concurso, sendo estas expressamente aceites por cada um dos participantes. </p>
			<p class="politica__paragraph u-margin-top-small u-margin-bottom-small">  <b>4 – Como participar</b><br>
				<br><b>4.1</b>   Para participar, cada utilizador, terá que fazer upload de uma demo/mini-set (10 minutos de tempo máximo), no Youtube ou no SoundCloud, que demonstre o seu talento para atuar no festival. O nome da faixa publicada deverá ser “Salta da Pista para o palco do Dancefloor – [Nome do Artista]”.
				<br><b>4.2</b>   O link do vídeo deverá ser enviado para o email passatempo@dancefloor.pt com o assunto “Passatempo – [NOME DO ARTISTA]”
				<br><b>4.3</b>  Os participantes só podem participar com uma demo e esta deverá ser um original seu. Em caso de plágio ou utilização de demos de outro artista, o participante está excluído do concurso.    
				<br><b>4.4</b>  Os participantes não devem submeter músicas e vídeos que, pela sua apresentação, letra, ou outros aspetos, possam ser considerados ofensivos aos bons costumes ou suscetíveis de incentivar a violência ou discriminação de qualquer tipo. 
				<br><b>4.5</b> As participações que violem o previsto na alínea anterior ou que, de qualquer forma, infrinjam a lei ou direitos de terceiros, serão excluídas do passatempo.</p>
			<p class="politica__paragraph u-margin-top-small u-margin-bottom-small"> <b>5  – Apuramento do vencedor</b><br>
				Os vencedores deste passatempo serão apurados da seguinte forma:
				<br><b>5.1</b> O vencedor será eleito por um júri escolhido pelo Dancefloor. O júri é composto por:<br>
					<li style="color: white;"> Tiago Martins (CEO Dancefloor)</li>
					<li style="color: white;"> José Manso (Talent Acquisition Dancefloor)</li>
			</p>
			<p class="politica__paragraph u-margin-top-small u-margin-bottom-small"><b>6 – Comunicação dos vencedores</b><br>
				<b>6.1</b>  O vencedor será anunciado através da página de Facebook do Dancefloor.    
				<br><b>6.2</b> O vencedor deverá indicar os seus dados pessoais (nome, contacto, morada) através de mensagem privada no Facebook. 
				<br><b>6.3</b> O vencedor terá até 15 dias para facultar os seus dados pessoais sob a penalização de ser desqualificado do passatempo, sendo o seu prémio atribuído ao participante que, de acordo com as regras de participação, seja considerado o primeiro finalista, respeitando o procedimento até o prémio ser atribuído ou se esgotar a lista de finalistas.
			</p>
			<p class="politica__paragraph u-margin-top-small u-margin-bottom-small">  <b>7 – Prémios</b><br>
				O vencedor do Passatempo <b>"Salta da pista para o palco”</b> irá ter a oportunidade de tocar no palco principal em hora e data a confirmar posteriormente. Cede ainda os direitos de produção da sua música de livre vontade, para esta ser utilizada em qualquer material produzido pelo Dancefloor.</p>
			<p class="politica__paragraph u-margin-top-small u-margin-bottom-small"><b> 8 – Privacidade e tratamento de dados pessoais</b><br>
				<b>8.1</b>  Os participantes autorizam que as suas criações sejam utilizadas com fim promocional na página de Instagram e de Facebook do Dancefloor. 
				<br><b>8.2</b> O fornecimento de dados pessoais é obrigatório para atribuição do prémio. 
				<br><b>8.3</b> Os participantes autorizam que os dados facultados sejam recolhidos e tratados pelo Dancefloor para efeitos de contacto e atribuição de prémio ao vencedor. 
				<br><b>8.4</b> O Dancefloor garante a confidencialidade e segurança dos dados pessoais dos utilizadores. </p>
			<p class="politica__paragraph u-margin-top-small u-margin-bottom-small">  <b>9 – Disposições várias</b><br>
				<b>9.1</b>  A participação no passatempo implica a concordância integral com o presente regulamento.
				<br><b>9.2</b> O Dancefloor reserva-se o direito de não aceitar as participações que não respeitem as normas do presente regulamento, bem como de desclassificar os participantes, sempre que verifique qualquer violação do presente regulamentou qualquer indício de participação fraudulenta.
				<br><b>9.3</b> O Dancefloor não se responsabiliza por situações a que é alheia, designadamente por mau funcionamento da plataforma Facebook.
				<br><b>9.4</b>  Caberá ao Dancefloor decidir sobre qualquer situação omissa neste regulamento, devendo divulgar a decisão adotada em https://www.dancefloor.pt/concurso.
				<br><b>9.5</b>  O Dancefloor reserva-se o direito de suspender, em qualquer altura, temporária ou definitivamente o presente passatempo bem como introduzir quaisquer alterações ao presente regulamento, que considerem justificadas ou necessárias, assumindo a obrigação de divulgar convenientemente as alterações em https://www.dancefloor.pt/concurso.
	
			</p>
			<p class="politica__paragraph u-margin-top-small u-margin-bottom-small"> 
				Lisboa, 22 de Abril de 2019
			</p>

            
		</section>
		
    </main>

    @include('frontend.partials._footer')
    
</body>

</html>

<style>
	.politica__paragraph li {
		color: white; 
	}
</style>