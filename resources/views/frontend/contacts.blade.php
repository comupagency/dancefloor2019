<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,600i,700,800,800i" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('css/frontend/style.css') }}">
        <link rel="shortcut icon" type="image/png" href="{{ asset('img/favicon.png') }}">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.6/css/swiper.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">


        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.6/js/swiper.min.js"></script>
        <script src="{{ asset('js/frontend/style.js') }}"></script>
        <script src='https://www.google.com/recaptcha/api.js'></script>

        
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-113582586-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-113582586-1');
</script>

        <title>Dancefloor | #JumpToTheDrop</title>
    </head>



<body>
    @include('frontend.partials._navigation')


    <main>
        <section class="section-intro u-center-text">
            <div class="u-center-text u-margin-bottom-big">
                <h2 class="heading-secondary heading-big">
                    {{ __('contacts.contacts') }}
                </h2>
            </div> 
        </section>

        <section class="section-contacts">
            <div class="contacts__content">
                <div class="row">
                    <form id="contact-form" class="needs-validation" enctype='multipart/form-data' role="form" method="post" action="{{ action('frontend\ContactsController@sendEmail') }}">
                        {{ csrf_field() }}  
                        
                        <div class="form__row">

                        <input type="text" name="name" placeholder="{{ __('contacts.name') }}" class="form__input" required>
                            <input type="email" name="email" placeholder="{{ __('contacts.email') }}" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" class="form__input u-margin-top-small" required>
                            <input type="text" name="msg" placeholder="{{ __('contacts.message') }}" class="form__input form__textarea u-margin-top-small" required>
                            <input type="checkbox" name="botCheker" value="check" hidden>
                        </div>
                        <div class="g-recaptcha u-margin-top-small" data-sitekey="6LfbBJIUAAAAACqHPV999WNdNzGSV_V6I7ReOoyk"></div>
							
                        <input onClick="
                        ga('send', {
                          hitType: 'event',
                          eventCategory: 'Contactos',
                          eventAction: 'Enviar Formulário',
                        });" id="submit" type="submit" class="u-margin-top-medium u-margin-bottom-medium btn btn--blue u-margin-top-medium" value="{{ __('contacts.send') }}"> 
                    </form>
                </div>
            </div>
        </section>
    </main>

    @include('frontend.partials._footer')

    
</body>


<script>

$("#contact-form").submit(function(event) {

var recaptcha = $("#g-recaptcha-response").val();
if (recaptcha === "") {
    event.preventDefault();

    if(document.getElementsByClassName('error-div').length  == 0){
        var error = document.createElement('div')
        error.innerHTML = "Por favor verifique o recaptcha"
        error.className = "error-div";
        var appendDiv = document.getElementsByClassName('g-recaptcha')[0];
        appendDiv.append(error);
    }


    }
    });

</script>
</html>