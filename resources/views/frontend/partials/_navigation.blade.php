<header class="header">
    
        <div class="header__logo-box">
            <a href="{{ route('home') }}"><img src="{{ asset('img/logo.png') }}" alt="Logo" class="header__logo"></a>
        </div>

        <div class="navigation">
            <nav class="navigation__nav">
                <ul class="navigation__list">
                    <li class="navigation__item"><a href="{{ route('home') }}" class="navigation__link {{ (\Request::route()->getName() == 'home') ? 'active' : '' }}">{{ __('navigation.home') }}</a></li>
                    <li class="navigation__item"><a href="{{ route('lineup') }}" class="navigation__link {{ (\Request::route()->getName() == 'lineup') ? 'active' : '' }}">{{ __('navigation.cartaz') }}</a></li>
                    <li class="navigation__item"><a href="{{ route('info') }}" class="navigation__link {{ (\Request::route()->getName() == 'info') ? 'active' : '' }}">{{ __('navigation.info') }}</a></li>
                    <li class="navigation__item"><a href="{{ route('press') }}" class="navigation__link {{ (\Request::route()->getName() == 'press') ? 'active' : '' }}">{{ __('navigation.press') }}</a></li>
                    <li class="navigation__item"><a href="{{ route('tickets') }}" class="navigation__link {{ (\Request::route()->getName() == 'tickets') ? 'active' : '' }}">{{ __('navigation.bilhetes') }}</a></li>
                    <li class="navigation__item"><a href="{{ route('sponsors') }}" class="navigation__link {{ (\Request::route()->getName() == 'sponsors') ? 'active' : '' }}">{{ __('navigation.parceiros') }}</a></li>
                    <li class="navigation__item"><a href="{{ route('news') }}" class="navigation__link {{ (\Request::route()->getName() == 'news') ? 'active' : '' }}">{{ __('navigation.news') }}</a></li>
                    <li class="navigation__item"><a href="{{ route('contacts') }}" class="navigation__link {{ (\Request::route()->getName() == 'contacts') ? 'active' : '' }}">{{ __('navigation.contacts') }}</a></li>
                    <li class="navigation__item"><a href="https://shop.dancefloor.pt/" target="_blank" class="navigation__link">{{ __('navigation.store') }}</a></li>
                
                    <div class="lang">
                        <li class="navigation__item navigation__item--language"><a href="{{ LaravelLocalization::getLocalizedURL('pt', null, [], true) }}" class="navigation__link {{ (LaravelLocalization::getCurrentLocale() == 'pt') ? 'active' : '' }}">PT</a></li>
                        <li class="navigation__item navigation__item--language"><a href="{{ LaravelLocalization::getLocalizedURL('en', null, [], true) }}" class="navigation__link {{ (LaravelLocalization::getCurrentLocale() == 'en') ? 'active' : '' }}">EN</a></li>
                    </div>
                </ul>
            </nav>
        </div>

        <div class="navigation__mobile">
            <input type="checkbox" class="navigation__mobile__checkbox" id="navi-toggle">

            <label for="navi-toggle" class="navigation__mobile__button">
                <span class="navigation__mobile__icon">&nbsp;</span>
            </label>

            <nav class="navigation__mobile__nav">
                <ul class="navigation__mobile__list">
                    <li class="navigation__mobile__item"><a href="{{ route('home') }}" class="navigation__mobile__link">{{ __('navigation.home') }}</a></li>
                    <li class="navigation__mobile__item"><a href="{{ route('lineup') }}" class="navigation__mobile__link">{{ __('navigation.cartaz') }}</a></li>
                    <li class="navigation__mobile__item"><a href="{{ route('info') }}" class="navigation__mobile__link">{{ __('navigation.info') }}</a></li>
                    <li class="navigation__mobile__item"><a href="{{ route('press') }}" class="navigation__mobile__link">{{ __('navigation.press') }}</a></li>
                    <li class="navigation__mobile__item"><a href="{{ route('tickets') }}" class="navigation__mobile__link">{{ __('navigation.bilhetes') }}</a></li>
                    <li class="navigation__mobile__item"><a href="{{ route('sponsors') }}" class="navigation__mobile__link">{{ __('navigation.parceiros') }}</a></li>
                    <li class="navigation__mobile__item"><a href="{{ route('news') }}" class="navigation__mobile__link">{{ __('navigation.news') }}</a></li>
                    <li class="navigation__mobile__item"><a href="{{ route('contacts') }}" class="navigation__mobile__link">{{ __('navigation.contacts') }}</a></li>
                    <li class="navigation__mobile__item"><a href="https://shop.dancefloor.pt/" class="navigation__mobile__link">{{ __('navigation.store') }}</a></li>
                    <div class="lang">
                        <li class="navigation__item navigation__item--language"><a href="{{ LaravelLocalization::getLocalizedURL('pt', null, [], true) }}" class="navigation__link {{ (LaravelLocalization::getCurrentLocale() == 'pt') ? 'active' : '' }}">PT</a></li>
                        <li class="navigation__item navigation__item--language"><a href="{{ LaravelLocalization::getLocalizedURL('en', null, [], true) }}" class="navigation__link {{ (LaravelLocalization::getCurrentLocale() == 'en') ? 'active' : '' }}">EN</a></li>
                    </div>
                </ul>
            </nav>
        </div>
    </header>
