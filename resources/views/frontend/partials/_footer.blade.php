<footer class="footer">
        <div class="footer__partners" style="{{ (\Request::route()->getName() == 'sponsors') ? 'display:none;' : '' }}">
            <div class="u-center-text" style="    margin-top: 3rem;">
                <h2 class="heading-secondary heading-secondary--purple">
                    {{ __('footer.partners') }}
                </h2>
            </div>

            <div class="swiper-container">
               <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <img src="{{ asset('/images/sponsors/Logo-Bold-Cinza-1-1.png')}}" alt="Slider" class="swiper-image">
                    </div>
                    <div class="swiper-slide">                    
                        <img src="{{ asset('/images/sponsors/Logo-Caticom-Cinza.png')}}" alt="Slider" class="swiper-image">
                    </div>
                    <div class="swiper-slide">                    
                        <img src="{{ asset('/images/sponsors/Logo-Comup-Cinza.png')}}" alt="Slider" class="swiper-image">
                    </div>
                    <div class="swiper-slide">                    
                        <img src="{{ asset('/images/sponsors/Logo-Young-Network-Group-Cinza.png')}}" alt="Slider" class="swiper-image">
                    </div>
                    <div class="swiper-slide">                    
                        <img src="{{ asset('/images/sponsors/Logos-Parceiros-Backstage-Cinza.jpg')}}" alt="Slider" class="swiper-image">
                    </div>
                    <div class="swiper-slide">                    
                        <img src="{{ asset('/images/sponsors/Logo-2MEvent-Cinza.png')}}" alt="Slider" class="swiper-image">
                    </div>
                    <div class="swiper-slide">                    
                        <img src="{{ asset('/images/sponsors/Logo-Les-Halles-Du-Portugal-Cinza.png')}}" alt="Slider" class="swiper-image">
                    </div>
                    <div class="swiper-slide">                    
                        <img src="{{ asset('/images/sponsors/Logo-Luso-Conseil-Cinza.png')}}" alt="Slider" class="swiper-image">
                    </div>
                    <div class="swiper-slide">                    
                        <img src="{{ asset('/images/sponsors/Logo-Mz-Voyages-Cinza.png')}}" alt="Slider" class="swiper-image">
                    </div>
                    <div class="swiper-slide">                    
                        <img src="{{ asset('/images/sponsors/Logo-Subtil-Cinza.png')}}" alt="Slider" class="swiper-image">
                    </div>
                    <div class="swiper-slide">                    
                        <img src="{{ asset('/images/sponsors/Logo-TTS-Cinza.png')}}" alt="Slider" class="swiper-image">
                    </div>
                    <div class="swiper-slide">                    
                        <img src="{{ asset('/images/sponsors/Logo-ptoTravel-Cinza.png')}}" alt="Slider" class="swiper-image">
                    </div>
                    <div class="swiper-slide">                    
                        <img src="{{ asset('/images/sponsors/Logos-Parceiros-AlticeForumBraga.png')}}" alt="Slider" class="swiper-image">
                    </div>
				    <div class="swiper-slide">                    
                        <img src="{{ asset('/images/sponsors/Logo-Wide-Future-Cinza.png')}}" alt="Slider" class="swiper-image">
                    </div>
				   <div class="swiper-slide">                    
                        <img src="{{ asset('/images/sponsors/Logo-Katra-Cinza.png')}}" alt="Slider" class="swiper-image">
                    </div>
				   <div class="swiper-slide">                    
                        <img src="{{ asset('/images/sponsors/Logo-CP-PB.png')}}" alt="Slider" class="swiper-image">
                    </div>
				   <div class="swiper-slide">                    
                        <img src="{{ asset('/images/sponsors/Logo-Meo-Cinza.png')}}" alt="Slider" class="swiper-image">
                    </div>
				      <div class="swiper-slide">                    
                        <img src="{{ asset('/images/sponsors/Logo-Curtir-Festivais.png')}}" alt="Slider" class="swiper-image">
                    </div>
				   

				   <!--
				   <div class="swiper-slide">                    
                        <img src="{{ asset('/images/sponsors/Logo-TUB-Cinza.jpg')}}" alt="Slider" class="swiper-image">

                    </div>
				   -->
				   
                </div>
            </div>             
        </div>
        <div class="footer__content u-center-text">
            <a class="btn btn--pink u-margin-top-big" href="{{ route('contacts') }}">{{ __('footer.contact-us') }}</a>
			
			
			<div class="footer__cp u-margin-top-negative">
                <img src="{{ asset('images/sponsors/CP-Selo.png') }}" alt="CP-Selo" class="">
            </div>
			
		
			
            <div class="footer__bottom u-margin-top-medium">
                <div class="footer__copyright">
                    <h3 class="heading-tertiary u-left-text"> 
                        <span class="heading-tertiary--main footer__copyright--text">DANCEFLOOR <?php echo date("Y"); ?></span>
                        <span class="heading-tertiary--sub footer__copyright--text">{{ __('footer.copyright') }}</span>
                    </h3>
                </div>
                <div class="footer__social">
                    <a href="https://www.facebook.com/dancefloorjump/" target="_blank"><i class="fab fa-facebook-f"></i></a>
                    <a href="https://www.instagram.com/dancefloorjump/" target="_blank"><i class="fab fa-instagram"></i></a>
                    <a href="https://twitter.com/dancefloorjump" target="_blank"><i class="fab fa-twitter"></i></a>
                    <a href="https://www.youtube.com/channel/UCLhSdC5slL8s3JUvcrnV3Dg" target="_blank"><i class="fab fa-youtube"></i></a>
                </div>
                <div class="footer__links">
                    <ul class="footer__navigation">
                        <!---<li class="footer__navigation__item"><a href="#" class="footer__navigation__link">{{ __('footer.faqs') }}</a></li>-->
                        <li class="footer__navigation__item"><a href="{{ route('politica') }}" class="footer__navigation__link">{{ __('footer.policy') }}</a></li>
						<li class="footer__navigation__item"><a href="{{ route('concurso') }}" class="footer__navigation__link">{{ __('footer.concurso') }}</a></li>

                        <!---<li class="footer__navigation__item"><a href="#" class="footer__navigation__link">{{ __('footer.terms') }}</a></li>-->
                    </ul>
                </div>
            </div>
        </div>
    </footer>
    
    <script>
    
    var swiper = new Swiper('.swiper-container', {
      slidesPerView: 8,
      autoplay: {
        delay: 1500,
        disableOnInteraction: false,
      },
      breakpoints: {
        768: {
          slidesPerView: 2,
        },
      }
    });
    </script>
