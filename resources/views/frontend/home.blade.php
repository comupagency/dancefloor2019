<!DOCTYPE html>
<html lang="en">
    <head>
    
        <meta property="og:url"           content="https://df.comup-dev.pt" />
        <meta property="og:title"         content="DANCEFLOOR | #JUMPTOTHEDROP" />
        <meta property="og:type"          content="website" />
        <meta property="og:description"   content="O Dancefloor traz os melhores DJ's de hardstyle e EDM a Portugal." />
        <meta property="og:image"         content="{{asset('/img/logo.png')}}" />


        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,600i,700,800,800i" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('css/frontend/style.css') }}">
        <link rel="shortcut icon" type="image/png" href="{{ asset('img/favicon.png') }}">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.6/css/swiper.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">


        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.6/js/swiper.min.js"></script>
        <script src="{{ asset('js/frontend/style.js') }}"></script>
        <script src='https://www.google.com/recaptcha/api.js'></script>

        
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-113582586-1"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-113582586-1');
        </script>


        <title>Dancefloor | #JumpToTheDrop</title>
    </head>

<body>
    @include('frontend.partials._navigation')

    <main>
        <section class="section-intro u-center-text">
            <h1 class="heading-primary">
                <span class="heading-primary--main">Dancefloor</span>
                <span class="heading-primary--sub"><span class="heading-primary--sub-1">#</span>Jumptothedrop</span>
            </h1>

            <div class="u-center-text u-margin-top-medium">
                    <h2 class="heading-secondary">
                        {{ __('home.date') }}
                    </h2>
            </div>

            <a id="buynow" class="btn btn--blue u-margin-top-big" href="{{ route('tickets') }}">{{ __('home.buy') }}</a>
			<script>

					//analytics triggers

					jQuery('#buynow').on('click', function(){
							ga.getAll()[0].send('event', 'Botão Comprar Bilhete', 'clique')
					});

			</script>
				
        </section>

        <section class="section-about u-center-text">
            <div class="row">
                <div class="u-center-text u-margin-bottom-big">

                    <p class="paragraph paragraph--home u-margin-top-small">
                        <strong>{{ __('home.dancefloor') }}</strong> {{ __('home.intro-text-1') }} <strong>{{ __('home.intro-text-date') }}</strong>.
                    </p>
                    <p class="paragraph paragraph--home">
                        {{ __('home.intro-text-2')}} <strong>{{ __('home.intro-text-local') }}</strong>.
                    </p>
                </div>
            </div>
        </section>

        <section class="section-video">
            <div class="row">
                <div class="youtube__video">
                    <img src="/img/youtube_main.jpg" class="youtube__image">
                    <div class="youtube__icon__holder">
                        <a class="youtube__popup" data-video="lEv4PkOcJUA"> <i class="fab fa-youtube youtube__icon"></i> </a>
                    </div>
                </div>
            </div>
        </section>
        <div id="ytplayer"></div>
        
        <section class="section-news">
            <div class="u-center-text">
                <div class="row">
                    <div class="u-center-text u-margin-bottom-big">
                        <h2 class="heading-secondary">
                            {{ __('home.news') }}
                        </h2>
                    </div>

                <div class="news">
                @foreach($news as $new)
                        @php  
                            $newsTtranlastions = DB::table('news_translations')->where('news_id', $new->id)
                            ->where('language_id', $language)->get();
                        @endphp
                    <div class="news__box">
                        <div class="news__image__holder">
                            <img src="{{ asset($new->thumbnail) }}" class="news__image">
                        </div>
                        <div class="news__title">
                           {{ $newsTtranlastions[0]->title }}
                        </div>
                        <div class="news__description">
                            <div class="news__description--1">
                                <a href="{{ route('new', ['name' => $newsTtranlastions[0]->news_id]) }}" class="btn btn--blue btn--small u-margin-top-small">{{ __('home.see-more') }}</a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                </div>
                <a href="{{ route('news') }}" class="btn btn--blue u-margin-bottom-medium">{{ __('home.all-news') }}</a>
            </div>

        </section>
        
    </main>
    
    @include('frontend.partials._footer')
    
</body>


<script src="{{ asset('js/frontend/youtube.js') }}"></script>
</html>