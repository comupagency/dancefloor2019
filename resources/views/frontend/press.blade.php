<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,600i,700,800,800i" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('css/frontend/style.css') }}">
        <link rel="shortcut icon" type="image/png" href="{{ asset('img/favicon.png') }}">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.6/css/swiper.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">


        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.6/js/swiper.min.js"></script>
        <script src="{{ asset('js/frontend/style.js') }}"></script>
        <script src='https://www.google.com/recaptcha/api.js'></script>

        
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-113582586-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-113582586-1');
</script>


        <title>Dancefloor | #JumpToTheDrop</title>
    </head>



<body>
    @include('frontend.partials._navigation')


    <main>
        <section class="section-intro u-center-text">
            <div class="u-center-text u-margin-bottom-big">
                <h2 class="heading-secondary heading-big">
                    press
                </h2>
                <a id="buynow" class="btn btn--blue u-margin-top-medium" href="{{ route('tickets') }}">{{ __('home.buy') }}</a>
								<script>

					//analytics triggers

					jQuery('#buynow').on('click', function(){
							ga.getAll()[0].send('event', 'Botão Comprar Bilhete', 'clique')
					});

				</script>
            </div> 
        </section>

        <section class="section-press">
            <div class="press__content">
                <div class="row">
                    <div class="press__description u-margin-bottom-medium">
                        <p class="press__text">{{ __('press.text-1') }}:</p>
                        <p class="press__text">{{ __('press.text-2') }}</p>
                        <p class="press__text">{{ __('press.text-3') }}:</p>
                        
                    </div>
                    <div class="press__contacts  u-margin-bottom-medium">
                        <a class="btn btn--pink btn--small u-margin-bottom-medium" href="mailto:press@2mevent.com"> {{ __('press.send-email') }} </a>
                        <a class="btn btn--blue btn--small u-margin-bottom-medium" href="{{ asset('img/Logo-Top-Dancefloor.png')}}" download="Logo-Top-Dancefloor.png">Press Kit</a>
                    </div>
                    <div class="press__description">
                        <p class="press__text">{{ __('press.text-4') }}.</p>
                    </div>
                </div>
            </div>
        </section>
    </main>

    @include('frontend.partials._footer')

    
</body>

</html>