<!DOCTYPE html>
<html lang="en">
    <head>
    
        <meta property="og:url"           content="{{ url()->current() }}" />
        <meta property="og:title"         content="DANCEFLOOR - {{ $newsTtranlastions->title }}" />
        <meta property="og:type"          content="article" />
        <meta property="og:image"         content="{{ asset($new->thumbnail) }}" />


        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,600i,700,800,800i" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('css/frontend/style.css') }}">
        <link rel="shortcut icon" type="image/png" href="{{ asset('img/favicon.png') }}">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.6/css/swiper.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">


        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.6/js/swiper.min.js"></script>
        <script src="{{ asset('js/frontend/style.js') }}"></script>
        <script src='https://www.google.com/recaptcha/api.js'></script>


        
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-113582586-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-113582586-1');
</script>


        <title>Dancefloor | #JumpToTheDrop</title>
    </head>



<body>
    @include('frontend.partials._navigation')

    <main>
        <section class="section-new u-center-text">
            <div class="u-center-text u-margin-bottom-big">
                <a id="buynow" class="btn btn--blue u-margin-top-medium" href="{{ route('tickets') }}">{{ __('home.buy') }}</a>
				<script>

					//analytics triggers

					jQuery('#buynow').on('click', function(){
							ga.getAll()[0].send('event', 'Botão Comprar Bilhete', 'clique')
					});

				</script>
			</div> 
        </section>


        <section class="new">
            <div class="row">
                <div class="new__image--holder u-margin-top-medium">
                    <img src="{{ $new->thumbnail }}" alt="Nes" class="new__image">
                    <div class="new__share">
                        @php 
                            $newLink = url('/').'/news/'.$new->id;
                        @endphp

                        <div class="fb-share-button artist__share__buttons" data-href="<?php echo $newLink; ?>" data-layout="button" data-size="large" data-mobile-iframe="true">
                            <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $newLink; ?>&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">
                            Partilhar
                            </a>
                        </div>
                        
                        <a class="twitter-share-button" target="_blank"
                        href="https://twitter.com/intent/tweet?text=<?php echo $newsTtranlastions->title; ?>&url=<?php echo $newLink; ?>&hashtags=DANCEFLOOR,JUMPTOTHEDROP" data-size="large">
                        Tweet</a>

                    </div>
                </div>

                <div class="new__title u-margin-top-medium">
                    {{ $newsTtranlastions->title }}
                </div>

                <div class="new__subtitle">
                    {{ $newsTtranlastions->subtitle }}
                </div>

                <div class="new__description--holder u-margin-top-big">
                    {!! $newsTtranlastions->description !!}
                </div>

                <div class="artist__navigation u-margin-top-medium u-padding-bottom-big">
                    
                    @if($previousId)
                        <a href="{{ route('new', ['id' => $previousId] )}}" class="btn btn--artist btn--artist--blue artist__navigation--title"><i class="fas fa-caret-left artist__navigation--icon_pink"></i> {{ __('home.previous-new') }} </a>
                    @endif

                    @if($nextId)
                        <a href="{{ route('new', ['id' => $nextId] )}}" class="btn btn--artist btn--artist--blue artist__navigation--title"> {{ __('home.next-new') }} <i class="fas fa-caret-right artist__navigation--icon_blue"></i></a>
                    @endif

                
                </div>

            </div>
        </section>
    </main>
    @include('frontend.partials._footer')

</body>

<div id="fb-root"></div>
<script src="{{ asset('js/frontend/facebook-twitter.js') }}"> </script>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

</html>