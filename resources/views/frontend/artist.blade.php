<!DOCTYPE html>
<html lang="en">
    <head>
    
        <meta property="og:url"           content="{{ url()->current() }}" />
        <meta property="og:title"         content="DANCEFLOOR - {{ $artistName }}" />
        <meta property="og:type"          content="article" />
        <meta property="og:image"         content="{{ asset($artistImage) }}" />

        <meta name="twitter:card" content="summary_large_image">
        <meta name="twitter:site" content="{{ url()->current() }}">
        <meta name="twitter:title" content="DANCEFLOOR - {{ $artistName }}">
        <meta name="twitter:image" content="{{ asset($artistImage) }}">


        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,600i,700,800,800i" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('css/frontend/style.css') }}">
        <link rel="shortcut icon" type="image/png" href="{{ asset('img/favicon.png') }}">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.6/css/swiper.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">


        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.6/js/swiper.min.js"></script>
        <script src="{{ asset('js/frontend/style.js') }}"></script>
        <script src='https://www.google.com/recaptcha/api.js'></script>

        
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-113582586-1"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-113582586-1');
        </script>


        <title>Dancefloor | #JumpToTheDrop</title>
    </head>



<body>

    <div id="fb-root"></div>
    <script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = 'https://connect.facebook.net/pt_PT/sdk.js#xfbml=1&version=v3.2&appId=2024650511144113&autoLogAppEvents=1';
    fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

    @include('frontend.partials._navigation')

    <main>
        <section class="section-artist u-center-text">
            <div class="u-center-text u-margin-bottom-big">
                <h2 class="heading-secondary heading-big">
                    {{ $artistName }}
                </h2>
                <a id="buynow" class="btn btn--blue u-margin-top-medium" href="{{ route('tickets') }}">{{ __('home.buy') }}</a>
				<script>

					//analytics triggers

					jQuery('#buynow').on('click', function(){
							ga.getAll()[0].send('event', 'Botão Comprar Bilhete', 'clique')
					});

				</script>
			
			</div> 
        </section>


        <section class="artist">
            <div class="row">
                <div class="artist__image--holder">
                    <img src="{{ $artistImage }}" alt="Artist" class="artist__image">
                    <div class="artist__share">


                    </div>  
                </div>
                <div class="artist__description--holder u-margin-top-big" style="color: white">
                    {!! $artistDescription !!}
                </div>

                <div class="artist__social u-margin-top-medium">
                    <a href={!! $artist->facebook !!} ><i class="fab fa-facebook-f artist_social--icon"></i></a>
                    <a href="{!! $artist->youtube !!}" ><i class="fab fa-instagram artist_social--icon"></i></a>
                </div>


                <div class="artist__navigation u-margin-top-medium u-padding-bottom-big">
                    
                    @if($previousName)
                        <a href="{{ route('artist', ['id' => $previous] )}}" class="btn btn--artist btn--artist--blue artist__navigation--title"><i class="fas fa-caret-left artist__navigation--icon_pink"></i>{{ __('home.previous-artist') }}</a>
                    @endif

                    @if($nextName)
                        <a href="{{ route('artist', ['id' => $next] )}}" class="btn btn--artist btn--artist--blue artist__navigation--title">{{ __('home.next-artist') }}<i class="fas fa-caret-right artist__navigation--icon_blue"></i></a>
                    @endif

                
                </div>
            </div>
        </section>
    </main>
    @include('frontend.partials._footer')

</body>
<div id="fb-root"></div>
<script src="{{ asset('js/frontend/facebook-twitter.js') }}"> </script>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

</html>