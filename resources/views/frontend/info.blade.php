<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,600i,700,800,800i" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('css/frontend/style.css') }}">
        <link rel="shortcut icon" type="image/png" href="{{ asset('img/favicon.png') }}">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.6/css/swiper.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">


        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.6/js/swiper.min.js"></script>
        <script src="{{ asset('js/frontend/style.js') }}"></script>
        <script src='https://www.google.com/recaptcha/api.js'></script>


        
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-113582586-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-113582586-1');
</script>


        <title>Dancefloor | #JumpToTheDrop</title>
    </head>


<body>
    @include('frontend.partials._navigation')


    <main>
        <section class="section-intro u-center-text">
            <div class="u-center-text u-margin-bottom-big">
                <h2 class="heading-secondary heading-big">
                    {{ __('info.info') }}
                </h2>
                <a id="buynow" class="btn btn--blue u-margin-top-medium" href="{{ route('tickets') }}">{{ __('home.buy') }}</a>
				<script>

					//analytics triggers

					jQuery('#buynow').on('click', function(){
							ga.getAll()[0].send('event', 'Botão Comprar Bilhete', 'clique')
					});

				</script>
			</div> 
        </section>


        <section class="section-info">
            <div class="info__content">
                <div class="row">
                    <div class="u-center-text u-margin-top-medium">
                        <h2 class="heading-secondary">
                            {{ __('info.when') }}
                        </h2>
                        <p class="info__paragraph u-margin-top-medium">{{ __('info.date') }}</p>
                    </div>

                    <div class="u-center-text u-margin-top-medium">
                        <h2 class="heading-secondary">
                            {{ __('info.where') }}
                        </h2>
                        <p class="info__paragraph u-margin-top-medium">ALTICE FORUM BRAGA</p>
                    </div>

                    <div class="u-center-text u-margin-top-medium">
                        <h2 class="heading-secondary">
                            {{ __('info.how-to') }}
                        </h2>
                    
                        <div class="info__map u-margin-top-medium u-margin-bottom-medium">
                            <a href="https://goo.gl/maps/HctCHjAuRbs" target="_blank" class="info__googlemaps">{{ __('info.maps') }}</a>
                            
                            <div class="info__adress">
                                <p class="info__adress__content">Av.Dr.Francisco Pires Gonçalves</p>
                                <p class="info__adress__content">4711-909 Braga</p>
                            </div>
                        </div>
                    </div>

                    <div class="info__transport u-margin-top-huge">
                        <div class="tab">
                            <button class="tab__links" onclick="openTransport(event, 'Car')"  id="defaultOpen">
                                <img class="tab__image" src="{{ asset('/img/car.png') }}" alt="Car">
                                <h3 class="tab__title heading-tertiary heading-tertiary--blue u-margin-top-small">{{ __('info.carro') }}</h3>
                            </button>
                            <button class="tab__links" onclick="openTransport(event, 'Bus')">
                                <img class="tab__image" src="{{ asset('/img/bus.png') }}" alt="Bus">
                                <h3 class="tab__title heading-tertiary heading-tertiary--blue u-margin-top-small">{{ __('info.autocarro') }}</h3>
                            </button>
                            <button class="tab__links" onclick="openTransport(event, 'Train')">
                                <img class="tab__image" src="{{ asset('/img/train.png') }}" alt="Train">
                                <h3 class="tab__title heading-tertiary heading-tertiary--blue u-margin-top-small">{{ __('info.comboio') }}</h3>
                            </button>
                        </div>

                        <div id="Car" class="tab__content">
                            <p>{{ __('info.carro-text-1') }}.</p>
                            <p>{{ __('info.carro-text-2') }}.</p>
                        </div>

                        <div id="Bus" class="tab__content">
                            <p>{{ __('info.autocarro-text')}}.</p>
                        </div>

                        <div id="Train" class="tab__content">
                            <p> {{ __('info.comboio-text-1') }}.</p>
                            <p> {{ __('info.comboio-text-2') }}.</p>
                            <p> {{ __('info.comboio-text-3') }}.</p>
                        </div>
                    </div>

                    <div class="u-center-text u-margin-top-medium">
                        <h2 class="heading-secondary">
                            {{ __('info.how-to-park') }}
                        </h2>
                    </div>

                    <div class="info__camping u-margin-top-medium u-margin-bottom-medium"></div>

                    <div class="info__transport u-margin-top-huge">
                        <div class="tab__campismo">
                            <button class="tab__campismo__links__campismo" onclick="openCamping(event, 'campismo1')" id="campingOpen">
                                <img class="tab__campismo__image__campismo" src="{{ asset('/img/icone-camping.png') }}" alt="Car">
                                <h3 class="tab__campismo__title__campismo heading-tertiary heading-tertiary--blue u-margin-top-small"> {{ __('info.camping') }}</h3>
                            </button>
                        </div>

                        <div id="campismo1" class="tab__campismo__content__campismo">
                            <p>{{ __('info.camping-text-1')}}.</p>
                            <p>{{ __('info.camping-text-2')}}. </p>
                            <br>
                            <p>{{ __('info.camping-address-1')}}: </p>
                            <p>{{ __('info.camping-address-2')}}</p>
                            <p>{{ __('info.camping-address-3')}}</p>
                            <p>{{ __('info.camping-address-4')}}</p>   
                        </div>
                    </div>

                </div>
            </div>
        </section>

    </main>

    @include('frontend.partials._footer')
    
</body>


<script>

function openTransport(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tab__content");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tab__links");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" tab__active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " tab__active";
}


function openCamping(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tab__content__campismo");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tab__links__campismo");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" tab__active__campismo", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " tab__active__campismo";
}

document.getElementById('defaultOpen').click();
document.getElementById('campingOpen').click();

</script>

</html>