<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,600i,700,800,800i" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('css/frontend/style.css') }}">
        <link rel="shortcut icon" type="image/png" href="{{ asset('img/favicon.png') }}">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.6/css/swiper.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">


        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.6/js/swiper.min.js"></script>
        <script src="{{ asset('js/frontend/style.js') }}"></script>
        <script src='https://www.google.com/recaptcha/api.js'></script>

        
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-113582586-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-113582586-1');
</script>


        <title>Dancefloor | #JumpToTheDrop</title>
    </head>



<body>
    @include('frontend.partials._navigation')

     <main>
        <section class="section-intro u-center-text">
            <div class="u-center-text u-margin-bottom-big">
                <h2 class="heading-secondary heading-big">
                    {{ __('tickets.ticket') }}
                </h2>
            </div> 
        </section>

        <section class="section-tickets">
            <div class="tickets__content">
                <div class="row">
                    <div class="tickets__day">
                        <h2 class="heading-secondary">
                            {{ __('tickets.tickets') }}
                        </h2>

                        <div class="tickets__buttons u-margin-top-big">
                            <div class="tickets__buttons-div">
                                    <a id="1dayticket" href="https://ticketline.sapo.pt/evento/40503" target="_blank" class="btn btn--blue btn--small u-margin-bottom-medium tickets__style">
                                            <img src="/img/ticket1.png" class="tickets__image" alt="">
                                            {{ __('tickets.ticket-daily')}} <span class="tickets__price">19&euro;</span> </a>
                                    
                                    <a id="2dayticket" 
                                        href="https://ticketline.sapo.pt/evento/40503" target="_blank" class="btn btn--blue btn--small u-margin-bottom-medium tickets__style">
                                            <img src="/img/ticket2.png" class="tickets__image" alt="">
                                            {{ __('tickets.ticket-double')}} <span class="tickets__price">24&euro;</span> </a>

                                    <a id="2daycamping"  href="https://ticketline.sapo.pt/evento/40503" target="_blank" class="btn btn--blue btn--small u-margin-bottom-medium tickets__style">
                                            <img src="/img/ticket3.png" class="tickets__image" alt="">
                                            {{ __('tickets.ticket-double-camping')}} <span class="tickets__price">42&euro;</span> </a>
         

  
                        </div>
                    </div>

<!---
                    <div class="tickets__day u-margin-top-big">
                        <h2 class="heading-secondary">
                            {{ __('tickets.packs') }}
                        </h2>

                        <div class="tickets__buttons u-margin-top-big">
                            <div class="tickets__buttons-div">
                            </div>
                        </div>
                    </div>
-->

                        <div class="tickets__day u-margin-top-big">
                            <div class="tickets__buttons u-margin-top-big">
                                <div class="tickets__buttons-div">
                                    <a id="masqueticket"  href="https://www.masqueticket.com/entrada/563-dancefloor-festival-braga" target="_blank" class="btn btn--blue btn--small u-margin-bottom-medium tickets__style">
                                        Masqueticket
                                    </a>                                
                                </div>
                            </div>
                        </div>
						
						<div class="tickets__day u-margin-top-small">
                            <div class="tickets__buttons u-margin-top-small">
                                <div class="tickets__buttons-div">
                                    <a onClick="ga('send', {
                                        hitType: 'event',
                                        eventCategory: 'Bilhetes',
                                        eventAction: 'FesTicket',
                                      });" href="https://www.festicket.com/pt/festivals/dancefloor-leiria/2019/" target="_blank" class="btn btn--blue btn--small u-margin-bottom-medium tickets__style">
                                        FesTicket
                                    </a>                                
                                </div>
                            </div>
						</div>
                </div>
            </div>
        </section>
    </main>

    @include('frontend.partials._footer')

</body>

<script>

//ticket analytics triggers

jQuery('#1dayticket').on('click', function(){
        ga.getAll()[0].send('event', 'Bilhetes', 'Bilhete diário')
});

jQuery('#2dayticket').on('click', function(){
        ga.getAll()[0].send('event', 'Bilhetes', 'Bilhete 2 dias')
});

jQuery('#2daycamping').on('click', function(){
        ga.getAll()[0].send('event', 'Bilhetes', 'Bilhete 2 dias + camping')
});

jQuery('#masqueticket').on('click', function(){
        ga.getAll()[0].send('event', 'Bilhetes', 'Masqueticket')
});
</script>


</html>

