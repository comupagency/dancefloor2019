<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'festival' => 'THE FESTIVAL',
    'dancefloor' => 'Dancefloor’s ',
    'intro-text-1' => 'the festival that promotes the best in electronic music and it’s back, from ',
    'intro-text-date' => 'the 26th to the 27th of July 2019',
    'intro-text-2' => 'That which will be the 5th edition of the event, will welcome the biggest and most famous national and international DJs. This year’s edition takes place at the',
    'intro-text-local' => ' Altice Forum in Braga',
    'news' => 'News',
    'all-news' => 'All news',
    'see-more' => 'See more',
    'next-new' => 'Next News',
    'previous-new' => 'Previous News',
    'next-artist' => 'Next artist',
    'previous-artist' => 'Previous artist',
    'date' => '26 - 27 July',
    'buy' => 'Buy now',

];
