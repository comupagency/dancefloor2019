<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'text-1' => 'THE REQUESTS FOR ACCREDITATION MUST BE DONE BY THE EDITOR AND SHOULD INDICATE',
    'text-2' => 'NUMBER OF ACCREDITATIONS, PROFESSIONAL LICENSE NUMBER, FUNCTIONS HELD  AND THE NAME OF THE MEDIA OUTLET REPRESENTED',
    'text-3' => 'MUST BE SUBMITTED UNTIL THE 26TH OF JULY TO THE FOLLOWING EMAIL',
    'text-4' => 'REQUESTS RECEIVED AFTER THE SPECIFIED DATE MAY NOT BE CONSIDERED',

    'send-email' => 'send email',

];