<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'ticket' => 'Tickets',
    'tickets' => 'CHOOSE YOUR TICKET',
    'packs' => 'CHOOSE YOUR PACK',
    'ticket-daily' => 'DAILY TICKET',
    'ticket-double' => '2 DAY TICKET',
    'ticket-double-camping' => '2 DAY TICKET + CAMPINg',
];