<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'partners' => 'Partners',
    'copyright' => 'All rights reserved',
    'faqs' => 'faqs',
    'policy' => 'Privacy policy',
    'terms' => 'Terms & conditions',
    'contact-us' => 'Contact us',
	'concurso' => 'Contest',

];