<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'info' => 'Info',
    'when' => 'When',
    'where' => 'Where',
    'how-to' => 'How to get there',
    'date' => 'JULY 26 - 27',
    'maps' => 'Directions',
    'carro' => 'By car',
    'carro-text-1' => 'From Porto, follow the A3 to Braga. It is approximately 55 km to the center of Braga',
    'carro-text-2' => 'From Lisbon, follow the A1. By following the signs take the A3 and a small part of the A11, straight to Braga. The route is approximately 360 km',
    'autocarro' => 'By Bus',
    'autocarro-text' => 'Rede Expressos provides bus service to Braga, leaving from the Lisbon-Sete Rios bus station and going through the city of Porto. Braga’s bus station is 0.8km from the city center',
    'comboio' => 'By train',
    'comboio-text-1' => 'There is a direct train connection between Lisbon and Braga',
    'comboio-text-2' => 'The Alfa Pendular trains from the Portuguese train company CP, leave from the Lisbon- Santa Apolónia station to Braga, going through Porto',
    'comboio-text-3' => 'Braga’s train station is  1.1 km from the city center',
    'how-to-park' => 'GETTING TO THE PARK',
    'camping' => 'CAMPING',
    'camping-text-1' => 'Braga municipality’s Camping and Caravan Park is in the southern area of the urban park known as Parque da Ponte, in a leisure area inside a sports complex with the same name and contiguous t the Urban Park of Picoto. A wonderful shady park in a privileged location to explore the surrounding city in all of its architectonic and historical splendour',
    'camping-text-2' => 'The camping site, almost in the city center, provides all the tranquillity and peace and quiet with its sport and leisure areas from where there is a magnificent view over the city',
    'camping-address-1' => 'Address',
    'camping-address-2' => 'Av. Dr. Viriato Amaral Nunes S/n',
    'camping-address-3' => '4715-214 S.Lázaro, Braga',
    'camping-address-4' => 'Portugal',
];