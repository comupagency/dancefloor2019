<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'festival' => 'O FESTIVAL',
    'dancefloor' => 'Dancefloor',
    'intro-text-1' => ', é um festival que promove o melhor da música eletrónica e está de regresso nos dias',
    'intro-text-date' => '26 e 27 de julho 2019',
    'intro-text-2' => 'Aquela que será a 5ª edição do evento, contará com a presença dos maiores e mais mediáticos DJs nacionais e internacionais. Esta edição terá lugar no ',
    'intro-text-local' => 'Altice Forum, em Braga',

    'news' => 'Notícias',
    'all-news' => 'Todas as novidades',
    'see-more' => 'Ver mais',
    'next-new' => 'Proxima notícia',
    'previous-new' => 'Notícia Anterior',
    'next-artist' => 'Proximo Artista',
    'previous-artist' => 'Artista Anterior',
    'date' => '26 - 27 JULHO',
    'buy' => 'Compra já',
];