<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'ticket' => 'Bilhetes',
    'tickets' => 'ESCOLHE O TEU BILHETE',
    'packs' => 'ESCOLHE O TEU PACK',
    'ticket-daily' => 'BILHETE DIÁRIO',
    'ticket-double' => 'BILHETE 2 dias',
    'ticket-double-camping' => 'BILHETE 2 dias + camping',

];