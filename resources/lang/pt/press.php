<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'text-1' => 'Os pedidos de acreditação devem ser feitos pelo editor e devem indicar',
    'text-2' => 'Número de acreditações, Número da carteira profissional, Funções e nome do Órgão',
    'text-3' => 'de Comunicação que representam',
    'text-4' => 'Devem ser enviados até de 26 de julho de 2019 para o email',
    'text-5' => 'OS PEDIDOS RECEBIDOS APÓS ESSA DATA PODEM NÃO SER CONSIDERADOS',
    'send-email' => 'Enviar email',


];