<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'info' => 'Informações',
    'when' => 'Quando',
    'where' => 'Onde',
    'how-to' => 'Como Chegar',
    'date' => '26 - 27 JULHO',
    'maps' => 'Obter direções',
    'carro' => 'Carro',
    'carro-text-1' => 'Vindo do Porto, segue pela A3, aproximadamente 55 km de viagem até ao centro de Braga',
    'carro-text-2' => 'De Lisboa, segue pela A1, seguindo as placas, depois pela A3 e um pequeno trecho na A11, direto até Braga. O trajeto é de aproximadamente 360 km',
    'autocarro' => 'Autocarro',
    'autocarro-text' => 'A rede Expresso assegura os serviços de autocarro até Braga, com partida no terminal rodoviário de Lisboa - Sete Rios, passando pela cidade do Porto. A estação rodoviária de Braga fica a 0.8km do centro da cidade',
    'comboio' => 'comboio',
    'comboio-text-1' => 'O comboio Lisboa Braga é direto',
    'comboio-text-2' => 'Os comboios Alfa Pendular da CP partem da estação de Lisboa - Santa Apolónia até a estação de Braga, passando pela estação do Porto',
    'comboio-text-3' => 'A estação de Braga fica a 1.1 km do centro',
    'how-to-park' => 'Como chegar ao parque',
    'camping' => 'campismo',
    'camping-text-1' => 'O Parque de Campismo e Caravanismo Municipal de Braga localiza-se na zona sul do parque urbano, conhecido por Parque da Ponte, numa área de lazer inserida num complexo desportivo, com o mesmo nome e contiguo ao parque urbano do picoto. Oferece espaços com excelente sombra e é, pela sua localização, um local privilegiado para explorar a cidade circundante com todo o seu esplendor arquitetónico e histórico',
    'camping-text-2' => 'Este sítio, que nos proporciona tranquilidade, praticamente no centro da cidade, com a sua envolvência de áreas desportivas e de lazer, onde se pode disfrutar da magnifica panorâmica sobre a cidade de Braga',
    'camping-address-1' => 'Endereço',
    'camping-address-2' => 'Av. Dr. Viriato Amaral Nunes S/n',
    'camping-address-3' => '4715-214 S.Lázaro, Braga',
    'camping-address-4' => 'Portugal',

];