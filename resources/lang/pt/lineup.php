<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'lineup' => 'Cartaz',
    'day-one' => '26 julho',
    'day-two' => '27 Julho',
    'days' => '26 & 27 Julho',
    'confirm' => 'A aguardar confirmação',
];
