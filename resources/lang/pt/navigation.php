    <?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'home' => 'HOME',
    'cartaz' => 'CARTAZ',
    'info' => 'INFORMAÇÕES',
    'press' => 'PRESS',
    'bilhetes' => 'BILHETES',
    'parceiros' => 'PARCEIROS',
    'news' => 'NOTÍCIAS',
    'loja' => 'LOJA',
    'contacts' => 'CONTACTOS',
    'store' => 'LOJA',

];
