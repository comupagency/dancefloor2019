<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'partners' => 'Parceiros',
    'copyright' => 'Todos os direitos reservados',
    'faqs' => 'faqs',
    'policy' => 'Política de Privacidade',
    'terms' => 'Termos e condições',
    'contact-us' => 'Contacta-nos',
	'concurso' => 'Concurso',
];