<?php

use Faker\Generator as Faker;
use App\Sponsors;



$factory->define(App\SponsorsTranslations::class, function (Faker $faker) {

    $sponsors = Sponsors::all()->pluck('id')->toArray();
    static $increment = 1;
    $sponsorLanguage = sponsorLanguage();

    
    return [
        'description' => $faker->text(10),        
        'sponsors_id' => $increment++,
        'language_id' => $sponsorLanguage->current()

    ];
});

function sponsorLanguage()
{
    yield rand(1, 2); 
}