<?php

use Faker\Generator as Faker;
use App\Artists; 

 

$factory->define(App\ArtistsTranslations::class, function (Faker $faker) {

    $artists = Artists::all()->pluck('id')->toArray();
    static $increment = 1;
    $language = language();

    return [
        
        'description' => $faker->text(10),        
        'artists_id' => $increment++,
        'language_id' => $language->current()

    ];
});



function language()
{
    yield rand(1, 2); 
}