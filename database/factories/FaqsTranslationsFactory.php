<?php

use Faker\Generator as Faker;
use App\Faqs; 




$factory->define(App\FaqsTranslations::class, function (Faker $faker) {

    $faqs = Faqs::all()->pluck('id')->toArray();
    static $increment = 1;
    $faqLanguage = faqLanguage();

    return [
        'question' => $faker->text(10),  
        'answer' => $faker->text(10),       
        'faqs_id' => $increment++,
        'language_id' => $faqLanguage->current()

    ];
});

function faqLanguage()
{
    yield rand(1, 2); 
}