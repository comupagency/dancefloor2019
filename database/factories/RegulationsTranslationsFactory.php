<?php

use Faker\Generator as Faker;
use App\Regulations; 
use App\Translations; 



$factory->define(App\RegulationsTranslations::class, function (Faker $faker) {

    $regulations = Regulations::all()->pluck('id')->toArray();
    static $increment = 1;
    $regulationsLanguage = regulationsLanguage();

    
    return [

        'title' => $faker->text(10),  
        'description' => $faker->text(20),        
        'regulations_id' => $increment++,
        'language_id' => $regulationsLanguage->current()

    ];
});


function regulationsLanguage()
{
    yield rand(1, 2); 
}