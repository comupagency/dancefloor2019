<?php

use Faker\Generator as Faker;
use App\News; 

$factory->define(App\News::class, function (Faker $faker) {
    return [
        'thumbnail' => 'images/news/1/thumbnail',
        'main_image' => 'images/news/1/main-image'
    ];
});
