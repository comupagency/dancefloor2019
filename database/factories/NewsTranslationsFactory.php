<?php

use Faker\Generator as Faker;
use App\Translations; 
use App\News; 




$factory->define(App\NewsTranslations::class, function (Faker $faker) {

    $news = News::all()->pluck('id')->toArray();
    static $increment = 1;
    $newsLanguage = newsLanguage();

    return [

        'title' => $faker->text(10),  
        'subtitle' => $faker->text(10),
        'description' => $faker->text(10),        
        'news_id' => $increment++,
        'language_id' => $newsLanguage->current()
    ];
});


function newsLanguage()
{
    yield rand(1, 2); 
}