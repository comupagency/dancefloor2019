<?php

use Faker\Generator as Faker;
use App\SponsorsCategories;

$factory->define(App\SponsorsCategoriesTranslations::class, function (Faker $faker) {

    $categories = SponsorsCategories::all()->pluck('id')->toArray();
    static $increment = 1;
    $categoryLanguage = categoryLanguage();

    return [
        'name' => $faker->text(10),
        'sponsorscategories_id' => $increment++,
        'language_id' => $categoryLanguage->current()
    ];
});




function categoryLanguage()
{
    yield rand(1, 2); 
}
