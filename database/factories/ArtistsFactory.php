<?php

use Faker\Generator as Faker;

$factory->define(App\Artists::class, function (Faker $faker) {
    return [

        'name' => $faker->text(10),
        'day' => $faker->randomNumber(),
        'hours' => $faker->time(),
        'facebook' => 'www.facebook.com',
        'youtube' => 'www.youtube.com',
        'main_image' => 'images/artists/1/main-image/tst'
    ];
});
