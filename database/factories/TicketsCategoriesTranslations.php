<?php

use Faker\Generator as Faker;
use App\TicketsCategories; 

$factory->define(App\TicketsCategoriesTranslations::class, function (Faker $faker) {
    
    static $increment = 1;


    return [
        'name' => $faker->text(10),
        'ticketscategories_id' => $increment++,
        'language_id' => rand(1,2)
    ];
});

