<?php

use Faker\Generator as Faker;

$factory->define(App\Sponsors::class, function (Faker $faker) {

    static $increment = 1;
    $sponsorCategory = sponsorCategory();

    

    return [

        'name' => $faker->text(10),
        'logo' => 'images/news/1/logo',
        'category_id' => $sponsorCategory->current()

    ];
});


function sponsorCategory()
{
    yield rand(1, 5); 
}