<?php

use Faker\Generator as Faker;
use App\Tickets; 



$factory->define(App\TicketsTranslations::class, function (Faker $faker) {
    
    $tickets = Tickets::all()->pluck('id')->toArray();
    static $increment = 1;


    return [
        'name' => $faker->text(10),
        'tickets_id' => $increment++,
        'language_id' => rand(1,2)
    ];
});



