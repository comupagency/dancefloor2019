<?php

use Faker\Generator as Faker;
use App\SponsorsCategories;

$factory->define(App\SponsorsCategories::class, function (Faker $faker) {

    static $increment = 1;

    return [
        'order' => $increment++,
    ];
});
