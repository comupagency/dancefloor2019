<?php

use Faker\Generator as Faker;
use App\Tickets; 

$factory->define(App\Tickets::class, function (Faker $faker) {
    return [
        'price' => $faker->randomNumber(),
        'category_id' => rand(1,10),
        'main_image' => '/images/artists/1/main-image/tst',
        'link' => $faker->text(10)

    ];
});
