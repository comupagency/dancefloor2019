<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSponsorsTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sponsors_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('description');
            $table->unsignedInteger('sponsors_id');
            $table->unsignedInteger('language_id');
            $table->foreign('sponsors_id')->references('id')->on('sponsors');
            $table->foreign('language_id')->references('id')->on('languages');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sponsors_translations');
    }
}
