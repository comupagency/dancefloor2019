<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSponsorsCategoriesTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sponsors_categories_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name'); 
            $table->unsignedInteger('sponsorscategories_id');
            $table->unsignedInteger('language_id');
            $table->foreign('sponsorscategories_id')->references('id')->on('sponsors_categories');
            $table->foreign('language_id')->references('id')->on('languages');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sponsors_categories_translations');
    }
}
