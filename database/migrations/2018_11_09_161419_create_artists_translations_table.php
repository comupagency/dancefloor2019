<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArtistsTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('artists_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('description');
            $table->unsignedInteger('artists_id');
            $table->unsignedInteger('language_id');
            $table->foreign('artists_id')->references('id')->on('artists');
            $table->foreign('language_id')->references('id')->on('languages');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('artists_translations');
    }
}
