<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFaqsTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('faqs_translations', function (Blueprint $table) {

            $table->increments('id');
            $table->string('question');
            $table->string('answer');
            $table->unsignedInteger('faqs_id');
            $table->unsignedInteger('language_id');
            $table->foreign('faqs_id')->references('id')->on('faqs');
            $table->foreign('language_id')->references('id')->on('languages');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('faqs_translations');

    }
}
