<?php

use Illuminate\Database\Seeder;

class ArtistsTranslationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\ArtistsTranslations::class, 10)->create();
    }
}
