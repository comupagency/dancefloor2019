<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class LanguagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('languages')->insert([

            'name' => 'PT'

        ]);

        DB::table('languages')->insert([

            'name' => 'EN',

        ]);

        DB::table('languages')->insert([

            'name' => 'ES',

        ]);

    }
}
