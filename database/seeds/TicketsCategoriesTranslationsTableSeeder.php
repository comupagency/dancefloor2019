<?php

use Illuminate\Database\Seeder;

class TicketsCategoriesTranslationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\TicketsCategoriesTranslations::class, 10)->create();

    }
}
