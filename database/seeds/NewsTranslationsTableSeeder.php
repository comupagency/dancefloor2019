<?php

use Illuminate\Database\Seeder;

class NewsTranslationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\NewsTranslations::class, 10)->create();
    }
}
