<?php

use Illuminate\Database\Seeder;

class TicketsTranslationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\TicketsTranslations::class, 10)->create();

    }
}
