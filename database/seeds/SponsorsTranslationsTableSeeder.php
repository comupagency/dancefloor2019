<?php

use Illuminate\Database\Seeder;

class SponsorsTranslationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\SponsorsTranslations::class, 10)->create();
    }
}
