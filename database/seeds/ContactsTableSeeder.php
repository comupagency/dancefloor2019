<?php

use Illuminate\Database\Seeder;

class ContactsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contacts')->insert([

            'email' => 'test@email.com',

            'phone' => '912345678',

            'secondaryPhone' => '876543219',

            'address' => 'Fake street nr 3',

            'facebook' => 'www.facebook.com',

            'twitter' => 'www.twitter.com',

            'linkedin' => 'www.linkedin',

            'created_at' => date("Y-m-d H:i:s")

        ]);
    }
}
