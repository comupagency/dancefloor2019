<?php

use Illuminate\Database\Seeder;

class FaqsTranslationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\FaqsTranslations::class, 10)->create();

    }
}
