<?php

use Illuminate\Database\Seeder;

class TicketsCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\TicketsCategories::class, 10)->create();

    }
}
