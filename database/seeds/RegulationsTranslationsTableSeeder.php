<?php

use Illuminate\Database\Seeder;

class RegulationsTranslationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\RegulationsTranslations::class, 3)->create();
    }
}
