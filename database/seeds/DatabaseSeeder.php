<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AdminsTableSeeder::class); 
        $this->call(LanguagesTableSeeder::class); 
        $this->call(ArtistsTableSeeder::class);
        $this->call(ArtistsTranslationsTableSeeder::class);
        $this->call(FaqsTableSeeder::class);
        $this->call(FaqsTranslationsTableSeeder::class);
        $this->call(NewsTableSeeder::class);
        $this->call(NewsTranslationsTableSeeder::class);
        $this->call(RegulationsTableSeeder::class);
        $this->call(RegulationsTranslationsTableSeeder::class); 
        $this->call(SponsorsCategoriesTableSeeder::class);
        $this->call(SponsorsCategoriesTranslationsTableSeeder::class);
        $this->call(SponsorsTableSeeder::class);
        $this->call(SponsorsTranslationsTableSeeder::class);
        $this->call(TicketsCategoriesTableSeeder::class);
        $this->call(TicketsCategoriesTranslationsTableSeeder::class);
        $this->call(TicketsTableSeeder::class);
        $this->call(TicketsTranslationsTableSeeder::class);



    }
}
