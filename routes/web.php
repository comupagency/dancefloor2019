<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */


/*
    ADMIN AUTH ROUTES 
 */

Route::group(
    [],
    function () {
        Route::prefix('admin')->group(function () {
            /* --- Artists --- */
            Route::get('/artists', 'backoffice\ArtistsController@index')->name('admin.artists');
            Route::get('/artists/create', 'backoffice\ArtistsController@create')->name('admin.artists.create');
            Route::post('/artists/create', 'backoffice\ArtistsController@store')->name('admin.artists.create.submit');
            Route::get('/artists/edit/{id}', 'backoffice\ArtistsController@edit')->name('admin.artists.edit');
            Route::post('/artists/edit', 'backoffice\ArtistsController@update')->name('admin.artists.update');
            Route::get('/artists/delete/{id}', 'backoffice\ArtistsController@destroy')->name('admin.artists.delete');            
            Route::post('/artists/imageUpload', 'backoffice\ArtistsController@imageUpload')->name('admin.image-upload');
            Route::post('/artists/imageDelete', 'backoffice\ArtistsController@imageDelete')->name('admin.image-delete');
            Route::get('/artists/imageLoad', 'backoffice\ArtistsController@imageLoad')->name('admin.image-load');

            /* --- Tickets Categories --- */
            Route::get('/ticketscategories', 'backoffice\TicketsCategoriesController@index')->name('admin.ticketscategories');
            Route::get('/ticketscategories/create', 'backoffice\TicketsCategoriesController@create')->name('admin.ticketscategories.create');
            Route::post('/ticketscategories/create', 'backoffice\TicketsCategoriesController@store')->name('admin.ticketscategories.create.submit');
            Route::get('/ticketscategories/edit/{id}', 'backoffice\TicketsCategoriesController@edit')->name('admin.ticketscategories.edit');
            Route::post('/ticketscategories/edit', 'backoffice\TicketsCategoriesController@update')->name('admin.ticketscategories.update');
            Route::get('/ticketscategories/delete/{id}', 'backoffice\TicketsCategoriesController@destroy')->name('admin.ticketscategories.delete');   

            /* --- Tickets --- */
            Route::get('/tickets', 'backoffice\TicketsController@index')->name('admin.tickets');
            Route::get('/tickets/create', 'backoffice\TicketsController@create')->name('admin.tickets.create');
            Route::post('/tickets/create', 'backoffice\TicketsController@store')->name('admin.tickets.create.submit');
            Route::get('/tickets/edit/{id}', 'backoffice\TicketsController@edit')->name('admin.tickets.edit');
            Route::post('/tickets/edit', 'backoffice\TicketsController@update')->name('admin.tickets.update');
            Route::get('/tickets/delete/{id}', 'backoffice\TicketsController@destroy')->name('admin.tickets.delete');   
            Route::post('/tickets/imageUpload', 'backoffice\TicketsController@imageUpload')->name('admin.image-upload');
            Route::post('/tickets/imageDelete', 'backoffice\TicketsController@imageDelete')->name('admin.image-delete');
            Route::get('/tickets/imageLoad', 'backoffice\TicketsController@imageLoad')->name('admin.image-load');

            /* --- News --- */
            Route::get('/news', 'backoffice\NewsController@index')->name('admin.news');
            Route::get('/news/create', 'backoffice\NewsController@create')->name('admin.news.create');
            Route::post('/news/create', 'backoffice\NewsController@store')->name('admin.news.create.submit');
            Route::get('/news/edit/{id}', 'backoffice\NewsController@edit')->name('admin.news.edit');
            Route::post('/news/edit', 'backoffice\NewsController@update')->name('admin.news.update');
            Route::get('/news/delete/{id}', 'backoffice\NewsController@destroy')->name('admin.news.delete'); 
            Route::post('/news/imageUpload', 'backoffice\NewsController@imageUpload')->name('admin.image-upload');
            Route::post('/news/imageDelete', 'backoffice\NewsController@imageDelete')->name('admin.image-delete');
            Route::get('/news/imageLoad', 'backoffice\NewsController@imageLoad')->name('admin.image-load');

            /* --- Sponsor Categories --- */
            Route::get('/sponsorcategories', 'backoffice\SponsorsCategoriesController@index')->name('admin.sponsorscategories');
            Route::get('/sponsorcategories/create', 'backoffice\SponsorsCategoriesController@create')->name('admin.sponsorscategories.create');
            Route::post('/sponsorcategories/create', 'backoffice\SponsorsCategoriesController@store')->name('admin.sponsorscategories.create.submit');
            Route::get('/sponsorcategories/edit/{id}', 'backoffice\SponsorsCategoriesController@edit')->name('admin.sponsorscategories.edit');
            Route::post('/sponsorcategories/edit', 'backoffice\SponsorsCategoriesController@update')->name('admin.sponsorscategories.update');
            Route::get('/sponsorcategories/delete/{id}', 'backoffice\SponsorsCategoriesController@destroy')->name('admin.sponsorscategories.delete');            
            /* --- Sponsors --- */
            Route::get('/sponsors', 'backoffice\SponsorsController@index')->name('admin.sponsors');
            Route::get('/sponsors/create', 'backoffice\SponsorsController@create')->name('admin.sponsors.create');
            Route::post('/sponsors/create', 'backoffice\SponsorsController@store')->name('admin.sponsors.create.submit');
            Route::get('/sponsors/edit/{id}', 'backoffice\SponsorsController@edit')->name('admin.sponsors.edit');
            Route::post('/sponsors/edit', 'backoffice\SponsorsController@update')->name('admin.sponsors.update');
            Route::get('/sponsors/delete/{id}', 'backoffice\SponsorsController@destroy')->name('admin.sponsors.delete');            
            Route::post('/sponsors/imageUpload', 'backoffice\SponsorsController@imageUpload')->name('admin.image-upload');
            Route::post('/sponsors/imageDelete', 'backoffice\SponsorsController@imageDelete')->name('admin.image-delete');
            Route::get('/sponsors/imageLoad', 'backoffice\SponsorsController@imageLoad')->name('admin.image-load');
            /* --- Regulations --- */
            Route::get('/regulations', 'backoffice\RegulationsController@index')->name('admin.regulations');
            Route::get('/regulations/create', 'backoffice\RegulationsController@create')->name('admin.regulations.create');
            Route::post('/regulations/create', 'backoffice\RegulationsController@store')->name('admin.regulations.create.submit');
            Route::get('/regulations/edit/{id}', 'backoffice\RegulationsController@edit')->name('admin.regulations.edit');
            Route::post('/regulations/edit', 'backoffice\RegulationsController@update')->name('admin.regulations.update');
            Route::get('/regulations/delete/{id}', 'backoffice\RegulationsController@destroy')->name('admin.regulations.delete');     
            /* --- Faqs --- */
            Route::get('/faqs', 'backoffice\FaqsController@index')->name('admin.faqs');
            Route::get('/faqs/create', 'backoffice\FaqsController@create')->name('admin.faqs.create');
            Route::post('/faqs/create', 'backoffice\FaqsController@store')->name('admin.faqs.create.submit');
            Route::get('/faqs/edit/{id}', 'backoffice\FaqsController@edit')->name('admin.faqs.edit');
            Route::post('/faqs/edit', 'backoffice\FaqsController@update')->name('admin.faqs.update');
            Route::get('/faqs/delete/{id}', 'backoffice\FaqsController@destroy')->name('admin.faqs.delete');     

            

            /* --- services --- */
            Route::get('/services/create', 'backoffice\ServicesController@create')->name('admin.services.create');
            Route::post('/services/create', 'backoffice\ServicesController@store')->name('admin.services.create.submit');
            Route::get('/services/delete/{id}', 'backoffice\ServicesController@destroy')->name('admin.services.delete');
            Route::get('/services/edit/{id}', 'backoffice\ServicesController@edit')->name('admin.services.edit');
            Route::post('/admin/services/edit', 'backoffice\ServicesController@update')->name('admin.services.update');
            Route::post('/services/imageUpload', 'backoffice\ServicesController@imageUpload')->name('admin.image-upload');
            Route::post('/services/imageDelete', 'backoffice\ServicesController@imageDelete')->name('admin.image-delete');
            Route::get('/services/imageLoad', 'backoffice\ServicesController@imageLoad')->name('admin.image-load');

            Route::get('/portfolio', 'backoffice\PortfolioController@index')->name('admin.portfolio');
            Route::get('/portfolio/create', 'backoffice\PortfolioController@create')->name('admin.portfolio.create');
            Route::post('/portfolio/create', 'backoffice\PortfolioController@store')->name('admin.portfolio.create.submit');
            Route::get('/portfolio/edit/{id}', 'backoffice\PortfolioController@edit')->name('admin.portfolio.edit');
            Route::post('/portfolio/edit', 'backoffice\PortfolioController@update')->name('admin.portfolio.edit.submit');
            Route::post('/portfolio/imageUpload', 'backoffice\PortfolioController@imageUpload')->name('admin.image-upload');
            Route::post('/portfolio/imageDelete', 'backoffice\PortfolioController@imageDelete')->name('admin.image-delete');
            Route::get('/portfolio/imageLoad', 'backoffice\PortfolioController@imageLoad')->name('admin.image-load');
            
            /* Main slider*/
            Route::get('/main-slider', 'backoffice\MainSliderController@index')->name('admin.main-slider');
            Route::get('/main-slider/create', 'backoffice\MainSliderController@create')->name('admin.main-slider.create');
            Route::post('/main-slider/create', 'backoffice\MainSliderController@store')->name('admin.main-slider.create.submit');
            Route::get('/main-slider/edit/{id}', 'backoffice\MainSliderController@edit')->name('admin.main-slider.edit');
            Route::post('/main-slider/edit', 'backoffice\MainSliderController@update')->name('admin.main-slider.update');
            Route::get('/main-slider/delete/{id}', 'backoffice\MainSliderController@destroy')->name('admin.main-slider.delete');
            Route::post('/main-slider/imageUpload', 'backoffice\MainSliderController@imageUpload')->name('admin.image-upload');
            Route::post('/main-slider/imageDelete', 'backoffice\MainSliderController@imageDelete')->name('admin.image-delete');
            Route::get('/main-slider/imageLoad', 'backoffice\MainSliderController@imageLoad')->name('admin.image-load');

            Route::get('/portfolio/delete/{id}', 'backoffice\PortfolioController@destroy')->name('admin.portfolio.delete');
            /* --- Contacts --- */
            Route::get('/contacts', 'backoffice\ContactsController@index')->name('admin.contacts');
            Route::post('/contacts', 'backoffice\ContactsController@update')->name('admin.contacts.update');

            /* --- Privacy --- */
            Route::get('/privacy', 'backoffice\PrivacyController@index')->name('admin.privacy');
            Route::post('/privacy/edit', 'backoffice\PrivacyController@update')->name('admin.privacy.edit.update');

            /* --- Users --- */
            Route::get('/users', 'backoffice\UsersController@index')->name('admin.users');
            Route::get('/users/create', 'backoffice\UsersController@create')->name('admin.users.create');
            Route::post('/users/create', 'backoffice\UsersController@store')->name('admin.users.create.submit');
            Route::get('/users/edit/{id}/{userType}', 'backoffice\UsersController@edit')->name('admin.user.edit');
            Route::post('/users', 'backoffice\UsersController@update')->name('admin.users.edit.submit');
            Route::get('/users/my-profile/{id}', 'backoffice\UsersController@myProfile')->name('admin.users.my-profile');
            Route::post('/users/my-profile/{id}', 'backoffice\UsersController@storeMyProfile')->name('admin.users.store.my-profile');
            Route::get('/users/delete/{id}/{userType}', 'backoffice\UsersController@destroy')->name('admin.user.delete');

            /* --- Logs --- */
            Route::get('/logs', 'backoffice\LogViewerController@index');

            /* ---  Login  --- */
            Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
            Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
            
            /* --- Reset Password --- */
            // Password reset routes
            Route::post('/password/email', 'Auth\AdminForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
            Route::get('/password/reset', 'Auth\AdminForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
            Route::post('/password/reset', 'Auth\AdminResetPasswordController@reset');
            Route::get('/password/reset/{token}', 'Auth\AdminResetPasswordController@showResetForm')->name('admin.password.reset');
            Route::post('/logout', 'Auth\AdminLoginController@logout')->name('admin.logout');

            /* --- Dashboard --- */
            Route::get('/', 'backoffice\DashboardController@index')->name('admin.dashboard');
        });
    }
);

Auth::routes();


Route::group(['prefix' => LaravelLocalization::setLocale()], function()
{

    Route::get('/', 'frontend\HomeController@index')->name('home');
    Route::get('/lineup', 'frontend\LineupController@index')->name('lineup');
    Route::get('/press', 'frontend\PressController@index')->name('press');
    Route::get('/sponsors', 'frontend\SponsorsController@index')->name('sponsors');
    Route::get('/tickets', 'frontend\TicketsController@index')->name('tickets');
    Route::get('/artist/{id}', 'frontend\ArtistController@index')->name('artist');
    Route::get('/news', 'frontend\NewsController@index')->name('news');
    Route::get('/news/{id}', 'frontend\NewsController@singleNew')->name('new');
    Route::get('/info', 'frontend\InfoController@index')->name('info');
    Route::get('/contacts', 'frontend\ContactsController@index')->name('contacts');
    Route::post('/contacts', 'frontend\ContactsController@sendEmail')->name('contacts.submit');
    Route::get('/politica', 'frontend\PoliticaController@index')->name('politica');
		Route::get('/concurso', 'frontend\ConcursoController@index')->name('concurso');


});
